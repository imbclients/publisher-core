unit TilerControl;

interface

uses
  StdIni,
  imb4,
  WorldDataCode,
  WorldLegends,
  WorldTilerConsts, // iceh* tiler consts

  Int64Time,

  Vcl.graphics,
  Vcl.Imaging.pngimage,

  Logger, // after bitmap units (also uses log)

  // web request
  {$IFDEF WebRequestWinInet}
  Winapi.WinInet,
  {$ELSE}
  IdHTTP,
  {$ENDIF}

  System.Math,
  System.Generics.Collections, System.Generics.Defaults,
  System.Classes, System.SysUtils;

const
  PreviewImageWidth = 64; // default width/height of a minuture layer preview in pixels

  actionStatus = 4;

  sepElementID = '$';
  sepEventName = '.';

  DefaultTilerPort = 4503;

type
  TTilerLayer = class; // forward

  TOnRefresh = reference to procedure(aTilerLayer: TTilerLayer; aTimeStamp: TDateTime; aImmediate: Boolean);
  TOnTilerInfo = reference to procedure(aTilerLayer: TTilerLayer);
  TOnPreview = reference to procedure(aTilerLayer: TTilerLayer);
  TOnPointValue = reference to procedure(aTilerLayer: TTilerLayer; const aRequestID: TWDID; aLat, aLon: Double; aTimeStamp: TDateTime; aValue: Double);

  TTiler = class; // forward

  TTilerLayer = class
  constructor Create(aConnection: TConnection; const aElementID: string; aSliceType: Integer;
    aOnTilerInfo: TOnTilerInfo; aOnRefresh: TOnRefresh; aOnPreview: TOnPreview;
    aPalette: TWDPalette=nil; aID: Integer=-1; const aURL: string='');
  destructor Destroy; override;
  private
    fEvent: TEventEntry;
    // send
    fElementID: string;
    fSliceType: Integer;
    fPalette: TWDPalette;
    // return
    fID: Integer;
    fURL: string;
    fPreview: TPngImage;
    // events
    fOnTilerInfo: TOnTilerInfo;
    fOnRefresh: TOnRefresh;
    fOnPreview: TOnPreview;
    fOnPointValue: TOnPointValue;
    // refs to event handlers: make removable
    fHandleLayerEventRef: TOnEvent;
    function getURITimeStamp: string;
    function getURLTimeStamped: string;
    procedure handleLayerEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
    function LoadPreviewFromCache(): Boolean;
  public
    property event: TEventEntry read fEvent;
    property elementID: string read fElementID;
    property sliceType: Integer read fSliceType;
    property palette: TWDPalette read fPalette;
    property ID: Integer read fID;
    property URL: string read fURL;
    property URLTimeStamped: string read getURLTimeStamped;
    function previewAsBASE64: string;
    // events
    property onPointValue: TOnPointValue read fOnPointValue write fOnPointValue;
    // signal layer info
    procedure signalRegisterLayer(aTiler: TTiler; const aDescription: string; aPersistent: Boolean=False; aEdgeLengthInMeters: Double=NaN);
    // add slice with specific data for slice types
    procedure signalAddSlice(aPalette: TWDPalette=nil; aTimeStamp: TDateTime=0);
    procedure signalAddPOISlice(const aPOIImages: TArray<TPngImage>; aTimeStamp: TDateTime=0);
    procedure signalAddPNGSlice(aPNG: TPngImage; const aExtent: TWDExtent; aDiscreteColorsOnStretch: Boolean=True; aTimeStamp: TDateTime=0);
    procedure signalAddDiffSlice(
      aPalette: TWDPalette;
      aCurrentLayerID, aReferenceLayerID: Integer;
      aCurrentTimeStamp: TDateTime=0; aReferenceTimeStamp: TDateTime=0; aTimeStamp: TDateTime=0);
    procedure signalAddDiffPOISlice(
      aColorRemovedPOI, aColorSamePOI, aColorNewPOI: TAlphaRGBPixel;
      aCurrentLayerID, aReferenceLayerID: Integer;
      aCurrentTimeStamp: TDateTime=0; aReferenceTimeStamp: TDateTime=0; aTimeStamp: TDateTime=0);
    procedure signalSliceAction(aAction: UInt32=tsaClearSlice; aTimeStamp: TDateTime=0);
    procedure signalSliceRequestPointValue(const aRequestID: TWDID; aLat, aLon: Double; aTimeStamp: TDateTime=0);
    // slice properties and data
    procedure updatePalette(aPalette: TWDPalette; aTimeStamp: TDateTime=0);
    procedure signalPalette(aTimeStamp: TDateTime=0);
    procedure signalData(const aData: TByteBuffer; aTimeStamp: TDateTime=0);
    // when any slice is build
    procedure signalRequestPreview;
    procedure resetURL;
  end;

  TOnTilerStartup = reference to procedure(aTiler: TTiler; aStartupTime: TDateTime);
  TOnTilerStatus = reference to function(aTiler: TTiler): string;

  TTiler = class
  constructor Create(aConnection: TConnection; const aTilerFQDN, aTilerStatusURL: string);
  destructor Destroy; override;
  private
    fEvent: TEventEntry;
    fOnTilerStartup: TOnTilerStartup;
    fOnTilerStatus: TOnTilerStatus;
    fTilerStatusURL: string;
    fTilerFQDN: string;
    // refs to event handlers: make removable
    fHandleTilerStatusRef: TOnIntString;
    fHandleTilerEventRef: TOnEvent;

    procedure handleTilerEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
    procedure handleTilerStatus(aEventEntry: TEventEntry; aInt: Integer; const aString: string);
  public
    property event: TEventEntry read fEvent;
    property onTilerStartup: TOnTilerStartup read fOnTilerStartup write fOnTilerStartup;
    property onTilerStatus: TOnTilerStatus read fOnTilerStatus write fOnTilerStatus;
    property tilerFQDN: string read fTilerFQDN;
    property tilerStatusURL: string read fTilerStatusURL;
    function getTilerStatus: string;
  end;

function TilerStatusURLFromTilerName(const aTilerName: string; aTilerPort: Integer=DefaultTilerPort): string;

implementation

// http://stackoverflow.com/questions/301546/whats-the-simplest-way-to-call-http-get-url-using-delphi

{$IFDEF WebRequestWinInet}
function WebRequest(const aURL: string): string;
var
  NetHandle: HINTERNET;
  UrlHandle: HINTERNET;
  Buffer: array[0..1024] of Char;
  BytesRead: dWord;
begin
  Result := '';
  NetHandle := InternetOpen('Delphi 5.x', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);

  if Assigned(NetHandle) then
  begin
    UrlHandle := InternetOpenUrl(NetHandle, PChar(Url), nil, 0, INTERNET_FLAG_RELOAD, 0);

    if Assigned(UrlHandle) then
      { UrlHandle valid? Proceed with download }
    begin
      FillChar(Buffer, SizeOf(Buffer), 0);
      repeat
        Result := Result + Buffer;
        FillChar(Buffer, SizeOf(Buffer), 0);
        InternetReadFile(UrlHandle, @Buffer, SizeOf(Buffer), BytesRead);
      until BytesRead = 0;
      InternetCloseHandle(UrlHandle);
    end
    else
      { UrlHandle is not valid. Raise an exception. }
      raise Exception.CreateFmt('Cannot open URL %s', [Url]);

    InternetCloseHandle(NetHandle);
  end
  else
    { NetHandle is not valid. Raise an exception }
    raise Exception.Create('Unable to initialize Wininet');
end;
{$ELSE}
function WebRequest(const aURL: string): string;
var
  lHTTP: TIdHTTP;
begin
  lHTTP := TIdHTTP.Create;
  try
    Result := lHTTP.Get(aURL);
  finally
    lHTTP.Free;
  end;
end;
{$ENDIF}

function TilerStatusURLFromTilerName(const aTilerName: string; aTilerPort: Integer): string;
begin
  Result := 'http://'+aTilerName+'/tiler/TilerWebService.dll/status';
end;

{ TTilerLayer }

constructor TTilerLayer.Create(aConnection: TConnection; const aElementID: string; aSliceType: Integer;
  aOnTilerInfo: TOnTilerInfo; aOnRefresh: TOnRefresh; aOnPreview: TOnPreview;
  aPalette: TWDPalette; aID: Integer; const aURL: string);
begin
  inherited Create;
  fElementID := aElementID;
  fSliceType := aSliceType;
  fPalette := aPalette;
  fID := aID;
  fURL := aURL;
  fPreview := nil;
  fOnTilerInfo := aOnTilerInfo;
  fOnRefresh := aOnRefresh;
  fOnPreview := aOnPreview;
  fHandleLayerEventRef := handleLayerEvent;
  if LoadPreviewFromCache()
  then fOnPreview(Self);
  // define layer event name and make active
  fEvent := aConnection.eventEntry('USIdle.Sessions.TileLayers.'+aElementID.Replace(sepElementID, sepEventName), False).subscribe;
  fEvent.OnEvent.Add(fHandleLayerEventRef);
end;

destructor TTilerLayer.Destroy;
begin
  if Assigned(fEvent) then
  begin
    fEvent.OnEvent.Remove(fHandleLayerEventRef);
    fEvent := nil;
  end;
  FreeAndNil(fPreview);
  FreeAndNil(fPalette);
  inherited;
end;

function TTilerLayer.getURITimeStamp: string;
begin
  Result := NowUTCInt64.ToString();
end;

function TTilerLayer.getURLTimeStamped: string;
begin
  if fURL<>''
  then Result := fURL+'&ts='+getURITimeStamp
  else Result := '';
end;

procedure TTilerLayer.handleLayerEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  fieldInfo: UInt32;
  timeStamp: TDateTime;
  previewsFolder: string;
  bytes: TBytes;
  immediate: Boolean;
  lat: Double;
  lon: Double;
  value: Double;
  requestID: TWDID;
begin
  try
    timeStamp := Double.NaN;
    lat := Double.NaN;
    lon := Double.NaN;
    value := Double.NaN;
    while aCursor<aLimit do
    begin
      fieldInfo := aBuffer.bb_read_UInt32(aCursor);
      case fieldInfo of
        (icehTilerID shl 3) or wtVarInt:
          begin
            fID := aBuffer.bb_read_int32(aCursor);
          end;
        (icehTilerURL shl 3) or wtLengthDelimited:
          begin
            fURL := aBuffer.bb_read_string(aCursor);
            if Assigned(fOnTilerInfo) and Assigned(Self) then
            begin
              try
                fOnTilerInfo(self); // -> AddCommandToQueue(Self, Self.signalObjects);
              except
                on e: Exception
                do Log.WriteLn('Exception TTilerLayer.handleLayerEvent ('+Self.fElementID+') handling OnTilerInfo: '+e.Message, llError);
              end;
            end;
          end;
        (icehTilerRefresh shl 3) or wt64Bit,
        (icehTilerRefreshImmediate shl 3) or wt64Bit:
          begin
            timeStamp := aBuffer.bb_read_double(aCursor);
            if Assigned(fOnRefresh) and Assigned(Self) then
            begin
              try
                immediate := fieldInfo=(icehTilerRefreshImmediate shl 3) or wt64Bit;
                fOnRefresh(self, timeStamp, immediate);
              except
                on e: Exception
                do Log.WriteLn('Exception TTilerLayer.handleLayerEvent ('+Self.fElementID+') handling OnRefresh: '+e.Message, llError);
              end;
            end;
          end;
        (icehTilerPreviewImage shl 3) or wtLengthDelimited:
          begin
            try
              bytes := aBuffer.bb_read_tbytes(aCursor);
              FreeAndNil(fPreview);
              fPreview := TPngImage.Create;
              if Assigned(fPreview) then
              begin
                BytesToImage(bytes, fPreview);
                // cache preview
                previewsFolder := ExtractFilePath(ParamStr(0))+'previews';
                ForceDirectories(previewsFolder);
                fPreview.SaveToFile(previewsFolder+'\'+elementID+'.png');
                if Assigned(fOnPreview) and Assigned(Self) then
                begin
                  try
                    fOnPreview(self);
                  except
                    on E: Exception
                    do Log.WriteLn('Exception TTilerLayer.handleLayerEvent handling OnPreview: '+e.Message, llError);
                  end;
                end;
              end
              else Log.WriteLn('TTilerLayer.handleLayerEvent handling icehTilerPreviewImage: could not create preview png', llError);
            except
              on E: Exception
              do Log.WriteLn('Exception TTilerLayer.handleLayerEvent handling icehTilerPreviewImage: '+e.Message, llError);
            end;
          end;
        (icehTilerSliceID shl 3) or wt64Bit:
          timeStamp := aBuffer.bb_read_double(aCursor);
        (icehTilerPointValueLat shl 3) or wt64Bit:
          lat := aBuffer.bb_read_double(aCursor);
        (icehTilerPointValueLon shl 3) or wt64Bit:
          lon := aBuffer.bb_read_double(aCursor);
        (icehTilerValue shl 3) or wt64Bit:
          value := aBuffer.bb_read_double(aCursor);
        (icehTilerSlicePointValue shl 3) or wtLengthDelimited:
          begin
            requestID := aBuffer.bb_read_rawbytestring(aCursor);
            if Assigned(onPointValue)
            then onPointValue(Self, requestID, lat, lon, timeStamp, value);
          end
      else
        aBuffer.bb_read_skip(aCursor, fieldInfo and 7);
      end;
    end;
  except
    on e: Exception
    do log.WriteLn('exception in TTilerLayer.handleLayerEvent: '+e.Message, llError);
  end;
end;

function TTilerLayer.LoadPreviewFromCache: Boolean;
var
  previewsFolder: string;
  previewFilename: string;
begin
  // try to load preview from cache
  try
    previewsFolder := ExtractFilePath(ParamStr(0))+'previews';
    previewFilename := previewsFolder+'\'+elementID+'.png';
    if FileExists(previewFilename) then
    begin
      fPreview.Free;
      fPreview := TPngImage.Create;
      fPreview.LoadFromFile(previewFilename);
      Result := True;
    end
    else
    begin
      previewFilename := previewsFolder+'\'+'NoPreview'+'.png';
      if FileExists(previewFilename) then
      begin
        fPreview.Free;
        fPreview := TPngImage.Create;
        fPreview.LoadFromFile(previewFilename);
        Result := True;
      end
      else Result := False;
    end;
  except
    Result := False;
  end;
end;

function TTilerLayer.previewAsBASE64: string;
begin
  Result := ImageToBase64(fPreview);
end;

procedure TTilerLayer.resetURL;
begin
  fURL := '';
end;

procedure TTilerLayer.signalAddDiffPOISlice(aColorRemovedPOI, aColorSamePOI, aColorNewPOI: TAlphaRGBPixel; aCurrentLayerID,
  aReferenceLayerID: Integer; aCurrentTimeStamp, aReferenceTimeStamp, aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
begin
  buffer :=
    TByteBuffer.bb_tag_uint32(icehTilerColorRemovedPOI, aColorRemovedPOI)+
    TByteBuffer.bb_tag_uint32(icehTilerColorSamePOI, aColorSamePOI)+
    TByteBuffer.bb_tag_uint32(icehTilerColorNewPOI, aColorNewPOI)+
    TByteBuffer.bb_tag_int32(icehTilerLayer, aCurrentLayerID)+
    TByteBuffer.bb_tag_double(icehTilerCurrentSlice, aCurrentTimeStamp)+
    TByteBuffer.bb_tag_int32(icehTilerLayer, aReferenceLayerID)+
    TByteBuffer.bb_tag_double(icehTilerRefSlice, aReferenceTimeStamp)+
    TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.signalAddDiffSlice(aPalette: TWDPalette; aCurrentLayerID, aReferenceLayerID: Integer; aCurrentTimeStamp, aReferenceTimeStamp, aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
begin
  if Assigned(aPalette) then
  begin
    fPalette.Free;
    fPalette := aPalette;
  end;
  if Assigned(fPalette)
  then buffer := TByteBuffer.bb_tag_rawbytestring(fPalette.wdTag, fPalette.Encode)
  else buffer :='';
  buffer := buffer+
    TByteBuffer.bb_tag_int32(icehTilerLayer, aCurrentLayerID)+
    TByteBuffer.bb_tag_double(icehTilerCurrentSlice, aCurrentTimeStamp)+
    TByteBuffer.bb_tag_int32(icehTilerLayer, aReferenceLayerID)+
    TByteBuffer.bb_tag_double(icehTilerRefSlice, aReferenceTimeStamp)+
    TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.signalAddPNGSlice(aPNG: TPngImage; const aExtent: TWDExtent; aDiscreteColorsOnStretch: Boolean; aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
begin
  buffer :=
    TByteBuffer.bb_tag_rawbytestring(icehTilerPNGExtent, aExtent.Encode)+
    TByteBuffer.bb_tag_tbytes(icehTilerPNGImage, ImageToBytes(aPNG))+
    TByteBuffer.bb_tag_bool(icehTilerDiscreteColorsOnStretch, aDiscreteColorsOnStretch)+
    TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.signalAddPOISlice(const aPOIImages: TArray<TPngImage>; aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
  poi: TPngImage;
begin
  buffer :='';
  for poi in aPOIImages
  do buffer := buffer+TByteBuffer.bb_tag_tbytes(icehTilerPOIImage, ImageToBytes(poi));
  buffer := buffer+TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.signalAddSlice(aPalette: TWDPalette; aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
begin
  if Assigned(aPalette) then
  begin
    fPalette.Free;
    fPalette := aPalette;
  end;
  // first all layer properties
  if Assigned(fPalette)
  then buffer := TByteBuffer.bb_tag_rawbytestring(fPalette.wdTag, fPalette.Encode)
  else buffer :='';
  buffer := buffer+TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.signalData(const aData: TByteBuffer; aTimeStamp: TDateTime);
begin
  fEvent.signalEvent(
    TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp)+
    TByteBuffer.bb_tag_rawbytestring(icehTilerSliceUpdate, aData));
end;

procedure TTilerLayer.signalPalette(aTimeStamp: TDateTime);
begin
  if Assigned(fPalette) then
  begin
    fEvent.signalEvent(
      TByteBuffer.bb_tag_rawbytestring(fPalette.wdTag, fPalette.Encode)+
      TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp));
  end;
end;

procedure TTilerLayer.signalRegisterLayer(aTiler: TTiler; const aDescription: string; aPersistent: Boolean; aEdgeLengthInMeters: Double);
var
  payload: TByteBuffer;
begin
  payload :=
    TByteBuffer.bb_tag_string(icehTilerEventName, fEvent.eventName);
  if not IsNaN(aEdgeLengthInMeters)
  then payload := Payload+
    TByteBuffer.bb_tag_double(icehTilerEdgeLength, aEdgeLengthInMeters);
  payload := Payload+
    TByteBuffer.bb_tag_string(icehTilerLayerDescription, aDescription)+
    TByteBuffer.bb_tag_bool(icehTilerPersistent, aPersistent)+
    TByteBuffer.bb_tag_int32(icehTilerRequestNewLayer, fSliceType); // last to trigger new layer request
  aTiler.event.signalEvent(payload);
end;

procedure TTilerLayer.signalRequestPreview;
begin
  fEvent.signalEvent(TByteBuffer.bb_tag_uint32(icehTilerRequestPreviewImage, PreviewImageWidth));
end;

procedure TTilerLayer.signalSliceAction(aAction: UInt32; aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
begin
  buffer :=
    TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp)+
    TByteBuffer.bb_tag_uint32(icehTilerSliceAction, aAction);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.signalSliceRequestPointValue(const aRequestID: TWDID; aLat, aLon: Double; aTimeStamp: TDateTime);
var
  buffer: TByteBuffer;
begin
  buffer :=
    TByteBuffer.bb_tag_double(icehTilerPointValueLat, aLat)+
    TByteBuffer.bb_tag_double(icehTilerPointValueLon, aLon)+
    TByteBuffer.bb_tag_double(icehTilerSliceID, aTimeStamp)+
    TByteBuffer.bb_tag_rawbytestring(icehTilerSlicePointValue, aRequestID);
  fEvent.signalEvent(buffer);
end;

procedure TTilerLayer.updatePalette(aPalette: TWDPalette; aTimeStamp: TDateTime);
begin
  if fPalette<>aPalette then
  begin
    fPalette.Free;
    fPalette := aPalette;
    signalPalette(aTimeStamp);
  end;
end;

{ TTiler }

constructor TTiler.Create(aConnection: TConnection; const aTilerFQDN, aTilerStatusURL: string);
begin
  inherited Create;
  fOnTilerStartup := nil;
  fTilerStatusURL := aTilerStatusURL;
  fTilerFQDN := aTilerFQDN;
  fHandleTilerStatusRef := handleTilerStatus;
  fHandleTilerEventRef := handleTilerEvent;
  //fLayers := TObjectDictionary<string, TTilerLayer>.Create([doOwnsValues]);
  fEvent := aConnection.eventEntry('USIdle.Tilers.'+aTilerFQDN.Replace('.', '_'), False).subscribe;
  if not fEvent.OnEvent.Contains(fHandleTilerEventRef)
  then fEvent.OnEvent.Add(fHandleTilerEventRef);
  if not fEvent.OnIntString.Contains(fHandleTilerStatusRef)
  then fEvent.OnIntString.Add(fHandleTilerStatusRef);
  // start tiler web service
  if fTilerStatusURL<>'' then
  begin
    try
      getTilerStatus;
    except
      on E: Exception
      do Log.WriteLn('Exception getting tiler status on url '+fTilerStatusURL+': '+E.Message, llError);
    end;
  end;
end;

destructor TTiler.Destroy;
begin
  if Assigned(fEvent) then
  begin
    fEvent.OnIntString.remove(fHandleTilerStatusRef);
    fEvent.OnEvent.Remove(fHandleTilerEventRef);
    fEvent := nil;
  end;
  inherited;
end;

function TTiler.getTilerStatus: string;
begin
  Result := WebRequest(fTilerStatusURL);
end;

procedure TTiler.handleTilerEvent(aEventEntry: TEventEntry; const aBuffer: TByteBuffer; aCursor, aLimit: Integer);
var
  fieldInfo: UInt32;
  tilerStartup: TDateTime;
begin
  try
    while aCursor<aLimit do
    begin
      fieldInfo := aBuffer.bb_read_UInt32(aCursor);
      case fieldInfo of
        (icehTilerStartup shl 3) or wt64Bit:
          begin
            tilerStartup := aBuffer.bb_read_double(aCursor);
            if Assigned(fOnTilerStartup)
            then fOnTilerStartup(Self, tilerStartup);
          end;
      else
        aBuffer.bb_read_skip(aCursor, fieldInfo and 7);
      end;
    end;
  except
    on e: Exception
    do log.WriteLn('exception in TTiler.handleTilerEvent: '+e.Message, llError);
  end;
end;

procedure TTiler.handleTilerStatus(aEventEntry: TEventEntry; aInt: Integer; const aString: string);
var
  e: TEventEntry;
  status: string;
  info: string;
begin
  case aInt of
    actionStatus:
      begin
        if aString<>''
        then e := aEventEntry.connection.eventEntry(aString, False).publish
        else e := aEventEntry;
        try
          if e.connection.connected
          then status := 'connected'
          else status := 'NOT connected';
          if Assigned(fOnTilerStatus)
          then info := ',"info":"'+fOnTilerStatus(self)+'"'
          else info := '';
          e.signalString('{"id":"'+aEventEntry.connection.ModelName+'","status":"'+status+'"'+info+'}');
        finally
          if aString<>''
          then e.unPublish();
        end;
      end;
  end;
end;

end.
