unit PubModel;

interface

uses
  system.generics.collections,
  system.sysutils;

type
  TPubModel = class; // forward

  THandler = reference to procedure(aRecevier, aModel: TPubModel; const aVerb: string);

  TForEach = reference to procedure(aModel: TPubModel);

  TReceiverEntry = class
  constructor Create(aReceiver: TPubModel; aHandler: THandler);
  destructor Destroy; override;
  private
    fReceiver: TPubModel; // ref
    fHandler: THandler; // owns, but is auto type
  public
    property receiver: TPubModel read fReceiver;
    property handler: THandler read fHandler;
  end;

  TPubModel = class
  constructor Create(aOwner: TPubModel; const aID: string);
  destructor Destroy; override;
  private
    fOwner: TPubModel; // ref
    fID: string;
    fUsers: TDictionary<string, TPubModel>; // refs
    fUsing: TDictionary<string, TPubModel>; // refs
    fReceivers: TObjectDictionary<string, TObjectList<TReceiverEntry>>;
    function getElementID(): string;
  public
    property owner: TPubModel read fOwner;
    property id: string read fID;
    property elementID: string read getElementID;
    property using: TDictionary<string, TPubModel> read fUsing;
    property users: TDictionary<string, TPubModel> read fUsers;
  public
    function use(aModel: TPubModel): TPubModel;
    procedure unUse(aModel: TPubModel);
  public
    property receivers: TObjectDictionary<string, TObjectList<TReceiverEntry>> read fReceivers;
    procedure &on(const aVerb: string; aReceiver: TPubModel; aHandler: THandler);
    procedure off(const aVerb: string; aReceiver: TPubModel);
    procedure emmit(const aVerb: string);
    procedure forEachReceiver<T: TPubModel>(aCallBack: TForEach);
  end;

implementation

{ TReceiverEntry }

constructor TReceiverEntry.Create(aReceiver: TPubModel; aHandler: THandler);
begin
  inherited Create;
  fReceiver := aReceiver;
  fHandler := aHandler;
end;

destructor TReceiverEntry.Destroy;
begin
  fReceiver := nil;
  fHandler := nil;
  inherited;
end;

{ TPubModel }

procedure TPubModel.&on(const aVerb: string; aReceiver: TPubModel; aHandler: THandler);
begin

end;

constructor TPubModel.Create(aOwner: TPubModel; const aID: string);
begin
  inherited Create;
  fOwner := aOwner;
  fID := aID;
  fUsers := TDictionary<string, TPubModel>.Create();
  fUsing := TDictionary<string, TPubModel>.Create();
  fReceivers := TObjectDictionary<string, TObjectList<TReceiverEntry>>.Create([doOwnsValues]);
end;

destructor TPubModel.Destroy;
begin
  FreeAndNil(fReceivers);
  FreeAndNil(fUsing);
  FreeAndNil(fUsers);
  inherited;
end;

procedure TPubModel.emmit(const aVerb: string);
var
  receiverList: TObjectList<TReceiverEntry>;
  r: TReceiverEntry;
begin
  if receivers.TryGetValue(aVerb, receiverList) then
  begin
    for r in receiverList
    do r.handler(r.fReceiver, Self, aVerb);
  end;
end;

procedure TPubModel.forEachReceiver<T>(aCallBack: TForEach);
begin

end;

function TPubModel.getElementID: string;
begin
  if Assigned(owner)
  then Result := owner.id+'$'+id
  else Result := id;
end;

procedure TPubModel.off(const aVerb: string; aReceiver: TPubModel);
begin

end;

procedure TPubModel.unUse(aModel: TPubModel);
begin

end;

function TPubModel.use(aModel: TPubModel): TPubModel;
begin
  Result := aModel;
  TMonitor.Enter(aModel.users);
  try
    aModel.users.AddOrSetValue(id, self);
  finally
    TMonitor.Exit(aModel.users);
  end;
  TMonitor.Enter(using);
  try
    using.AddOrSetValue(aModel.id, aModel);
  finally
    TMonitor.Exit(using);
  end;
end;

end.
