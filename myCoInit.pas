unit MyCoInit;

interface

uses
  Winapi.ActiveX, // COINIT_MULTITHREADED, CoUninitialize
  System.Win.ComObj, // CoInitializeEx
  System.Generics.Collections, // TDictionary<>
  System.Classes,
  System.SysUtils;


// call CoInitializeEx once for every thread calling this functions
// store those threads to call CoUnitialize on exit
procedure CheckCoInit();


implementation

var
  coinitThreadList: TDictionary<TThread, Integer> = nil;

procedure CheckCoInit();
begin
  TMonitor.Enter(coinitThreadList);
  try
    if not coinitThreadList.ContainsKey(TThread.CurrentThread) then
    begin
      CoInitializeEx(nil, COINIT_MULTITHREADED);
      coinitThreadList.Add(TThread.CurrentThread, 1);
    end;
  finally
    TMonitor.Exit(coinitThreadList);
  end;
end;

procedure CoUninit();
var
  tsp: TPair<TThread, Integer>;
begin
  TMonitor.Enter(coinitThreadList);
  try
    for tsp in coinitThreadList
    do CoUninitialize();
    coinitThreadList.Clear;
  finally
    TMonitor.Exit(coinitThreadList);
  end;
end;

initialization
  coinitThreadList := TDictionary<TThread, Integer>.Create;
finalization
  CoUninit;
  FreeAndNil(coinitThreadList);
end.

