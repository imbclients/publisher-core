unit PublishServerLib;

interface

uses
  StdIni,
  imb4,
  Data.DB,
  FireDAC.Comp.Client,
  TimerPool,
  WorldDataCode,
  WorldLegends,
  IdCoderMIME, IdGlobal, //IdCoder,
  Int64Time,

  WinApi.Windows,

  Vcl.graphics,
  Vcl.Imaging.pngimage,

  // use vcl bitmaps (FMX: conflict on er)

  Logger, // after bitmap units (also use log)
  CommandQueue,
  WorldTilerConsts, // iceh* tiler consts
  TilerControl,

	System.Classes, System.Generics.Collections, // mind order
  System.JSON, System.SysConst, System.Math,
  System.Generics.Defaults,
  System.SysUtils;

const
  WS2IMBEventName = 'USIdle.Sessions.WS2IMB';
  sepElementID = '$';
  MaxDirectSendObjectCount = 300; // todo: tune, 200, 500.. ?

  colorBasicOutline = $3388FF or $FF000000;
  colorBasicFill = $B5C6DD or $FF000000;

  TilerNameSwitch = 'TilerName';
    DefaultTilerName = 'vps17642.public.cloudvps.com';

  TilerStatusURLSwitch = 'TilerStatusURL';

  MaxEdgeLengthInMetersSwitchName = 'MaxEdgeLengthInMeters';
    DefaultMaxEdgeLengthInMeters = 250;

  MaxNearestObjectDistanceInMetersSwitch = 'MaxNearestObjectDistanceInMeters';
    DefaultMaxNearestObjectDistanceInMeters = 50;

  WebClientURISwitch = 'WebClientURI';
    DefaultWebClientURI = 'HTTP://vps17642.public.cloudvps.com';

  SourceEPSGSwitch = 'SourceEPSG';

  UseScenarioHierarchySwitch = 'UseScenarioHierarchy';

  MaxFileUploadSize = 600*1024*1024;

  // client layer types
  ltTile  = 'tile';
  ltObject = 'object';
  ltGeo = 'geo';
  ltSwitch = 'switch';
  ltMarker = 'marker';
  ltEmpty  = '';
  ltSimple = 'simple';

  // simple object json names
  sojnGeometry = 'latlng';
  sojnOptions = 'options';
  // other
  sojnIcon = 'icon';
  sojnObjectType = 'objectType';
  sojnTooltip = 'tooltip';
  sojnNoTooltip = 'notooltip';
  sojnPopup = 'popup';
  sojnNoPopup = 'nopopup';
  sojnContextMenu = 'contextmenu';
  sojnContextmenuItems = 'contextmenuItems';
  sojnContextmenuInheritItems = 'contextmenuInheritItems';
  sojnInteractive = 'interactive'; // false on markers makes non-draggable!
  sojnDraggable = 'draggable'; // on non-markers makes always interactive!
  sojnRadius = 'radius'; // circle (in meters), circle marker (pixels)

  // time slider options
  ts_Off = 0;
  ts_collapsed = 1;
  ts_expanded = 2;

  // controls
  timeSliderControl = 'timeslider';
  selectControl = 'selectionEnabled';
  measuresControl = 'measuresEnabled';
  measuresHistoryControl = 'measuresHistoryEnabled';
  simulationControl = 'simulationControlEnabled';
  simulationSetupControl = 'simulationSetup';
  simulationCloseControl = 'simulationClose';
  startstopControl = 'startstopControlEnabled';
  presenterViewerControl = 'presenterViewerControl';
  modelControl = 'modelControlEnabled';
  filesControl = 'filesControlEnabled';
  goLiveControl = 'goLiveControlEnabled';
  controlsControl = 'controlsControlEnabled';
  overviewControl = 'overviewControlEnabled';
  closeProjectControl = 'closeProjectEnabled';

  controlDisabled = '0';
  controlEnabled = '1';


  chartMaxPoints = 5000; // 200;
  chartDefaultXScale = 'linear';

  publisherDateTimeFormat = 'yyyy-mm-dd hh:mm';

  DefaultTilerPreviewTime = 10;
  DefaultTilerRefreshTime = 2;
  ImmediateTilerRefreshTime = 1/4; // one quarter a second
  DefaultTilerMaxRefreshTime = 7;

  DefaultLayerUpdateFrequency = 5.0; // 5 Hz

  DefaultChartPreviewTime = 15;
    MaxChartPreviewTime = 30;
  DefaultChartUpdateTime = 2;
  chartMaxUpdateTime = 10;

  EarthRadiusMeters = 63727982;
  MaxPreferredSelectPixelDist = 10;
  MaxSelectPixelDist = 50;

  MaxLayerObjects = 300; //max number of geojson objects to display for a layer

var
  subscribeObjectCS: TRTLCriticalSection;

type
  TMessageType = (
    mtNone,
    mtError,
    mtWarning,
    mtSucces
  );

  TDistanceLatLon = record
    m_per_deg_lat: Double;
    m_per_deg_lon: Double;

    class function Create(aLat1InDegrees, aLat2InDegrees: Double): TDistanceLatLon; overload; static;
    class function Create(aLatMidInRad: Double): TDistanceLatLon; overload; static;
    function distanceInMeters(aDeltaLat, aDeltaLon: Double): Double;
  end;

  TProject = class; // forward

  TSubscribeObject = class;

  TScenario = class; // forward

  TLayer = class; // forward

  TClientDomain = record
  class function Create(const aName: string): TClientDomain; static;
  private
    function getJSON: string;
  public
    name: string;
    kpis: string;
    charts: string;
    layers: string;
    enabled: Integer;
    property JSON: string read getJSON;
  end;

  TSessionModel = class; // forward

  TClient = class; // forward

  TGroup = TObjectList<TClient>;

  TForEachClient = reference to procedure(aClient: TCLient);

  TForEach<T: TSubscribeObject> = reference to procedure(aSubject: T);

  TSubAction = reference to procedure(aSender: TSubscribeObject; const aAction, aObjectID: Integer);

  TSubscribeObject = class
  constructor Create;
  destructor Destroy; override;
  private
    fSubscribers: TObjectDictionary<TSubscribeObject, TSubAction>; //read/write locks through fSubscribersLock, objects subscribed to this object
    fSubscriptions: TObjectDictionary<TSubscribeObject, TSubscribeObject>; //read/write locks through fSubscriptionsLock, objects this object is subscribed to
    fSubscribersLock, fSubscriptionsLock: TOmniMREW;
    fClosing: Boolean;
  protected
  public
    property Subscriptions: TObjectDictionary<TSubscribeObject, TSubscribeObject> read fSubscriptions;
    property Subscribers: TObjectDictionary<TSubscribeObject, TSubAction> read fSubscribers;
    property SubscribersLock: TOmniMREW read fSubscribersLock;
    property SubscriptionsLock: TOmniMREW read fSubscriptionsLock;
  public
    //subscribes/unsubscribes to other TSubscribeObject
    function SubscribeTo(aSubscribable: TSubscribeObject; aSubAction: TSubAction): Boolean;
    function UnsubscribeFrom(aSubscribable: TSubscribeObject; aDestroying: Boolean = False): Boolean;
    //handles subscription/unsubscription from other TSubscribeObject
    function HandleSubscribe(aSubscriber: TSubscribeObject; aSubAction: TSubAction): Boolean;
    function HandleUnsubscribe(aSubscriber: TSubscribeObject): Boolean;
    //sends event to all
    procedure SendEvent(aSender: TSubscribeObject; const aAction, aObjectID: Integer);
    procedure SendAnonymousEvent(const aAction, aObjectID: Integer);

    procedure forEachSubscriber<T: TSubscribeObject>(aForEach: TForEach<T>);
  end;

  TClientSubscribable = class(TSubscribeObject)
  constructor Create;
  destructor Destroy; override;
  private
    fClientAmount: Integer; //locks with fAmountLock
    fAmountLock: TOmniMREW;
  protected
    function getElementID: string; virtual; abstract;
  public
    property ClientAmount: Integer read fClientAmount;
    property elementID: string read getElementID;
  public
    function HandleClientSubscribe(aClient: TClient): Boolean; virtual;
    function HandleClientUnsubscribe(aClient: TClient): Boolean; virtual;
    procedure HandleFirstSubscriber(aClient: TClient); virtual;
    procedure HandleLastSubscriber(aClient: TClient); virtual;
  end;

  TDiffLayer = class(TClientSubscribable)
  constructor Create(const aElementID: string; aCurrentLayer, aReferenceLayer: TLayer; aTilerMaxRefreshTime: Integer=DefaultTilerMaxRefreshTime);
  destructor Destroy; override;
  private
    fCurrentLayer: TLayer;
    fReferenceLayer: TLayer;
    fTilerLayer: TTilerLayer; // owns
    fSendRefreshTimer: TTimer;
    fPreviewRequestTimer: TTimer;
    fTilerRefreshTime: Integer;
    fTilerPreviewTime: Integer;
    fLegendJSON: string;
    function getRefJSON: string;
  protected
    function getElementID: string; override;
    procedure handleTilerInfo(aTilerLayer: TTilerLayer);
    procedure handleTilerRefresh(aTilerLayer: TTilerLayer; aTimeStamp: TDateTime; aImmediate: Boolean);
    procedure handleTilerPreview(aTilerLayer: TTilerLayer);

    procedure handleRefreshTrigger(aTimeStamp: TDateTime);
  public
    property tilerLayer: TTilerLayer read fTilerLayer;
    property refJSON: string read getRefJSON;
    property legendJSON: string read fLegendJSON;

    property tilerRefreshTime: Integer read fTilerRefreshTime write fTilerRefreshTime;
    property tilerPreviewTime: Integer read fTilerPreviewTime write fTilerPreviewTime;

    procedure handleSubLayerInfo(aLayer: TLayer);
    procedure registerOnTiler();
    function HandleClientSubscribe(aClient: TClient): Boolean; override;
    function HandleClientUnsubscribe(aClient: TClient): Boolean; override;
  end;

  TClientFileUploadInfo = record
    fileName: string;
    fileSize: Integer;
    filledSize: Integer;
    path: string;
  end;

  TMapView = record
  class function Create(aLat, aLon, aZoom: Double): TMapView; overload; static;
  class function Create(const aConfig: string): TMapView; overload; static;
  private
    fLat: Double;
    fLon: Double;
    fZoom: Double;
  public
    property lat: Double read fLat;
    property lon: Double read fLon;
    property zoom: Double read fZoom;

    procedure DumpToLog;
    function ToExtent(xTileCount, yTileCount: Integer): TWDExtent;
  end;

  TFormDialogResults = class; // forward

  TOnFormDialogResult = reference to procedure(aClient: TClient; aResult: TFormDialogResults);

  TFormDialogResultHandling = record
  class function Create(aHandler: TOnFormDialogResult; aRunOnce: Boolean=True): TFormDialogResultHandling; static;
  public
    Handler: TOnFormDialogResult;
    RunOnce: Boolean;
  end;

  TFormDialogResultProperty = class
  constructor Create(const aName, aValue, aType: string);
  private
    fName: string;
    fValue: string;
    fType: string;
  public
    property Name: string read fName;
    property Value: string read fValue;
    property &Type: string read fType;
  end;

  TFormDialogResults = class
  constructor Create(const aFormID: string; aResultParameters: TJSONValue; aContext: TJSONValue);
  destructor Destroy; override;
  private
    fFormID: string;
    fProperties: TObjectDictionary<string, TFormDialogResultProperty>; // owns
    fContext: TJSONValue; // ref
  public
    property FormID: string read fFormID;
    property Properties: TObjectDictionary<string, TFormDialogResultProperty> read fProperties;
    property Context: TJSONValue read fContext;
  end;

  TFormDialogProperty = class
  constructor Create(const aName, aLabel, aType: string; const aFormElement: string='input';
    aRequired: boolean = true; aHidden: boolean = false);
  destructor Destroy; override;
  private
    fName: string;
    fLabel: string;
    fType: string;
    fFormElement: string;
    fRequired: boolean;
    fHidden: boolean;
    fOptionsArray: TStrings;
    fExtraOptionsJSON: string;
  public
    property Name: string read fName;
    property &Label: string read fLabel write fLabel;
    property &Type: string read fType write fType;
    property FormELement: string read fFormElement write fFormElement;
    property Required: boolean read fRequired write fRequired;
    property Hidden: boolean read fHidden write fHidden;
    property OptionsArray: TStrings read fOptionsArray;
    property ExtraOptionsJSON: string read fExtraOptionsJSON write fExtraOptionsJSON;

    function JSON: string; virtual;
  end;

  TFormDialog = class
  constructor Create(const aFormID: string; const aTitle: string);
  destructor Destroy; override;
  private
    fFormID: string;
    fTitle: string;
    fProperties: TObjectList<TFormDialogProperty>; // owns, order
    fPropertiesLookup: TObjectDictionary<string, TFormDialogProperty>; // refs, auto filled
    procedure HandlePropertiesNotify(aSender: TObject; const aItem: TFormDialogProperty; aAction: TCollectionNotification);
  public
    property FormID: string read fFormID;
    property Title: string read fTitle;
    property Properties: TObjectList<TFormDialogProperty> read fProperties; // order and triggers auto fill lookup
    property PropertiesLookup: TObjectDictionary<string, TFormDialogProperty> read fPropertiesLookup; // auto filled

    function AddPropertyInput(const aName, aLabel: string; const aDefaultValue: string = ''; const aType: string='string'; aRequired: boolean=true; aHidden: boolean=false): TFormDialogProperty;
    function AddPropertyRadio(const aName, aLabel, aDefaultValue: string; const aOptions: System.TArray<string>=[]; const aType: string='string'; aRequired: boolean=true; aHidden: boolean=false): TFormDialogProperty;
    function AddPropertySelect(const aName, aLabel, aDefaultValue: string; const aOptions: System.TArray<string>=[]; aRequired: boolean=true; aHidden: boolean=false): TFormDialogProperty;
    function AddPropertyCheck(const aName, aLabel: string; const aOptions: System.TArray<string>=[]; const aType: string='string'; aRequired: boolean=true; aHidden: boolean=false): TFormDialogProperty;
    function AddPropertyText(const aName, aLabel, aType: string; aRequired: boolean=true; aHidden: boolean=false): TFormDialogProperty;
    function AddPropertySlider(const aName, aLabel: string; aMin, aMax, aStep: Integer; const aUnit: string; const aType: string='int'; aRequired: boolean=true; aHidden: boolean=false): TFormDialogProperty;

    function JSON(const aContext: string='{}'): string;

    procedure Prepare(aClient: TClient; aOnResult: TFormDialogResultHandling); // only prepare ie add handler for form id results
    procedure Open(aClient: TClient; aOnResult: TFormDialogResultHandling; const aContext: string='{}'); // prepare and open dialog form on client
  end;

  TFileGenerator = reference to procedure(aProject: TProject; aClient: TClient; const aName, aType: string);

  TDownloadableFileInfo = record
    fileGenerator: TFileGenerator;
    fileTypes: TArray<string>;
  end;

  TClient = class (TSubscribeObject)
  constructor Create(aOwnerProject: TProject; aCurrentScenario, aRefScenario: TScenario; const aClientID: string);
  destructor Destroy; override;
  private
    fControls: TDictionary<string, string>; //locks with TMonitor -> always lock this first if you also lock the project controls!
    fSubscribedElements: TObjectList<TClientSubscribable>; // ref, use TMonitor to lock
    fCurrentScenario: TScenario; // ref
    fRefScenario: TScenario; // ref
    fClientID: string;
    fClientEvent: TEventEntry;
    fFileUploads: TDictionary<string, TClientFileUploadInfo>;
    fFormResultHandlers: TDictionary<string, TFormDialogResultHandling>;
    fLead: Boolean;
    fFollow: Boolean;
    fCanCopyScenario: Boolean;
    // portal
    fLastValidToken: string;
    // session info
    fUserID: string;
    fEditMode: string;

    function getSessionModel: TSessionModel;

    function sessionDescription(aProject: TProject): string;
    function activeScenarioJSON: string;
    function referenceScenarioJSON: string;
    function controlsJSON: string;
    function mapViewJSON: string;
    procedure setCurrentScenario(const aValue: TScenario);
    procedure setRefScenario(const aValue: TScenario);
    procedure setCanCopyScenario(const Value: Boolean);
  protected
    fOwnerProject: TProject; // ref to owning project
    procedure SendErrorMessage(const aMessage: string);
    procedure SendMeasures(aProject: TProject);
    procedure SendMeasuresHistory(aProject: TProject);
    procedure sendQueryDialogData(aProject: TProject);
  public
    procedure SendSession(aProject: TProject);
    procedure removeClient(aElement: TClientSubscribable);
    procedure addClient(aElement: TClientSubscribable);
    property sessionModel: TSessionModel read getSessionModel;
    property subscribedElements: TObjectList<TClientSubscribable> read fSubscribedElements; // ref, use TMonitor to lock
    property clientID: string read fClientID;
    property currentScenario: TScenario read fCurrentScenario write setCurrentScenario;
    property refScenario: TScenario read fRefScenario write setRefScenario;
    property formResultHandlers: TDictionary<string, TFormDialogResultHandling> read fFormResultHandlers;
    property CanCopyScenario: Boolean read fCanCopyScenario write setCanCopyScenario;
    property lastValidToken: string read fLastValidToken;
    property userID: string read fUserID;
    property editMode: string read fEditMode write fEditMode;

    procedure UpdateSession(aProject: TProject);

    procedure signalString(const aString: string);

    procedure SendRefresh(const aElementID, aTimeStamp, aObjectsTilesLink: string);
    procedure SendRefreshRef(const aElementID, aTimeStamp, aRef: string);
    procedure SendRefreshDiff(const aElementID, aTimeStamp, aDiff: string);
    procedure SendPreview(const aElementID, aPreviewBase64: string);
    procedure SendLegend(const aElementID, aTimeStamp, aLegendJSON: string);
    //procedure SendClearDownloadableFiles();
    procedure SendAddDownloadableFile(const aFileName: string; const aFileTypes: TArray<string>);
    procedure SendAddDownloadableFiles(aDownloadableFiles: TDictionary<string, TDownloadableFileInfo>);
    procedure SendRemoveDownloadableFile(const aNames: TArray<string>);
    procedure SendClearDownloadableFile();
    procedure SendDownloadableFileStream(const aName: string; aStream: TStream);
    procedure SendMessage(const aMessage: string; aMessageType: TMessageType=mtError; aMessageTimeOutMS: Integer=-1);
    procedure SendView(aLat, aLon, aZoom: Double);
    procedure SendDomains(const aPrefix: string);

    procedure HandleElementRemove(aElement: TClientSubscribable);
    procedure HandleScenarioRemove(aScenario: TScenario);

    procedure HandleClientCommand(const aJSONString: string);
  public
    property controls: TDictionary<string, string> read fControls;
  protected
    function getControl(const aControlName: string): string;
    procedure setControl(const aControlName, aControlValue: string);
  public
    property Control[const aControlName: string]: string read getControl write setControl;

    procedure signalControl(const aControlName: string);
    procedure signalControls();
  end;

  TScenarioElement = class(TClientSubscribable)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aDisplayGroup: string='default');
  destructor Destroy; override;
  protected
    fScenario: TScenario; // ref
    fDomain: string;
    fID: string;
    fName: string;
    fDescription: string;
    fDefaultLoad: Boolean;
    fDisplayGroup: string;
  protected
    function getJSON: string; virtual;
    function getElementID: string; override;
  public
    property scenario: TScenario read fScenario;
    property domain: string read fDomain;
    property ID: string read fID;
    property name: string read fName;
    property defaultLoad: Boolean read fDefaultLoad;
    property JSON: string read getJSON;
    property elementID: string read getElementID;
    property displayGroup: string read fDisplayGroup write fDisplayGroup;
    // writable
    property description: string read fDescription write fDescription;
  end;

  TLayerObject = class
  constructor Create(aLayer: TLayer);
  private
    fLayer: TLayer;
  protected
    function getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string; virtual; abstract;
    function getValidGeometry: Boolean; virtual;
    function getExtent: TWDExtent; virtual;
    function valueToColors(aValue: Double): TGeoColors; // lookup color on value through layer
  public
    function Encode: TByteBuffer; virtual; abstract;
    function EncodeRemove: TByteBuffer; virtual; abstract;
  public
    property layer: TLayer read fLayer;
    property JSON2D[const aSide: Integer; const aType, aExtraJSON2DAttributes: string]: string read getJSON2D;
    property ValidGeometry: Boolean read getValidGeometry;
    property Extent: TWDExtent read getExtent;
    function distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double; virtual; abstract;
    function intersects(aGeometry: TWDGeometry): Boolean; virtual; abstract;
    procedure translate(dx, dy: Double); virtual;
  end;

  TLayerObjectWithID = class(TLayerObject)
  constructor Create(aLayer: TLayer; const aID: TWDID);
  private
    fID: TWDID;
  public
    function Encode: TByteBuffer; override;
    function EncodeRemove: TByteBuffer; override;
  public
    property ID: TWDID read fID;

  end;

  TLayerMarker = class(TLayerObjectWithID)
  constructor Create(aLayer: TLayer; const aID: TWDID; aLat, aLon: Double; const aTitle: string=''; aDraggable: Boolean=True{; aRadius: Double=DefaultMarkerRadius});
  protected
    fGeometryPoint: TWDGeometryPoint; // owns
    fOpacity: Single;
    fTitle: string;
    fAlt: string;
    fDraggable: Boolean;
    fRiseOnHover: Boolean;
    fRiseOffset: Integer;
    //fRadius: Double;
    function getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string; override;
    function getValidGeometry: Boolean; override;
    function getExtent: TWDExtent; override;
  public
    function distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double; override;
    function intersects(aGeometry: TWDGeometry): Boolean; override;
    procedure translate(dx, dy: Double); override;
  end;

  TGeometryPointLayerObject = class(TLayerObjectWithID)
  constructor Create(aLayer: TLayer; const aID: TWDID; aGeometryPoint: TWDGeometryPoint; aValue: Double);
  destructor Destroy; override;
  protected
    fGeometryPoint: TWDGeometryPoint; // owns
    fValue: Double; // value to lookup color within palette of layer
  protected
    function getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string; override;
    function getValidGeometry: Boolean; override;
    function getExtent: TWDExtent; override;
  public
    function Encode: TByteBuffer; override;
  public
    property geometryPoint: TWDGeometryPoint read fGeometryPoint;
    property value: Double read fValue write fValue;
    function distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double; override;
    function intersects(aGeometry: TWDGeometry): Boolean; override;
    procedure translate(dx, dy: Double); override;
  end;
  {
  TGeometryPointHeightLayerObject = class(TGeometryPointLayerObject)
  constructor Create(aLayer: TLayer; const aID: TWDID; aGeometryPoint: TWDGeometryPoint; aValue, aHeight: Double);
  protected
    fHeight: Double;
  public
    function Encode: TByteBuffer; override;
  public
    property height: Double read fHeight write fHeight;
  end;
  }
  TGeometryLayerPOIObject = class(TLayerObjectWithID)
  constructor Create(aLayer: TLayer; const aID: TWDID; aPOI: Integer; aGeometryPoint: TWDGeometryPoint);
  destructor Destroy; override;
  protected
    fPOI: Integer;
    fGeometryPoint: TWDGeometryPoint; // owns
    function getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string; override;
    function getValidGeometry: Boolean; override;
    function getExtent: TWDExtent; override;
  public
    function Encode: TByteBuffer; override;
  public
    property POI: Integer read fPOI;
    property geometryPoint: TWDGeometryPoint read fGeometryPoint;
    function distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double; override;
    function intersects(aGeometry: TWDGeometry): Boolean; override;
    procedure translate(dx, dy: Double); override;
  end;

  TGeometryLayerObject = class(TLayerObjectWithID)
  constructor Create(aLayer: TLayer; const aID: TWDID; aGeometry: TWDGeometry; aValue: Double);
  destructor Destroy; override;
  protected
    fGeometry: TWDGeometry; // owns
    fValue: Double; // value to lookup color within palette of layer
  protected
    function getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string; override;
    function getValidGeometry: Boolean; override;
    function getExtent: TWDExtent; override;
  public
    function Encode: TByteBuffer; override;
  public
    property geometry: TWDGeometry read fGeometry;
    property value: Double read fValue write fValue;
    function distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double; override;
    function intersects(aGeometry: TWDGeometry): Boolean; override;
    procedure translate(dx, dy: Double); override;
  end;

  TLayerUpdateObject = class
  constructor Create(const aID: TWDID);
  destructor Destroy; override;
  private
    fID: TWDID;
    fAttributes: TDictionary<string, string>;
  public
    property ID: TWDID read fID;
    property attributes: TDictionary<string, string> read fAttributes;
  end;

  TAttrNameValue = record
  class function Create(const aName, aValue: string): TAttrNameValue; static;
  public
    name: string;
    value: string;
  end;

  TLayerBase = class(TScenarioElement)
  constructor Create(
    aScenario: TScenario; const aDomain, aID, aName, aDescription: string;
    aDefaultLoad: Boolean=False; const aDisplayGroup: string=''; aShowInDomains: Boolean=True;
    aBasicLayer: Boolean=False; aOpacity: Double=0.8;
    const aLegendJSON: string=''; const aPreviewBase64: string='');
  protected
    fShowInDomains: Boolean; // for hiding layer in domains request (see switch layer)
    fBasicLayer: Boolean;
    fOpacity: Double;
    fLegendJSON: string;
    fPreviewBase64: string; // todo: conflict with getPreviewBase64 in descendent TLayer?
    function getJSON: string; override;
    procedure setLegendJSON(const aLegendJSON: string); virtual;
    function getPreviewBase64: string; virtual;
    procedure setPreviewBase64(const aValue: string); virtual;
    procedure handleUpdateLayerObject(aClient: TClient; aPayload: TJSONObject); virtual;
    procedure RegisterLayer; virtual;
    function LoadPreviewFromCache(const aPreviewFileName: string=''): Boolean;
  public
    property showInDomains: Boolean read fShowInDomains;
    property basicLayer: Boolean read fBasicLayer;
    property opacity: Double read fOpacity;
    property legendJSON: string read fLegendJSON write setLegendJSON;
    property previewBase64: string read getPreviewBase64 write setPreviewBase64;
  end;

  TExternalTilesLayer = class(TLayerBase)
  constructor Create(
    aScenario: TScenario; const aDomain, aID, aName, aDescription: string;
    const aTilesURL: string;
    aDefaultLoad: Boolean=False; const aDisplayGroup: string=''; aShowInDomains: Boolean=True;
    aBasicLayer: Boolean=False; aOpacity: Double=0.8;
    const aLegendJSON: string=''; const aPreviewBase64: string='');
  private
    fTilesURL: string;
  protected
    function getJSON: string; override;
  public
    property  tilesURL: string read fTilesURL;
  end;

  TSimpleLayer = class; // forward declaration

  TSimpleNameValue = TArray<string>; //cannot pass [] as parameter when using defined like this!

  TSimpleObject = class
  constructor Create(aSimpleLayer: TSimpleLayer; const aID, aClassName: string;
    aGeometry: TWDGeometryBase=nil; const aGeometryType: string='';
    const aOptions: System.TArray<TSimpleNameValue>=[];
    const aOtherProperties: System.TArray<TSimpleNameValue>=[]); overload;
  constructor Create(aSimpleLayer: TSimpleLayer; const aID, aClassName: string;
    aLat, aLon: Double; aHeight: Double=NaN;
    const aOptions: System.TArray<TSimpleNameValue>=[];
    const aOtherProperties: System.TArray<TSimpleNameValue>=[]); overload;
  destructor Destroy; override;
  private
    procedure setGeometry(const aValue: TWDGeometryBase);
  protected
    fSimpleLayer: TSimpleLayer;
    fID: string;
    fGeometry: TWDGeometryBase;
    fGeometryType: string; // set geometry type to support building json from geometry object gtxxx
    fOptions: TDictionary<string, string>;
    fOtherProperties: TDictionary<string, string>;
  public
    property id: string read fID;
    property geometry: TWDGeometryBase read fGeometry write setGeometry;
    property geometryType: string read fGeometryType write fGeometryType;
    property options: TDictionary<string, string> read fOptions;
    property otherProperties: TDictionary<string, string> read fOtherProperties;
    // options
    procedure addOptionString(const aName, aValue: string);
    procedure addOptionColor(const aName: string; aColor: TAlphaRGBPixel);
    procedure addOptionGeoColor(const aColor: TGeoColors);
    procedure addOptionInteger(const aName: string; aValue: Integer);
    procedure addOptionDouble(const aName: string; aValue: Double);
    procedure addOptionStructure(const aName, aValue: string);
    // other properties
    procedure addPropertyString(const aName, aValue: string);
    procedure addPropertyInteger(const aName: string; aValue: Integer);
    procedure addPropertyDouble(const aName: string; aValue: Double);
    procedure addPropertyStructure(const aName, aValue: string);
    // translate properties to json
    function jsonGeometryValue: string;
    function jsonGeometry: string;
    function jsonOptionsValue: string;
    function jsonOptions: string;
    function jsonOtherProperties: string;
    // json composed
    function jsonNewObject: string; virtual;
  end;

  TCircleMarker = class(TSimpleObject)
  constructor Create(aSimpleLayer: TSimpleLayer; const aID: string;
    aLat, aLon, aRadius: Double; aHeight: Double=NaN;
    const aOptions: System.TArray<TSimpleNameValue>=[];
    const aOtherProperties: System.TArray<TSimpleNameValue>=[]); overload;
  private
    function getRadius: Double;
    procedure setRadius(const aValue: Double);
  public
    property radius: Double read getRadius write setRadius;
  end;

  TSimpleLayer = class(TLayerBase)
  constructor Create(
    aScenario: TScenario; const aDomain, aID, aName, aDescription: string;
    const aOptions: System.TArray<TSimpleNameValue>=[];
    const aOtherProperties: System.TArray<TSimpleNameValue>=[];
    aDefaultLoad: Boolean=False; const aDisplayGroup: string=''; aShowInDomains: Boolean=True;
    aBasicLayer: Boolean=False; aOpacity: Double=0.8; const aLegendJSON: string='';
    aLayerUpdateFrequency: Double=DefaultLayerUpdateFrequency);
  destructor Destroy; override;
  private
    fLayerUpdateTimer: TTimer;
    // json commands stores for changes to objects
    fObjectsAdded: TDictionary<string, string>; //locks with TMonitor -> fObjects > fObjectsAdded
    fObjectsUpdated: TObjectDictionary<string, TDictionary<string, string>>; //locks with TMonitor -> fObjects > fObjectsUpdated
    fObjectsRemoved: TDictionary<string, string>; // locks with TMonitor -> fObjects > fObjectsRemoved. We use a dictionary to quickly find the id to avoid duplicate removals
  protected
    fObjects: TObjectDictionary<string, TSimpleObject>; //locks with TMonitor
    fOptions: TDictionary<string, string>;
    fOtherProperties: TDictionary<string, string>;
    function getJSON: string; override;
    procedure setLegendJSON(const aLegendJSON: string); override;
    procedure setPreviewBase64(const aValue: string); override;
    function IsReceivingLayerUpdates(aClient: TClient): Boolean; virtual;
    procedure handleLayerUpdateTrigger(aTimer: TTimer; aTime: THighResTicks);
  public
    property objects: TObjectDictionary<string, TSimpleObject> read fObjects;
    property options: TDictionary<string, string> read fOptions;
    property otherProperties: TDictionary<string, string> read fOtherProperties;
    // translate properties to json
    function jsonOptionsValue: string;
    function jsonOptions: string;
    function jsonOtherProperties: string;
    // manipulate objects
    procedure AddObject(aObject: TSimpleObject; const aInitialization: string);
    function UpdateObject(aObject: TSimpleObject; const aName, aJSONValue: string): Boolean; overload;
    procedure RemoveObject(aObject: TSimpleObject);
    // update remote
    procedure triggerLayerUpdate();
    function HandleClientSubscribe(aClient: TClient): Boolean; override;
  end;

  TLayer = class(TLayerBase)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean;
    const aObjectTypes, aGeometryType, aLayerType: string; aShowInDomains: Boolean; aDiffRange: Double;
    aBasicLayer: Boolean=False; aOpacity: Double=0.8; const aDisplayGroup: string='default';
    const aLegendJSON: string=''; const aPreviewBase64: string=''; aPalette: TWDPalette=nil;
    aLayerUpdateFrequency: Double=DefaultLayerUpdateFrequency;
    aTilerMaxRefreshTime: Integer=DefaultTilerMaxRefreshTime);
  destructor Destroy; override;
  private
    fGeometryType: string;
    fDependentDiffLayers: TObjectList<TDiffLayer>; // refs
    fDiffRange: Double;
    fObjectTypes: string;
    fLayerType: string;
    fPreviewRequestTimer: TTimer;
    fSendRefreshTimer: TTImer;
    fLayerUpdateTimer: TTimer;
  public
    property PreviewRequestTimer: TTimer read fPreviewRequestTimer;
    property SendRefreshTimer: TTImer read fSendRefreshTimer;
    property LayerUpdateTimer: TTimer read fLayerUpdateTimer;
  private
    fTilerRefreshTime: Integer;
    fTilerPreviewTime: Integer;

    fObjectsLock: TOmniMREW;
    fObjects: TObjectDictionary<TWDID, TLayerObjectWithID>; // owns

    fObjectsAdded: TObjectDictionary<TWDID, TLayerUpdateObject>; // owns
    fObjectsUpdated: TObjectDictionary<TWDID, TLayerUpdateObject>; // owns
    fObjectsDeleted: TObjectList<TLayerObjectWithID>; // owns

    fExtraJSON2DAttributes: string;
  protected
    fQuery: string;
    fTilerLayer: TTilerLayer;
    fPalette: TWDPalette; // work-a-round for layers without tiler support
    fDiffPalette: TWDPalette;
    function getJSON: string; override;
    function getPreviewBase64: string; override;
    function getObjectsJSON: string; virtual;
    function getRefJSON: string; virtual;

    procedure handleTilerInfo(aTilerLayer: TTilerLayer);
  	procedure handleTilerRefresh(aTilerLayer: TTilerLayer; aTimeStamp: TDateTime; aImmediate: Boolean);
  	procedure handleTilerPreview(aTilerLayer: TTilerLayer);
    procedure handleRefreshTrigger(aTimeStamp: TDateTime);
  public
    procedure handleLayerUpdateTrigger(aTimer: TTimer; aTime: THighResTicks);
    procedure triggerLayerUpdate();

    property objects: TObjectDictionary<TWDID, TLayerObjectWithID> read fObjects;
    property objectsLock: TOmniMREW read fObjectsLock;
    property geometryType: string read fGeometryType;
    property objectsJSON: string read getObjectsJSON;

    property layerType: string read fLayerType;

    property tilerRefreshTime: Integer read fTilerRefreshTime write fTilerRefreshTime;
    property tilerPreviewTime: Integer read fTilerPreviewTime write fTilerPreviewTime;

    //
    property objectTypes: string read fObjectTypes;
    property query: string read fQuery write fQuery; // r/w
    property tilerLayer: TTilerLayer read fTilerLayer;
    property palette: TWDPalette read fPalette write fPalette; // work-a-round
    property refJSON: string read getRefJSON;
    property diffRange: Double read fDiffRange;
    property diffPalette: TWDPalette read fDiffPalette write fDiffPalette;
    function uniqueObjectsTilesLink: string;
    function SliceType: Integer; virtual;
    property extraJSON2DAttributes: string read fExtraJSON2DAttributes write fExtraJSON2DAttributes;
  public
    function FindObject(const aID: TWDID; out aObject: TLayerObjectWithID): Boolean;
    function AddObject(aObject: TLayerObjectWithID): TLayerObjectWithID;
    procedure AddObjectAttribute(const aID: TWDID; const aAttributes: TArray<TAttrNameValue>);
    procedure UpdateObjectAttribute(const aID: TWDID; const aAttribute, aValue: string);
    procedure RemoveObject(aObject: TLayerObjectWithID);
    function RemoveObjectOnID(const aID: TWDID): Boolean;
    procedure RemoveObjects(const aIDs: TArray<TWDID>);
    function ExtractObject(aObject: TLayerObjectWithID): TLayerObjectWithID;
    procedure ClearObjects;
    function findNearestObject(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; var aDistance: Double; out aSide: Integer): TLayerObjectWithID;
    function findObjectsInCircle(const aDistanceLatLon: TDistanceLatLon; aX, aY, aRadius: Double; var aObjectsJSON: string): Integer;
    function findObjectsInGeometry(const aGeometryExtent: TWDExtent; aGeometry: TWDGeometry; var aObjectsJSON, aObjectsID: string): Integer;
    //function signalObjectsInGeometry(const aGeometryExtent: TWDExtent; aGeometry: TWDGeometry; aTilerLayer: TTilerLayer): Integer;

  public
    procedure ReadObjectsDBSVGPaths(aQuery: TDataSet; aDefaultValue: Double);

    // signal to tiler
    procedure signalObject(aObject: TLayerObjectWithID); virtual;
    procedure signalNoObject(aObject: TLayerObjectWithID); virtual;
    procedure signalObjects(aSender: TObject; aObjects: TObjectDictionary<TWDID, TLayerObjectWithID>); overload; virtual;
    procedure signalObjects(aSender: TObject); overload; virtual;

    procedure RegisterOnTiler(aPersistent: Boolean; aSliceType: Integer; const aDescription: string; aEdgeLengthInMeters: Double=NaN; aPalette: TWDPalette=nil);

    procedure RegisterLayer; override; // override to call -> RegisterOnTiler(XX)
    procedure RegisterSlice; virtual; // override to call -> TTilerLayer.addSliceXX
    // registerLayer (override) -> registerOnTiler -> onTilerInfo -> RegisterSlice (override -> signalAddSlice) + signalObjects via timer

    procedure addDiffLayer(aDiffLayer: TDiffLayer);
    procedure removeDiffLayer(aDiffLayer: TDiffLayer);

    function HandleClientSubscribe(aClient: TClient): Boolean; override;
    function HandleClientUnsubscribe(aClient: TClient): Boolean; override;
  end;

  TLayerOnZoom = record
  class function Create(aZoomLevel: Double; aLayer: TLayer): TLayerOnZoom; static;
  public
    zoomLevel: Double;
    layer: TLayer; // ref
  end;

  TLayerSwitch = class(TLayer)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean;
    const aObjectTypes: string; aBasicLayer: Boolean);
  destructor Destroy; override;
  private
    // most zoomed out is lowest zoom level is first in list -> use that for preview
    fZoomLayers: TList<TLayerOnZoom>;
  protected
    function getJSON: string; override;
  public
    property zoomLayers: TList<TLayerOnZoom> read fZoomLayers; // refs to layers
  end;

  TKPI = class(TScenarioElement)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aDisplayGroup: string='default');
  destructor Destroy; override;
  private
    fTitle: string;
    fSubtitle: string;
    fRanges: TArray<double>;
    fMeasures: TArray<double>;
    fMarkers: TArray<double>;
  protected
    function getJSON: string; override;
  public
    property title: string read fTitle write fTitle;
    property Subtitle: string read fSubtitle write fSubtitle;
    property Ranges: TArray<double> read fRanges write fRanges;
    property Measures: TArray<double> read fMeasures write fMeasures;
    property Markers: TArray<double> read fMarkers write fMarkers;

    procedure Update;
  end;

  TChartAxis = class
  constructor Create(const aLabel, aColor, aQuantity, aUnit: string; aSet: Boolean = False; aMin: Double = NaN; aMax: Double = NaN);
  private
    fLabel: string;
    fColor: string;
    fQuantity: string;
    fUnit: string;
    fSet: Boolean;
    fMin, fMax: Double; //make functionality to be able to change these values after a graph is created?
  protected
    function getJSON: string; virtual;
  public
    property _label: string read fLabel write fLabel;
    property color: string read fColor write fColor;
    property quantity: string read fQuantity write fQuantity;
    property _unit: string read fUnit write fUnit;
    property toJSON: string read getJSON;
  end;

  TChartValue = record
  class function Create(aX: Double; const aY: TArray<Double>): TChartValue; static;
  private
    function getJSON: string;
  public
    x: Double;
    y: TArray<Double>;
    property toJSON: string read getJSON;
  end;

  TChartValues = TList<TChartValue>;

  TChart = class(TScenarioElement)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aChartType: string; const aDisplayGroup: string='default');
  destructor Destroy; override;
  protected
    fChildCharts: TDictionary<string, TChart>; //owns, locks with TMonitor
    fChartType: string;
    fPreviewRequestTimer: TTimer;
    function getJSON: string; override;
    function getJSONData: string; virtual; abstract;
  public
    property chartType: string read fChartType;
    procedure reset; virtual;
    procedure HandleGraphLabelClick(aClient: TClient; aLabelTitle: string); virtual;
    procedure Show();
  end;

  TSpiderData = class;

  TSpiderDataValue = class
  constructor Create(aValue: Double; const aLink: string; aData: TSpiderData);
  destructor Destroy; override;
  private
    fValue: Double;
    fLink: string;
    fData: TSpiderData;
  public
    property value: Double read fValue write fValue;
    property link: string read fLink write fLink;
    property data: TSpiderData read fData write fData;
  end;


  TSpiderChart  = class(TChart)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean);
  destructor Destroy; override;
  private
    fWidth: Integer;
    fHeight: Integer;
    fMaxScale: Double;
  protected
    fData: TSpiderData;
  protected
    function getJSON: string; override;
    function getJSONData: string; override;
  public
    property data: TSpiderData read fData;
    procedure normalize;
    procedure RecalculateAverages;
  end;

  TSpiderData = class
  constructor Create(const aTitle: string);
  destructor Destroy; override;
  private
    fTitle: string;
    fLabels: TDictionary<string, Integer>;
    fAxes: TDictionary<string, Integer>;
    fValues: TObjectList<TObjectList<TSpiderDataValue>>; // label, axis

    function createValue(const aLabel, aAxis: string): TSpiderDataValue;
    function getValue(const aLabel, aAxis: string): TSpiderDataValue;
    function getValueData(const aLabel, aAxis: string): TSpiderData;
    procedure setValueData(const aLabel, aAxis: string; const Value: TSpiderData);
    function getValueLink(const aLabel, aAxis: string): string;
    procedure setValueLink(const aLabel, aAxis, Value: string);
    function getValueValue(const aLabel, aAxis: string): Double;
    procedure setValueValue(const aLabel, aAxis: string; const Value: Double);
  public
    property title: string read fTitle;
    property labels: TDictionary<string, Integer> read fLabels;
    property axes: TDictionary<string, Integer> read fAxes;
    property values: TObjectList<TObjectList<TSpiderDataValue>> read fValues write fValues;

    property value[const aLabel, aAxis: string]: TSpiderDataValue read getValue;
    property valueValue[const aLabel, aAxis: string]: Double read getValueValue write setValueValue;
    property valueLink[const aLabel, aAxis: string]: string read getValueLink write setValueLink;
    property valueData[const aLabel, aAxis: string]: TSpiderData read getValueData write setValueData;

    function GetOrAddValue(const aLabel, aAxis: string): TSpiderDataValue;

    function getJSON: string;

    function maxValue: Double;
    procedure normalize(aMaxValue, aMaxScale: Double); overload; // single
    procedure normalize(aMaxScale: Double); overload; // recursive
    function RecalculateAverages: Double;
  end;

  TChartLines = class(TChart)
  constructor Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aChartType: string;
    aXAxis: TChartAxis; aYAxes: TArray<TChartAxis>; const aXScale: string = chartDefaultXScale; aChartMaxUpdateTime: Integer=chartMaxUpdateTime;
    aShowAvgLine: Boolean=False; aYMax: Double=NaN; aYMin: Double=NaN);
  destructor Destroy; override;
  private
    fXAxis: TChartAxis;
    fYAxes: TObjectList<TChartAxis>;
    fXScale: string;
    fValues: TChartValues;
    fAllValues: TChartValues;
    fUpdateTimer: TTimer;
    fChartUpdateTime: Integer;
    fShowAvgLine: Boolean;
    fYMin: Double;
    fYMax: Double;
  protected
    function getJSON: string; override;
    function getJSONData: string; override;
  public
    property chartType: string read fChartType write fChartType;
    property xAxis: TChartAxis read fXAxis;
    property yAxes: TObjectList<TChartAxis> read fYAxes;
    property xScale: string read fXScale;
    property values: TChartValues read fValues;
    property allValues: TChartValues read fAllValues;

    property chartUpdateTime: Integer read fChartUpdateTime write fChartUpdateTime;
    procedure reset; override;
    procedure AddValue(aX: Double; const aY: TArray<Double>);
    //procedure AddValueSorted(aX: Double; const aY: TArray<Double>);
    procedure RecalculateChartValues;
  end;

  TTableChart = class(TChart)
  protected
    function getJSONData: string; override;
  end;

  TScenario = class(TClientSubscribable)
  constructor Create(aProject: TProject; const aID, aName, aDescription: string; aAddbasicLayers: Boolean; aMapView: TMapView);
  destructor Destroy; override;
  private
    fControls: TDictionary<string, string>; //locks with TMonitor -> always lock this first if you also lock the project controls!
  protected
    fProject: TProject; // ref
    fID: string;
    fName: string;
    fDescription: string;
    fMapView: TMapView;
    fLayers: TObjectDictionary<string, TLayerBase>; // owns, locks with TMonitor
    //fSelectionLayer: TLayer;
    fKPIs: TObjectDictionary<string, TKPI>; // owns
    fCharts: TObjectDictionary<string, TChart>; // owns, locks with TMonitor
    fAddbasicLayers: Boolean;
    function getElementID: string; override;
    function selectLayersOnCategories(const aSelectCategories: TArray<string>; aLayers: TList<TLayerBase>): Boolean;
    function splitLayersOnCategories(const aSelectCategories: TArray<string>; aInCategories, aNotInCategories: TList<TLayerBase>): Boolean;
    procedure HandleNewMapView(aClient: TClient; aMapView: TMapView); virtual;
  public
    property project: TProject read fProject;
    property ID: string read fID;
    property name: string read fName write fName;
    property description: string read fDescription write fDescription;
    property mapView: TMapView read fMapView write fMapView;
    property Layers: TObjectDictionary<string, TLayerBase> read fLayers;
    property KPIs: TObjectDictionary<string, TKPI> read fKPIs;
    property Charts: TObjectDictionary<string, TChart> read fCharts;
    property addBasicLayers: Boolean read fAddBasicLayers;
    procedure ReadBasicData(); virtual;
    procedure registerLayers;
  public
    property controls: TDictionary<string, string> read fControls;
  protected
    function getControl(const aControlName: string): string;
    procedure setControl(const aControlName, aControlValue: string);
  public
    property Control[const aControlName: string]: string read getControl write setControl;
    procedure EnableControl(const aControlName: string);
    procedure DisableControl(const aControlName: string);
  public
    function AddLayer(aLayer: TLayerBase): TLayerBase;
    function AddKPI(aKPI: TKPI): TKPI;
    function AddChart(aChart: TChart): TChart;
  public
    function HandleClientSubscribe(aClient: TClient): Boolean; override;
    function HandleClientUnsubscribe(aClient: TClient): Boolean; override;
  public
    procedure HandleClientGraphLabelClick(aClient: TClient; aPayload: TJSONValue);
    procedure LayerRefreshed(aTilerLayerID: Integer; const aElementID: string; aTimeStamp: TDateTime; aLayer: TSubscribeObject); virtual;
  public
    // select objects
    function SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories, aPrefCategories: TArray<string>; aGeometry: TWDGeometry): string; overload; virtual;
    function SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories, aPrefCategories: TArray<string>; aX, aY, aRadius, aZoom: Double): string; overload; virtual;
    function SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories: TArray<string>; aJSONQuery: TJSONArray): string; overload; virtual;
    function SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories: TArray<string>; const aSelectedIDs: TArray<string>): string; overload; virtual;
    // select object properties
    function selectObjectsProperties(aClient: TClient; const aSelectCategories, aSelectedObjects: TArray<string>): string; virtual;
  end;

  TScenarioLink = class
  constructor Create(const aID, aParentID, aReferenceID: String; const aName, aDescription, aStatus: string; aLink: TScenario);
  destructor Destroy; override;
  private
    fChildren: TObjectList<TScenarioLink>;
    fLink: TScenario;
    fParent: TScenarioLink;
    fID: string; //Integer;
    fParentID: string; //Integer;
    fReferenceID: string; //Integer;
    fName: string;
    fDescription: string;
    fStatus: string;
  public
    property children: TObjectList<TScenarioLink> read fChildren;
    property link: TScenario read fLink write fLink;
    property parent: TScenarioLink read fParent write fParent;
    // scenario
    property ID: string read fID;
    property parentID: string read fParentID;
    property referenceID: string read fReferenceID;
    function name: string;
    property description: string read fDescription;
    property status: string read fStatus;

    function root: TScenarioLink;
    function findScenario(const aID: string): TScenarioLink;
    function removeLeaf(const aStatus: string): Boolean;
    procedure buildHierarchy();
    procedure sortChildren();
    function JSON: string;
  end;

  TMeasureAction = class(TObject)
  constructor Create(const aID, aDescription, aObjectTypes, aAction, aActionParameters: string;
    aActionID: Integer);
  private
    fID: string;
    fDescription: string;
    fObjectTypes: string;
    fAction: string;
    fActionParameters: string;
    fActionID: Integer;
  public
    property ID: string read fID;
    property description: string read fDescription;
    property objectTypes: string read fObjectTypes;
    property action: string read fAction;
    property actionParameters: string read fActionParameters;
    property actionID: Integer read fActionID;
    function getActionJSON: string;
  end;

  TMeasure = class(TObject)
  constructor Create(const aID, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters: string; aActionID: Integer);
  destructor Destroy; override;
  private
    fMeasure: string;
    fActions: TObjectDictionary<string, TMeasureAction>; //locks with TMonitor;
  public
    procedure AddAction(const aID, aDescription, aObjectTypes, aAction, aActionParameters: string; aActionID: Integer);
    procedure RemoveAction(const aID: string);
    function FindMeasure(const aActionID: string; out aMeasure: TMeasureAction): Boolean;
    function getMeasureJSON: string;
  end;

  TMeasureCategory = class(TObject)
  constructor Create(const aID, aCategory, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters: string; aActionID: Integer);
  destructor Destroy; override;
  private
    fCategory: string;
    fMeasures: TObjectDictionary<string, TMeasure>; //locks with TMonitor;
  public
    procedure AddMeasure(const aID, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters: string; aActionID: Integer);
    procedure RemoveMeasure(const aMeasure, aID);
    function FindMeasure(const aActionID: string; out aMeasure: TMeasureAction): Boolean;
    function GetCategoryJSON: string;
  end;

  TMeasureHistory = class(TObject)
  constructor Create(const aMeasureID, aCategory, aMeasure, aAction, aObjectIDs, aSelectCategories, aScenarioID: string; const aUTCTime: TDateTime); overload;
  constructor Create(aMeasure: TMeasureAction; const objectIDs, aSelectCategories: string); overload;
  private
    //fID: string; todo: add MeasureHistory ID
    fMeasureID: string;
    fCategory: string;
    fMeasure: string;
    fAction: string;
    fObjectIDs: string;
    fSelectCategories: string;
    fScenarioID: string;
    fUTCTime: TDateTime;
  public
    property MeasureID: string read fMeasureID;
    property Category: string read fCategory;
    property Measure: string read fMeasure;
    property Action: string read fAction;
    property ObjectIDs: string read fObjectIDs;
    property SelectCategories: string read fSelectCategories;
    property ScenarioID: string read fScenarioID;
    property UTCTime: TDateTime read fUTCTime;
  end;

  TWindControl = class
  constructor Create(aProject: TProject; aWindDirection, aWindSpeed: double; aLive: Boolean=True);
  private
    fProject: TProject;
    fWindDirection: double;
    fWindSpeed: double;
    fLive: Boolean;
    procedure setWindDirection(const aValue: double);
    procedure setWindSpeed(const aValue: double);
    procedure setLive(const aValue: Boolean);
  public
    property windDirection: double read fWindDirection write setWindDirection;
    property windSpeed: double read fWindSpeed write setWindSpeed;
    property live: Boolean read fLive write setLive;
    procedure updateClient(aWindDirection, aWindSpeed: double; aLive: Integer; aClient: TClient);
    procedure update(aWindDirection, aWindSpeed: double; aLive: Integer=-1; aSendingClient: TClient=nil);
  end;

  TClientMessageHandler = reference to procedure(aProject: TProject; aClient: TClient; const aType: string; aPayload: TJSONValue);

  TProject = class(TSubscribeObject)
  constructor Create(aSessionModel: TSessionModel; aConnection: TConnection; const aProjectID, aProjectName, aTilerFQDN, aTilerStatusURL: string;
    aDBConnection: TCustomConnection;
    aAddBasicLayers: Boolean; aMaxNearestObjectDistanceInMeters: Integer; aMapView: TMapView);
  destructor Destroy; override;
  private
    fMapView: TMapView;
    fControls: TDictionary<string, string>; //locks with TMonitor
    procedure setProjectName(const aValue: string);
    procedure setProjectDescription(const aValue: string);
    procedure setMapView(const aValue: TMapView);
  public
    property mapView: TMapView read fMapView write setMapView;
  protected
    fSessionModel: TSessionModel; // ref
    fConnection: TConnection; // ref
    fProjectID: string;
    fProjectName: string;
    fProjectDescription: string;
    fMaxNearestObjectDistanceInMeters: Integer;
    fTiler: TTiler;
    fDBConnection: TCustomConnection; // owns
    fProjectEvent: TEventEntry;
    fProjectEventHandler: TOnIntString;
    fClients: TGroup; // owns, lock with TMonitor
    fTimers: TTimerPool;
    fDiffLayers: TObjectDictionary<string, TDiffLayer>; // owns, lock with TMonitor
    // data
    // todo: not used so removed for now.. fMeasuresJSON: string;
    fMeasures: TObjectDictionary<string, TMeasureCategory>; // owns, locks with TMonitor
    // todo: what should be the key of measuresHistory??
    fScenarios: TObjectDictionary<string, TScenario>; // owns, lock with TMonitor
    fScenarioLinks: TScenarioLink;
    fProjectCurrentScenario: TScenario; // ref
    fProjectRefScenario: TScenario; // ref
    fAddBasicLayers: Boolean;

    fSimulationsetup: string;
    fWindControl: TWindControl;
    fDownloadableFiles: TDictionary<string, TDownloadableFileInfo>;
    fClientMessageHandlers: TDictionary<string, TClientMessageHandler>;

    function getMeasuresJSON: string; virtual;
    function getQueryDialogDataJSON: string; virtual;
    function ReadScenario(const aID: string): TScenario; virtual;

    procedure handleTilerStartup(aTiler: TTiler; aStartupTime: TDateTime);
    procedure timerTilerStatusAsHeartbeat(aTimer: TTimer; aTime: THighResTicks);
    function getWindControl: TWindControl;
    function getMeasuresHistoryJSON: string; virtual;
  private
    function getClientURL: string;
  protected
    procedure handleClientMessage(aClient: TClient; aScenario: TScenario; aJSONObject: TJSONObject); virtual;
    procedure handleTypedClientMessage(aClient: TClient; const aMessageType: string; var aJSONObject: TJSONObject); virtual;
    procedure handleNewClient(aClient: TClient); virtual;
    procedure handleRemoveClient(aClient: TClient); virtual;

    procedure HandleClientGraphLabelClick(aProject: TProject; aClient: TClient; const aType: string; aPayload: TJSONValue);
    procedure handleFileUpload(aClient: TClient; const aFileInfo: TClientFileUploadInfo); virtual;
  public
    function addClient(const aClientID: string): TClient; virtual;
    function addOrGetClient(const aClientID: string): TClient;
    procedure ReadBasicData(); virtual;
    function isAuthorized(aClient: TClient; const aToken: string): Boolean; virtual;
    procedure handleNotAuthorized(aClient: TClient; aMessage: TJSONObject; const aToken: string); virtual;
  public
    property Connection: TConnection read fConnection;
    property dbConnection: TCustomConnection read fDBConnection;
    property ProjectID: string read fProjectID;
    property ProjectName: string read fProjectName write setProjectName;
    property ProjectDescription: string read fProjectDescription write setProjectDescription;
    property ProjectEvent: TEventEntry read fProjectEvent;
    property maxNearestObjectDistanceInMeters: Integer read fMaxNearestObjectDistanceInMeters;
    property tiler: TTiler read fTiler;
    property Timers: TTimerPool read fTimers;
    property scenarios: TObjectDictionary<string, TScenario> read fScenarios; // owns, lock with TMonitor
    property scenarioLinks: TScenarioLink read fScenarioLinks;
    property clients: TGroup read fClients; // owns, lock with TMonitor
    property downloadableFiles: TDictionary<string, TDownloadableFileInfo> read fDownloadableFiles;
    property ClientURL: string read getClientURL;

    // todo: signal clients on write?
    property projectCurrentScenario: TScenario read fProjectCurrentScenario write fProjectCurrentScenario;
    property projectRefScenario: TScenario read fProjectRefScenario write fProjectRefScenario;

    property measuresJSON: string read getMeasuresJSON;
    property measures: TObjectDictionary<string, TMeasureCategory> read fMeasures;
    procedure AddMeasure(const aID, aCategory, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters: string; const aActionID: Integer);

    property queryDialogDataJSON: string read getQueryDialogDataJSON;
    property addBasicLayers: Boolean read fAddBasicLayers;
    property windControl: TWindControl read getWindControl;
    property clientMessageHandlers: TDictionary<string, TClientMessageHandler> read fClientMessageHandlers;

    procedure Login(aClient: TClient; aJSONObject: TJSONObject); virtual;
  public
    property diffLayers: TObjectDictionary<string, TDiffLayer> read fDiffLayers;
    function diffElementID(aCurrent, aReference: TScenarioElement): string;
  public
    procedure SendString(const aString: string);
  public
    property controls: TDictionary<string, string> read fControls;
  protected
    function getControl(const aControlName: string): string;
    procedure setControl(const aControlName, aControlValue: string);
  public
    property Control[const aControlName: string]: string read getControl write setControl;
    procedure EnableControl(const aControlName: string);
    procedure DisableControl(const aControlName: string);
  public
    procedure forEachClient(aForEachClient: TForEachClient);
    procedure addDownloadableFile(const aName: string; const aFileTypes: TArray<string>; aFileGenerator: TFileGenerator);
    procedure removeDownloadableFile(const aName: string);
  private
    fGroups: TObjectDictionary<string, TGroup>;
  public
    // group mamanagement
    property groups: TObjectDictionary<string, TGroup> read fGroups;
    function addGroup(const aGroup: string; aPresenter: TClient): Boolean;
    function addGroupMember(const aGroup: string; aMember: TClient): Boolean;
    function getGroupMembersNoLocking(const aGroup: string; aMembers: TGroup): Boolean; // NO locking in function!
    function isPresenterNoLocking(const aGroup: string; aMember: TClient): Boolean; // NO locking in function!
    function removeGroupMember(const aGroup: string; aMember: TClient): Boolean;
    function removeGroupNoLocking(const aGroup: string; aNoSendCloseMember: TClient): Boolean; // NO locking in function!
    function removeMemberFromAllGroups(aMember: TClient): Boolean;
  end;

  // empty model control entries
  //TModelParameters = class
  //end;

  TModel = class
  constructor Create(aConnection: TConnection);
  private
    fConnection: TConnection;
  public
    property Connection: TConnection read fConnection;
  end;

  TSessionModel = class(TModel)
  constructor Create(aConnection: TConnection);
  destructor Destroy; override;
  private
    fProjects: TObjectList<TProject>;
  public
    property Projects: TObjectList<TProject> read fProjects;
    function FindElement(const aElementID: string): TClientSubscribable;
  end;

function DoubleToJSON(d: Double): string;

function isObject(aJSONObject: TJSONObject; const aObjectName: string; var aJSONPair: TJSONPair): Boolean;
function isObjectValue(aJSONObject: TJSONObject; const aValueName: string; var aJSONValue: TJSONValue): Boolean;

function jsonOrNull(const aJSON: string): string;
procedure jsonAdd(var aCurrent: string; const aAdd: string);
function jsonAddCommaOnNonEmpty(const aJSON: string): string;
procedure jsonAddInteger(var aCurrent: string; const aName: string; aValue: Integer);
procedure jsonAddDouble(var aCurrent: string; const aName: string; aValue: Double);
procedure jsonAddString(var aCurrent: string; const aName: string; const aValue: string; aAlwaysAdd: Boolean=False);
procedure jsonAddStruct(var aCurrent: string; const aName: string; const aValue: string; aAlwaysAdd: Boolean=False);
function jsonArrayOfDoubleToStr(const aValues: TArray<double>): string;
function jsonString(const aText: string): string;
function geoJsonFeatureCollection(const aFeatures: string): string;
function geoJsonFeature(const aGeometry: string; const aProperties: string=''): string;

function ZoomLevelFromDeltaLon(aDeltaLon: Double): Integer;
function ZoomLevelFromDeltaLat(aDeltaLat: Double): Integer;

function BuildDiscreteLegendJSON(aPalette: TDiscretePalette; aLegendFormat: TLegendFormat): string;
function BuildRamplLegendJSON(aPalette: TRampPalette; aWidth: Integer=300; aLogScale: Boolean=False; aTickFontSize: Integer=11): string;
function CreateBasicPalette: TWDPalette;

function compareLayerNames(const aLayer1, aLayer2: TLayerBase): Integer;
function ConvertOptions(const aOptionsArray: TArray<TArray<string>>; aOptionsDictionary: TDictionary<string, string>): Boolean;

procedure DetectPeaks(aValues: TChartValues; aDelta: Double; aPeaks: TList<Integer>);

implementation

{ utils }

function DoubleToJSON(d: Double): string;
begin
  if d.IsNan
  then Result := 'null'
  else Result := d.toString(dotFormat);
end;

function jsonOrNull(const aJSON: string): string;
begin
  if aJSON<>''
  then Result := aJSON
  else Result := 'null';
end;

function isObject(aJSONObject: TJSONObject; const aObjectName: string; var aJSONPair: TJSONPair): Boolean;
begin
  aJSONPair := aJSONObject.Get(aObjectName);
  Result := Assigned(aJSONPair);
end;

function isObjectValue(aJSONObject: TJSONObject; const aValueName: string; var aJSONValue: TJSONValue): Boolean;
begin
  aJSONValue := aJSONObject.Values[aValueName];
  Result := Assigned(aJSONValue);
end;

procedure jsonAdd(var aCurrent: string; const aAdd: string);
begin
  if aAdd<>'' then
  begin
    if aCurrent<>''
    then aCurrent := aCurrent+','+aAdd
    else aCurrent := aAdd;
  end;
end;

function jsonAddCommaOnNonEmpty(const aJSON: string): string;
begin
  if aJSON<>''
  then Result := aJSON+','
  else Result := '';
end;

procedure jsonAddInteger(var aCurrent: string; const aName: string; aValue: Integer);
begin
  jsonAdd(aCurrent, '"'+aName+'":'+aValue.ToString);
end;

procedure jsonAddDouble(var aCurrent: string; const aName: string; aValue: Double);
begin
  jsonAdd(aCurrent, '"'+aName+'":'+DoubleToJSON(aValue));
end;

procedure jsonAddString(var aCurrent: string; const aName: string; const aValue: string; aAlwaysAdd: Boolean);
begin
  if aAlwaysAdd or (aValue<>'')
  then jsonAdd(aCurrent, '"'+aName+'":"'+aValue+'"');
end;

procedure jsonAddStruct(var aCurrent: string; const aName: string; const aValue: string; aAlwaysAdd: Boolean);
begin
  if aAlwaysAdd or (aValue<>'')
  then jsonAdd(aCurrent, '"'+aName+'":'+'{'+aValue+'}');
end;

function jsonArrayOfDoubleToStr(const aValues: TArray<double>): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to length(aValues)-1
  do jsonAdd(Result, Double.ToString(aValues[i], dotFormat));
end;

function jsonString(const aText: string): string;
var
  jsonS: TJSONString;
begin
  jsonS := TJSONString.Create(aText);
  try
    Result := jsonS.toJSON;
  finally
    jsonS.Free;
  end;
end;

function geoJsonFeatureCollection(const aFeatures: string): string;
begin
  Result := '{"type":"FeatureCollection",'+'"features":['+afeatures+']'+'}';
end;

function geoJsonFeature(const aGeometry, aProperties: string): string;
begin
  Result := '{"type":"Feature","geometry":'+aGeometry;
  jsonAdd(Result, aProperties);
  Result := Result+'}';
end;

function ZoomLevelFromDeltaLon(aDeltaLon: Double): Integer;
// todo: need correction for lat?
begin
  try
    if aDeltaLon>0 then
    begin
      if aDeltaLon<=180 then
      begin
        Result := Trunc(ln(360/aDeltaLon)/ln(2));
        if Result>19
        then Result := 19;
      end
      else Result := 0;
    end
    else Result := 19;
  except
    Result := 0;
  end;
end;

function ZoomLevelFromDeltaLat(aDeltaLat: Double): Integer;
begin
  try
    if aDeltaLat>0 then
    begin
      if aDeltaLat<=180 then
      begin
        Result := Trunc(ln(360/aDeltaLat)/ln(2));
        if Result>19
        then Result := 19;
      end
      else Result := 0;
    end
    else Result := 19;
  except
    Result := 0;
  end;
end;

function CalcPDDelta(aValues: TChartValues; aTreshholdFraction: Double): Double;
var
  v, maxValue, minValue: Double;
  i, f: Integer;
begin
  maxValue := Double.NegativeInfinity;
  minValue := Double.PositiveInfinity;
  for i := 0 to aValues.Count-1 do
  begin
    for f := 0 to length(aValues[i].y)-1 do
    begin
      v := aValues[i].y[f];
      if minValue>v
      then minValue := v;
      if maxValue>v
      then maxValue := v;
    end;
  end;
  if minValue<=maxValue
  then Result := (maxValue-minValue)*aTreshholdFraction
  else Result := 0;
end;

procedure DetectPeaks(aValues: TChartValues; aDelta: Double; aPeaks: TList<Integer>);
// http://billauer.co.il/peakdet.html
var
  fieldCount: Integer;
  l, f: Integer;
  lookForMax: TArray<Boolean>;
  mn: TArray<Double>;
  mx: TArray<Double>;
  mnIndex: TArray<Integer>;
  mxIndex: TArray<Integer>;
  p: Integer;
  v: Double;
begin
  if aValues.Count>2 then
  begin
    fieldCount := length(aValues[0].y);
    // make room for the given number of fields and init values
    setLength(mn, fieldCount);
    for f := 0 to fieldCount-1 do mn[f] := Infinity;
    setLength(mx, fieldCount);
    for f := 0 to fieldCount-1 do mx[f] := NegInfinity;
    setLength(mnIndex, fieldCount);
    for f := 0 to fieldCount-1 do mnIndex[f] := -1;
    setLength(mxIndex, fieldCount);
    for f := 0 to fieldCount-1 do mxIndex[f] := -1;
    setLength(lookForMax, fieldCount);
    for f := 0 to fieldCount-1 do LookForMax[f] := True; // arbitrary starting direction?
    // parse lines for specified field count per line
    for l := 0 to aValues.Count-1 do
    begin
      // parse fields
      for f := 0 to fieldCount-1 do
      begin
        // parse field value
        v := aValues[l].y[f];
        if not IsNaN(v) then
        begin
          // adjust min/max and store line index
          if v>mx[f] then
          begin
            mx[f] := v;
            mxIndex[f] := l;
          end;
          if v<mn[f] then
          begin
            mn[f] := v;
            mnIndex[f] := l;
          end;
          // find peak
          if lookForMax[f] then
          begin
            if v<mx[f]-aDelta then
            begin
              aPeaks.Add(mxIndex[f]);
              mn[f] := v;
              mnIndex[f] := l;
              lookForMax[f] := False;
            end;
          end
          else
          begin
            if v>mn[f]+aDelta then
            begin
              aPeaks.Add(mnIndex[f]);
              mx[f] := v;
              mxIndex[f] := l;
              lookForMax[f] := True;
            end;
          end;
        end;
      end;
    end;
    // do not check last point, will be handled as start in next series
    // remove double entries
    aPeaks.Sort;
    for p := aPeaks.Count-1 downto 1 do
    begin
      if aPeaks[p]=aPeaks[p-1]
      then aPeaks.Delete(p);
    end;
  end;
end;

{ utils }

function BuildDiscreteLegendJSON(aPalette: TDiscretePalette; aLegendFormat: TLegendFormat): string;
var
  i: Integer;
begin
  Result := '';
  case aLegendFormat of
    lfVertical:
      begin
        for i := 0 to length(aPalette.entries)-1
        do jsonAdd(Result, '{"'+aPalette.entries[i].description+'":{'+aPalette.entries[i].colors.toJSON+'}}');
        Result := '"grid":{"title":"'+aPalette.description+'","labels":['+Result+']}';
      end;
    lfHorizontal:
      begin
        for i := 0 to length(aPalette.entries)-1
        do jsonAdd(Result, '{"'+aPalette.entries[i].description+'":{'+aPalette.entries[i].colors.toJSON+'}}');
        Result := '"grid2":{"title":"'+aPalette.description+'","labels":['+Result+']}';;
      end;
  end;
end;

function BuildDiffLegendJSON(
  const aTitle: string;
  aValueLess: Double; const aColorLess, aLabelLess: string;
  aValueNoChange: Double; const aColorNoChange, aLabelNoChange: string;
  aValueMore: Double; const aColorMore, aLabelMore: string): string;
begin
  Result :=
    '"scale": {'+
      '"width": "300px",'+
      '"title":'+jsonString(aTitle)+','+
      '"logScale":0,'+
      '"tickFontSize": "11px",'+
      '"gradients":['+
        '{"color":"'+aColorLess+'","position":'+DoubleToJSON(aValueLess)+'},'+
        '{"color":"'+aColorNoChange+'","position":'+DoubleToJSON(aValueNoChange)+'},'+
        '{"color":"'+aColorMore+'","position":'+DoubleToJSON(aValueMore)+'}'+
      '],'+
      '"labels":['+
        '{"description":"'+aLabelLess+'","position":'+DoubleToJSON(aValueLess)+'},'+
        '{"description":"'+aLabelNoChange+'","position":'+DoubleToJSON(aValueNoChange)+'},'+
        '{"description":"'+aLabelMore+'","position":'+DoubleToJSON(aValueMore)+'}'+
      ']}';
end;

function BuildRamplLegendJSON(aPalette: TRampPalette; aWidth: Integer; aLogScale: Boolean; aTickFontSize: Integer): string;
var
  gradients: string;
  labels: string;
  e: TRampPaletteEntry;
begin
  gradients := '';
  labels := '';
  for e in aPalette.entries do
  begin
    jsonAdd(gradients, '{"color":"'+ColorToJSON(e.color)+'","position":'+DoubleToJSON(e.value)+'}');
    jsonAdd(labels, '{"description":'+jsonString(e.description)+',"position":'+DoubleToJSON(e.value)+'}');
  end;
  Result :=
    '"scale": {'+
      '"width": "'+aWidth.ToString+'px",'+
      '"title":'+jsonString(aPalette.description)+','+
      '"logScale":'+Ord(aLogScale).ToString+','+
      '"tickFontSize": "'+aTickFontSize.ToString+'px",'+
      '"gradients":['+gradients+
        //'{"color":"'+aColorLess+'","position":'+aValueLess.ToString(dotFormat)+'},'+
        //'{"color":"'+aColorNoChange+'","position":'+aValueNoChange.ToString(dotFormat)+'},'+
        //'{"color":"'+aColorMore+'","position":'+aValueMore.toString(dotFormat)+'}'+
      '],'+
      '"labels":['+labels+
        //'{"description":"'+aLabelLess+'","position":'+aValueLess.ToString(dotFormat)+'},'+
        //'{"description":"'+aLabelNoChange+'","position":'+aValueNoChange.ToString(dotFormat)+'},'+
        //'{"description":"'+aLabelMore+'","position":'+aValueMore.ToString(dotFormat)+'}'+
      ']'+
    '}';
end;

function CreateBasicPalette: TWDPalette;
begin
  Result := TDiscretePalette.Create('basic palette', [], TGeoColors.Create(colorBasicFill, colorBasicOutline));
end;

//go from pixel dist to meters dist using earth radius, latitude and zoom
function PixelsToMeters(const aPixels, aLat, aZoom: Double) : Double;
begin
  Result := aPixels * (earthRadiusMeters * Cos((aLat * (Pi / 180))) / Power(2, 8 + aZoom))
end;

{ TDistanceLatLon }

class function TDistanceLatLon.Create(aLat1InDegrees, aLat2InDegrees: Double): TDistanceLatLon;
begin
  Result := Create(((aLat1InDegrees + aLat2InDegrees) / 2) * PI / 180); // average converted to radians
end;

class function TDistanceLatLon.Create(aLatMidInRad: Double): TDistanceLatLon;
begin
  Result.m_per_deg_lat := 111132.954 - 559.822 * Cos(2 * aLatMidInRad) + 1.175 * Cos(4 * aLatMidInRad);
  Result.m_per_deg_lon := 111132.954 * Cos(aLatMidInRad);
end;

function TDistanceLatLon.distanceInMeters(aDeltaLat, aDeltaLon: Double): Double;
begin
  Result := sqrt(sqr(m_per_deg_lat * aDeltaLat) + sqr(m_per_deg_lon * aDeltaLon));
end;

{ TClientSubscribable }

constructor TClientSubscribable.Create;
begin
  inherited Create;
  fClientAmount := 0;
  fAmountLock.Create;
end;

destructor TClientSubscribable.Destroy;
begin
  inherited;
end;

function TClientSubscribable.HandleClientSubscribe(aClient: TClient): Boolean;
begin
  Result := False;
  if aClient.SubscribeTo(Self,
    procedure (aSender: TSubscribeObject; const aAction, aObjectID: Integer)
    begin
      exit; //empty function, client subscribes are only interested in regesitering for forEachClient calls
    end)
  then
  begin
    fAmountLock.BeginWrite;
    try
      Result := True;
      fClientAmount := fClientAmount + 1;
      if fClientAmount = 1 then
        HandleFirstSubscriber(aClient);
    finally
      fAmountLock.EndWrite;
    end;
  end;
end;

function TClientSubscribable.HandleClientUnsubscribe(aClient: TClient): Boolean;
begin
  Result := False;
  if aClient.UnsubscribeFrom(Self) then
  begin
    fAmountLock.BeginWrite;
    try
      Result := True;
      fClientAmount := fClientAmount - 1;
      if fClientAmount = 0 then
        HandleLastSubscriber(aClient);
    finally
      fAmountLock.EndWrite;
    end;
  end;
end;

procedure TClientSubscribable.HandleFirstSubscriber(aClient: TClient);
begin
  // default no action
end;

procedure TClientSubscribable.HandleLastSubscriber(aClient: TClient);
begin
  // default no action
end;

{ TDiffLayer }

constructor TDiffLayer.Create(const aElementID: string; aCurrentLayer, aReferenceLayer: TLayer; aTilerMaxRefreshTime: Integer);
begin
  inherited Create;
  fLegendJSON := '';
  fCurrentLayer := aCurrentLayer;
  fReferenceLayer := aReferenceLayer;
  // link to original layers
  fCurrentLayer.addDiffLayer(Self);
  fReferenceLayer.addDiffLayer(Self);
  fTilerLayer := TTilerLayer.Create(
    aCurrentLayer.scenario.project.Connection, aElementID, -aCurrentLayer.SliceType,
    handleTilerInfo, handleTilerRefresh, handleTilerPreview);
  // create timers
  fTilerRefreshTime := DefaultTilerRefreshTime;
  fTilerPreviewTime := DefaultTilerPreviewTime;
  fPreviewRequestTimer := aCurrentLayer.scenario.project.Timers.SetTimer(
    procedure(aTimer: TTImer; aTime: THighResTicks)
    begin
      Log.WriteLn('triggered preview timer for '+elementID);
      fTilerLayer.signalRequestPreview;
    end);
  fSendRefreshTimer := aCurrentLayer.scenario.project.Timers.CreateInactiveTimer;
  fSendRefreshTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond*aTilerMaxRefreshTime);
  handleSubLayerInfo(nil); // if both layers are known on tiler we can start registering now
end;

destructor TDiffLayer.Destroy;
begin
  if Assigned(fSendRefreshTimer) then
  begin
    fSendRefreshTimer.Cancel;
    fSendRefreshTimer := nil;
  end;
  if Assigned(fPreviewRequestTimer) then
  begin
    fPreviewRequestTimer.Cancel;
    fPreviewRequestTimer := nil;
  end;
  fCurrentLayer.removeDiffLayer(Self);
  fReferenceLayer.removeDiffLayer(Self);
  FreeAndNil(fTilerLayer); // owns
  inherited;
  fCurrentLayer := nil; // ref
  fReferenceLayer := nil; // ref
end;

function TDiffLayer.getElementID: string;
begin
  Result := fTilerLayer.elementID;
end;

function TDiffLayer.getRefJSON: string;
begin
  Result := '"id":"'+getElementID+'","tiles":"'+fTilerLayer.URLTimeStamped+'"';
  if Assigned(fCurrentLayer) and (fCurrentLayer.layerType<>'')
  then Result := Result+',"type":"'+fCurrentLayer.layerType+'"';
  if legendJSON<>''
  then Result := Result+',"legend":{'+legendJSON+'}';
end;

function TDiffLayer.HandleClientSubscribe(aClient: TClient): Boolean;
begin
  Result := inherited;
  //TODO: also send preview?
  aClient.SendRefresh(elementID, '', fTilerLayer.URLTimeStamped);
  aClient.SendLegend(elementID, '', Self.legendJSON);
end;

function TDiffLayer.HandleClientUnSubscribe(aClient: TClient): Boolean;
begin
  Result := inherited;
end;

procedure TDiffLayer.handleRefreshTrigger(aTimeStamp: TDateTime);
var
  timeStampStr: string;
  tiles: string;
begin
  if aTimeStamp<>0
  then timeStampStr := FormatDateTime(publisherDateTimeFormat, aTimeStamp)
  else timeStampStr := '';
  tiles := fTilerLayer.URLTimeStamped;
  Log.WriteLn('TDiffLayer.handleRefreshTrigger for '+elementID+' ('+timeStampStr+'): '+tiles);

  // signal refresh to layer client
  forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      aClient.SendRefresh(elementID, timeStampStr, tiles);
      aClient.SendLegend(elementID, timeStampStr, Self.legendJSON);
      Log.WriteLn('TDiffLayer.handleRefreshTrigger for '+elementID+', direct subscribed client: '+aClient.fClientID, llNormal, 1);
    end);
  fCurrentLayer.scenario.LayerRefreshed(tilerLayer.ID, elementID, aTimeStamp, Self);
  // todo: old system: signal current layer of diff layer refresh
  //TODO: rework layer subscription
  {
  if Assigned(fCurrentLayer) then
  begin
    fCurrentLayer.scenario.forEachClient(
      procedure(aClient: TClient)
      begin
        aClient.SendRefreshDiff(fCurrentLayer.elementID, timeStampStr, Self.refJSON);
        Log.WriteLn('TDiffLayer.handleRefreshTrigger for '+elementID+', current layer subscribed client: '+aClient.fClientID, llNormal, 1);
      end,
      Clients);
  end;
  }
end;

procedure TDiffLayer.handleSubLayerInfo(aLayer: TLayer);
begin
  if Assigned(tilerLayer) then
  begin
    if tilerLayer.URL=''
    then registerOnTiler
    else handleTilerInfo(tilerLayer);
  end;
end;

procedure TDiffLayer.handleTilerInfo(aTilerLayer: TTilerLayer);
var
  diffPalette: TWDPalette;
  v: Double;
  title: string;
begin
  if Assigned(fCurrentLayer.fDiffPalette) then
  begin
    diffPalette := fCurrentLayer.fDiffPalette.Clone;
    if diffPalette is TDiscretePalette then
      fLegendJSON := BuildDiscreteLegendJSON( diffPalette as TDiscretePalette, lfVertical)
    else if diffPalette is TRampPalette then
      fLegendJSON := BuildRamplLegendJSON( diffPalette as TRampPalette)
    else fLegendJSON := '';
  end
  else
  begin
    v := fCurrentLayer.diffRange;
    title := 'difference';
    if Assigned(fCurrentLayer.tilerLayer) and Assigned(fCurrentLayer.tilerLayer.palette)
    then title := title+', '+FormatLegendDescription(fCurrentLayer.tilerLayer.palette.description);
    diffPalette := TRampPalette.Create(title, [
      TRampPaletteEntry.Create($FF00FF00, -v, 'less'),
      TRampPaletteEntry.Create($FFFFFFFF, 0.0, 'no change'),
      TRampPaletteEntry.Create($FFFF0000, v, 'more')],
      $FF00FF00, 0, $FFFF0000);

    fLegendJSON := BuildDiffLegendJSON(
      diffPalette.description,
      -v, '#00FF00', (-v).ToString(dotFormat),
      0.0, '#FFFFFF', 'no change (0)',
      v, '#FF0000', v.ToString(dotFormat));
  end;
  if Assigned(fCurrentLayer.tilerLayer) and Assigned(fReferenceLayer.tilerLayer)
  then aTilerLayer.signalAddDiffSlice(diffPalette, fCurrentLayer.tilerLayer.ID, fReferenceLayer.tilerLayer.ID)
  else
  begin
    if not Assigned(fCurrentLayer.tilerLayer)
    then Log.WriteLn('TDiffLayer.handleTilerInfo: fCurrentLayer.tilerLayer=nil on '+ElementID, llWarning);
    if not Assigned(fReferenceLayer.tilerLayer)
    then Log.WriteLn('TDiffLayer.handleTilerInfo: fReferenceLayer.tilerLayer=nil on '+ElementID, llWarning);
  end;
end;

procedure TDiffLayer.handleTilerPreview(aTilerLayer: TTilerLayer);
var
  pvBASE64: string;
begin
  if Assigned(fTilerLayer) then
  begin
    pvBASE64 := fTilerLayer.previewAsBASE64;
    // layer clients
    forEachSubscriber<TClient>(
      procedure(aClient: TClient)
      begin
        aClient.SendPreview(elementID, pvBASE64);
      end);
    {
    if Assigned(fCurrentLayer) then
    begin
      fCurrentLayer.scenario.forEachSubscriber<TClient>(
        procedure(aClient: TClient)
        begin
          aClient.SendPreview(elementID, pvBASE64);
        end);
    end;
    }
    Log.WriteLn('send diff preview on '+elementID);
  end;
end;

procedure TDiffLayer.handleTilerRefresh(aTilerLayer: TTilerLayer; aTimeStamp: TDateTime; aImmediate: Boolean);
begin
  try
    if Assigned(fSendRefreshTimer) then
    begin
      if aImmediate  then
      begin
        fSendRefreshTimer.Arm(DateTimeDelta2HRT(dtOneSecond*ImmediateTilerRefreshTime),
          procedure (aTimer: TTimer; aTime: THighResTicks)
          begin
            handleRefreshTrigger(aTimeStamp);
          end);
      end
      else
      begin
        fSendRefreshTimer.Arm(DateTimeDelta2HRT(dtOneSecond*tilerRefreshTime),
          procedure (aTimer: TTimer; aTime: THighResTicks)
          begin
            handleRefreshTrigger(aTimeStamp);
          end);
      end;
    end
    else handleRefreshTrigger(aTimeStamp);
    // refresh preview also
    if Assigned(fPreviewRequestTimer) then
    begin
      fPreviewRequestTimer.DueTimeDelta := DateTimeDelta2HRT(dtOneSecond*tilerPreviewTime);
      Log.WriteLn('TDiffLayer.handleTilerRefresh: triggered diff preview timer for '+elementID);
    end
    else
    begin
      Log.WriteLn('TDiffLayer.handleTilerRefresh:  direct trigger diff preview for '+elementID);
      fTilerLayer.signalRequestPreview;
    end;
  except
    on E: Exception
    do Log.WriteLn('Exception in TDiffLayer.handleTilerRefresh for ('+self.elementID+'): '+E.Message, llError);
  end;
end;

procedure TDiffLayer.registerOnTiler;
var
  currentLayerTilerUrl: string;
  referenceLayerTilerUrl: string;
begin
  if Assigned(fTilerLayer) and Assigned(fCurrentLayer) and Assigned(fReferenceLayer)
  then fTilerLayer.signalRegisterLayer(fCurrentLayer.scenario.project.tiler, 'diff-'+fCurrentLayer.Description+'-'+fReferenceLayer.description)
  else
  begin
    if Assigned(fCurrentLayer) and Assigned(fCurrentLayer.tilerLayer)
    then currentLayerTilerUrl := fCurrentLayer.tilerLayer.URL
    else currentLayerTilerUrl := '##';

    if Assigned(fReferenceLayer) and Assigned(fReferenceLayer.tilerLayer)
    then referenceLayerTilerUrl := fReferenceLayer.tilerLayer.URL
    else referenceLayerTilerUrl := '##';

    Log.WriteLn('TDiffLayer.registerOnTiler '+elementID+', '+Assigned(fTilerLayer).ToString()+': '+currentLayerTilerUrl+', '+referenceLayerTilerUrl, llWarning);
  end;
end;

{ TClientDomain }

class function TClientDomain.Create(const aName: string): TClientDomain;
begin
  Result.name := aName;
  Result.kpis := '';
  Result.charts := '';
  Result.layers := '';
  Result.enabled := 0;
end;

function TClientDomain.getJSON: string;
begin
  Result := '"'+name+'":{"enabled":'+enabled.ToString+',"layers":['+layers+'],"kpis":['+kpis+'],"charts": ['+charts+']}';
end;

{ TFormDialogResultHandling }

class function TFormDialogResultHandling.Create(aHandler: TOnFormDialogResult; aRunOnce: Boolean): TFormDialogResultHandling;
begin
  Result.Handler := aHandler;
  Result.RunOnce := aRunOnce;
end;

{ TFormDialogResultProperty }

constructor TFormDialogResultProperty.Create(const aName, aValue, aType: string);
begin
  inherited Create;
  fName := aName;
  fValue := aValue;
  fType := aType;
end;

{ TFormDialogResults }

constructor TFormDialogResults.Create(const aFormID: string; aResultParameters: TJSONValue; aContext: TJSONValue);
var
  arrayOfResults: TJSONArray;
  a: Integer;
  r: TJSONValue;
  ro: TJSONObject;
  n: string;
  v: string;
  t: string;
begin
  inherited Create;
  fFormID := aFormID;
  fProperties := TObjectDictionary<string, TFormDialogResultProperty>.Create([doOwnsValues]); // owns
  fContext := aContext;
  // parsing aResultParameters
  if aResultParameters is TJSONArray then
  begin
    arrayOfResults := aResultParameters as TJSONArray;
    for a := 0 to arrayOfResults.Count-1 do
    begin
      r := arrayOfResults.Items[a];
      if r is TJSONObject then
      begin
        ro := r as TJSONObject;
        if ro.TryGetValue<string>('name', n) and
           ro.TryGetValue<string>('value', v) and
           ro.TryGetValue<string>('type', t) then
        begin
          fProperties.Add(n, TFormDialogResultProperty.Create(n, v, t));
        end
        else Log.WriteLn('Could not read form dialog result: '+r.ToJSON, llWarning);
      end;
    end;
  end;
end;

destructor TFormDialogResults.Destroy;
begin
  FreeAndNil(fProperties);
  inherited;
end;

{ TFormDialogProperty }

constructor TFormDialogProperty.Create(const aName, aLabel, aType, aFormElement: string; aRequired, aHidden: boolean);
begin
  inherited Create;
  fName := aName;
  fLabel := aLabel;
  fType := aType;
  fFormElement := aFormElement;
  fRequired := aRequired;
  fHidden := aHidden;
  fOptionsArray := TStringList.Create;
  fOptionsArray.StrictDelimiter := True;
  fOptionsArray.Delimiter := ',';
  fExtraOptionsJSON := 'false';
end;

destructor TFormDialogProperty.Destroy;
begin
  FreeAndNil(fOptionsArray);
  inherited;
end;

function TFormDialogProperty.JSON: string;
var
  _required: string;
  _optionsArray: string;
  o: string;
begin
  if fRequired
  then _required := 'y'
  else _required := 'n';
  (*
  if fFormElement='slider' then
  begin
    _optionsExtra := '{ "defaultValue" : "'+fDefaultValue+'" }';
  end
  else
  begin
    if fDefaultValue.IsEmpty
    then _optionsExtra := 'false'
    else _optionsExtra := '{ "defaultValue" : "'+fDefaultValue+'" }';
  end;
  *)
  if fOptionsArray.Count=0
  then _optionsArray := 'false'
  else
  begin
    _optionsArray := '';
    for o in fOptionsArray
    do jsonAdd(_optionsArray, '"'+o+'"');
    _optionsArray := '['+_optionsArray+']';
  end;
  Result :=
    '{'+
      '"formElement" : "'+fFormElement+'",'+
      '"type" : "'+fType+'",'+
      '"required" : "'+_required+'",'+
      '"optionsArray" : '+_optionsArray+','+
      '"labelText" : "'+fLabel+'",'+
      '"idName" : "'+fName+'",'+
      '"hidden" : '+fHidden.ToString(TUseBoolStrs.True).ToLower+','+
      '"extraOptions" : '+fExtraOptionsJSON+
    '}';
end;

{ TFormDialog }

function TFormDialog.AddPropertyCheck(const aName, aLabel: string; const aOptions: TArray<string>; const aType: string; aRequired, aHidden: boolean): TFormDialogProperty;
var
  o: string;
begin
  Result := TFormDialogProperty.Create(aName, aLabel, aType, 'checkbox', aRequired, aHidden); // todo: defaults
  // todo: checked state per element
  for o in aOptions
  do Result.OptionsArray.Add(o);
  Properties.Add(Result);
end;

function TFormDialog.AddPropertyInput(const aName, aLabel, aDefaultValue, aType: string; aRequired, aHidden: boolean): TFormDialogProperty;
begin
  Result := TFormDialogProperty.Create(aName, aLabel, aType, 'input', aRequired, aHidden);
  Result.ExtraOptionsJSON := '{ "defaultValue" : "'+aDefaultValue+'" }';
  Properties.Add(Result);
end;

function TFormDialog.AddPropertyRadio(const aName, aLabel, aDefaultValue: string; const aOptions: TArray<string>; const aType: string; aRequired, aHidden: boolean): TFormDialogProperty;
var
  o: string;
begin
  Result := TFormDialogProperty.Create(aName, aLabel, aType, 'radio', aRequired, aHidden);
  Result.ExtraOptionsJSON := '{ "defaultValue" : "'+aDefaultValue+'" }';
  for o in aOptions
  do Result.OptionsArray.Add(o);
  Properties.Add(Result);
end;

function TFormDialog.AddPropertySelect(const aName, aLabel, aDefaultValue: string; const aOptions: TArray<string>; aRequired, aHidden: boolean): TFormDialogProperty;
var
  o: string;
begin
  Result := TFormDialogProperty.Create(aName, aLabel, 'string', 'select', aRequired, aHidden);
  Result.ExtraOptionsJSON := '{ "defaultValue" : "'+aDefaultValue+'" }';
  for o in aOptions
  do Result.OptionsArray.Add(o);
  Properties.Add(Result);
end;

function TFormDialog.AddPropertySlider(const aName, aLabel: string; aMin, aMax, aStep: Integer;
  const aUnit, aType: string; aRequired, aHidden: boolean): TFormDialogProperty;
begin
  Result := TFormDialogProperty.Create(aName, aLabel, aType, 'slider', aRequired, aHidden);
  Result.ExtraOptionsJSON := '['+aStep.ToString+',"'+aUnit+'"]';
  Result.OptionsArray.Add(aMin.ToString);
  Result.OptionsArray.Add(aMax.ToString);
  Properties.Add(Result);
end;

function TFormDialog.AddPropertyText(const aName, aLabel, aType: string; aRequired, aHidden: boolean): TFormDialogProperty;
begin
  Result := TFormDialogProperty.Create(aName, aLabel, aType, 'textarea', aRequired, aHidden);
  Properties.Add(Result);
end;

constructor TFormDialog.Create(const aFormID, aTitle: string);
begin
  inherited Create;
  fFormID := aFormID;
  fTitle := aTitle;
  fProperties := TObjectList<TFormDialogProperty>.Create(True); // owns, order
  fPropertiesLookup := TObjectDictionary<string, TFormDialogProperty>.Create([]); // refs, auto filled
  fProperties.OnNotify := HandlePropertiesNotify;
end;

destructor TFormDialog.Destroy;
begin
  FreeAndNil(fProperties);
  FreeAndNil(fPropertiesLookup);
  inherited;
end;

procedure TFormDialog.HandlePropertiesNotify(aSender: TObject; const aItem: TFormDialogProperty;
  aAction: System.Generics.Collections.TCollectionNotification);
begin
  // update dictionary
  if aAction=System.Generics.Collections.TCollectionNotification.cnAdded then
  begin
    // cnAdded
    fPropertiesLookup.Add(aItem.Name, aItem);
  end
  else
  begin
    // cnRemoved, cnExtracted
    fPropertiesLookup.Remove(aItem.Name);
  end;
end;

function TFormDialog.JSON(const aContext: string): string;
var
  formElements: string;
  p: TFormDialogProperty;
begin
  formElements := '';
  for p in Properties
  do jsonAdd(formElements, p.JSON);
  Result :=
    '{'+
      '"type": "openformdialog",'+
      '"payload": {'+
        '"id": "'+FormID+'",'+
        '"title": "'+Title+'",'+
        '"data": ['+formElements+'],'+
        '"context": '+aContext+
      '}'+
    '}';
end;

procedure TFormDialog.Open(aClient: TClient; aOnResult: TFormDialogResultHandling; const aContext: string);
begin
  Prepare(aClient, aOnResult);
  // send form info to client
  aClient.signalString(JSON(aContext));
end;

procedure TFormDialog.Prepare(aClient: TClient; aOnResult: TFormDialogResultHandling);
begin
  // register handler for results on client
  TMonitor.Enter(aClient.formResultHandlers);
  try
    aClient.formResultHandlers.AddOrSetValue(FormID, aOnResult);
  finally
    TMonitor.Exit(aClient.formResultHandlers);
  end;
end;

{ TClient }

procedure TClient.SendDomains(const aPrefix: string);
var
  d: TClientDomain;
  domains: TDictionary<string, TClientDomain>;
  JSON: string;
  layer: TLayerBase;
  ndp: TPair<string, TClientDomain>;
  domainsJSON: string;
  nkp: TPair<string, TKPI>;
  ngp: TPair<string, TChart>;
  locLayers: TList<TLayerBase>;
  refLayer: TLayerBase;
  diffLayer: TDiffLayer;
  _diffElementID: string;
  refChart: TChart;
begin
  if Assigned(refScenario)
  then Log.WriteLn('TClient.SendDomains ('+aPrefix+'), ref scenario '+refScenario.ID)
  else Log.WriteLn('TClient.SendDomains ('+aPrefix+'): no ref scenario');
  // todo: add reference and diff layers/charts if fRefScenario<>nil
  domains := TDictionary<string, TClientDomain>.Create;
  try
    if Assigned(currentScenario) then
    begin
      // layers
      locLayers := TList<TLayerBase>.Create(TComparer<TLayerBase>.Construct(compareLayerNames));
      try
        locLayers.AddRange(currentScenario.Layers.Values);
        locLayers.Sort;
        for layer in locLayers do
        begin
          if layer.showInDomains then
          begin
            JSON := layer.JSON;
            if Assigned(refScenario) and not layer.basicLayer then
            begin
              if layer is TLayer then
              begin
                if refScenario.Layers.TryGetValue(layer.ID, refLayer) and (refLayer is TLayer) then
                begin
                  // todo: full JSON for ref and diff, to include legend?
                  JSON := JSON+',"ref":{'+(refLayer as TLayer).refJSON+'}';
                  _diffElementID :=  currentScenario.project.diffElementID(layer, refLayer);
                  TMonitor.Enter(currentScenario.project.diffLayers);
                  try
                    if not currentScenario.project.diffLayers.TryGetValue(_diffElementID, diffLayer) then
                    begin
                      diffLayer := TDiffLayer.Create(_diffElementID, layer as TLayer, refLayer as TLayer);
                      currentScenario.project.diffLayers.Add(_diffElementID, diffLayer);
                      // todo:

                    end;
                  finally
                    TMonitor.Exit(currentScenario.project.diffLayers);
                  end;
                  // todo: temp removed for testing
                  JSON := JSON+',"diff":{'+diffLayer.refJSON+'}';
                end
                else Log.WriteLn('TClient.SendDomains ('+aPrefix+'): no ref layer for '+layer.ID);
              end;
            end;
            JSON  := '{'+JSON+'}';
            if domains.TryGetValue(layer.domain, d) then
            begin
              jsonAdd(d.layers, JSON);
              domains[layer.domain] := d;
            end
            else
            begin
              d := TClientDomain.Create(layer.domain);
              d.layers := JSON;
              domains.Add(d.name, d);
            end;
          end;
        end;
      finally
        locLayers.Free;
      end;
      // kpis
      for nkp in currentScenario.KPIs do
      begin
        JSON := '{'+nkp.Value.JSON+'}';
        if domains.TryGetValue(nkp.Value.domain, d) then
        begin
          jsonAdd(d.kpis, JSON);
          domains[nkp.Value.domain] := d;
        end
        else
        begin
          d := TClientDomain.Create(nkp.Value.domain);
          d.kpis := JSON;
          domains.Add(d.name, d);
        end;
      end;
      // charts
      for ngp in currentScenario.Charts do
      begin
        JSON := ngp.Value.JSON;
        // add ref
        if Assigned(refScenario) then
        begin
          if refScenario.Charts.TryGetValue(ngp.Key, refChart)
          then jsonAdd(JSON, '"ref":{'+refChart.JSON+'}');
        end;
        JSON := '{'+JSON+'}';
        if domains.TryGetValue(ngp.Value.domain, d) then
        begin
          jsonAdd(d.charts, JSON);
          domains[ngp.Value.domain] := d;
        end
        else
        begin
          d := TClientDomain.Create(ngp.Value.domain);
          d.charts := JSON;
          domains.Add(d.name, d);
        end;
      end;
    end;
    domainsJSON := '';
    for ndp in domains do
    begin
      d := ndp.Value;
      // extra check to make domains enabled by default through entry in exe ini
      // todo: use const
      d.enabled := standardIni.ReadInteger('DefaultEnabledDomains', ndp.Key, GetSetting('DomainEnabledDefault', 0));
      jsonAdd(domainsJSON, d.JSON);
    end;
    signalString('{"type":"'+aPrefix+'","payload":{'+domainsJSON+'}}'); // default prefix is "domains":..
  finally
    domains.Free;
  end;
end;

function TClient.activeScenarioJSON: string;
begin
  if Assigned(fCurrentScenario)
  then Result := '"activeScenario":"'+fCurrentScenario.id+'"'
  else Result := '';
end;

procedure TClient.addClient(aElement: TClientSubscribable);
begin
  if Assigned(aElement) then
  begin
    SubscribeTo(aElement,
      procedure (aSender: TSubscribeObject; const aAction, aObjectID: Integer)
      begin
        exit; //empty function, client subscribes are only interested in registering for forEachClient calls
      end);
    aElement.HandleClientSubscribe(Self);
  end;
end;

function TClient.controlsJSON: string;
var
  controlStates: TDictionary<string, string>;
  csp: TPair<string, string>;
begin
  controlStates := TDictionary<string, string>.Create;
  try
    // gather all control states and merge levels client, scenario, project
    TMonitor.Enter(fControls);
    try
      for csp in fControls
      do controlStates.AddOrSetValue(csp.Key, csp.Value);
    finally
      TMonitor.Exit(fControls);
    end;
    if Assigned(fCurrentScenario) then
    begin
      TMonitor.Enter(fCurrentScenario.fControls);
      try
        for csp in fCurrentScenario.fControls do
        begin
          if not controlStates.ContainsKey(csp.Key)
          then controlStates.AddOrSetValue(csp.Key, csp.Value);
        end;
      finally
        TMonitor.Exit(fCurrentScenario.fControls);
      end;
      if Assigned(fCurrentScenario.project) then
      begin
        TMonitor.Enter(fCurrentScenario.project.fControls);
        try
          for csp in fCurrentScenario.project.fControls do
          begin
            if not controlStates.ContainsKey(csp.Key)
            then controlStates.AddOrSetValue(csp.Key, csp.Value);
          end;
        finally
          TMonitor.Exit(fCurrentScenario.project.fControls);
        end;
      end;
    end;
    // build control states json
    Result := '';
    if controlStates.Count>0 then
    begin
      for csp in controlStates
      do jsonAdd(Result, '"'+csp.Key+'":'+jsonOrNull(csp.value));
    end;
  finally
    controlStates.Free;
  end;
end;

procedure TClient.SendAddDownloadableFile(const aFileName: string; const aFileTypes: TArray<string>);
var
  types: string;
  t: string;
begin
  for t in aFileTypes
  do jsonAdd(types, '"'+t+'"');
  signalString('{"type":"fileDownload","payload":{"add":[{"fileName":"'+aFileName+'","fileTypes":['+types+']}]}}');
end;

procedure TClient.SendAddDownloadableFiles(aDownloadableFiles: TDictionary<string, TDownloadableFileInfo>);
var
  json: string;
  fip: TPair<string, TDownloadableFileInfo>;
  types: string;
  t: string;
begin
  json := '';
  for fip in aDownloadableFiles do
  begin
    types := '';
    for t in fip.value.fileTypes
    do jsonAdd(types, '"'+t+'"');
    jsonAdd(json, '{"fileName":"'+fip.key+'","fileTypes":['+types+']}');
  end;
  signalString('{"type":"fileDownload","payload":{"add":['+json+']}}');
end;

procedure TClient.SendClearDownloadableFile();
begin
  signalString('{"type":"fileDownload","payload":{"clear":1}}');
end;

//procedure TClient.SendClearDownloadableFiles;
//begin
//  signalString('{"type":"fileDownload","payload":{"clear":""}}');
//end;

procedure TClient.SendDownloadableFileStream(const aName: string; aStream: TStream);
const
  bufferSize: Integer = 20*1024;
var
  encodedData: string;
  position: Int64;
begin
  Log.WriteLn('Start sending downloadable file '+aName);
  while aStream.Position<aStream.Size do
  begin
    position := aStream.Position;
    Log.Progress('download '+aName+': '+position.ToString+'/'+aStream.Size.ToString+
              ' ('+Round(100.0*position/aStream.Size).tostring+'%)');
    encodedData := TIdEncoderMIME.EncodeStream(aStream, bufferSize);
    signalString('{"type":"fileDownload","payload":{'+
      '"fileName":"'+aName+'",'+
      '"fileSize":'+aStream.Size.ToString+','+
      '"fileBlockOffset":'+position.ToString+','+
      '"fileContents":"'+encodedData+'"'+
      '}}');
  end;
  Log.WriteLn('Finished sending downloadable file '+aName);
end;

constructor TClient.Create(aOwnerProject: TProject; aCurrentScenario, aRefScenario: TScenario; const aClientID: string);
begin
  inherited Create;
  fCanCopyScenario := False;
  fLastValidToken := '';
  fControls := TDictionary<string, string>.Create;
  fSubscribedElements := TObjectList<TClientSubscribable>.Create(False);
  fFileUploads := TDictionary<string, TClientFileUploadInfo>.Create;
  fFormResultHandlers := TDictionary<string, TFormDialogResultHandling>.Create;
  fOwnerProject := aOwnerProject;
  fClientID := aClientID;
  fClientEvent := aOwnerProject.fConnection.eventEntry(aClientID, False).publish;
  fClientEvent.subscribe;
  // add handler for disconnecting clients
  fClientEvent.OnIntString.Add(
    procedure(event: TEventEntry; aInt: Integer; const aString: string)
    begin
      try
        if aInt=actionDelete then
        begin
          Log.WriteLn('unlink from '+event.eventName);
          TMonitor.Enter(aOwnerProject.clients);
          try
            aOwnerProject.clients.Remove(Self);
          finally
            TMonitor.Exit(aOwnerProject.clients);
          end;
        end;
      except
        on E: Exception
        do Log.WriteLn('Exception in TClient.Create fClientEvent.OnIntString: '+E.Message, llError);
      end;
    end);
  // add handler for client commands
  fClientEvent.OnString.Add(
    procedure(event: TEventEntry; const aString: string)
    begin
      HandleClientCommand(aString);
    end);
  fClientEvent.OnStreamCreate :=
    function(aEventEntry: TEventEntry; const aName: string): TStream
    begin
      Result := TStringStream.Create('', TEncoding.UTF8, False);
    end;
  fClientEvent.OnStreamEnd :=
    procedure(aEventEntry: TEventEntry; const aName: string; var aStream: TStream; aCancel: Boolean)
    begin
      handleClientCommand((aStream as TStringStream).DataString);
    end;
  // trigger login
  signalString('{"type":"login","payload": {}}');
  // set scenarios
  fCurrentScenario := aCurrentScenario;
  fRefScenario := aRefScenario;
  addClient(fCurrentScenario);
  addClient(fRefScenario);
  if Assigned(fCurrentScenario) and Assigned(fRefScenario) then
  begin
    // todo: create list diff layers, charts and kpis
  end;
  // do default registration
  SendSession(aOwnerProject);
  if Assigned(currentScenario) then
  begin
    if Control[measuresControl]<>controlDisabled
    then SendMeasures(currentScenario.project);
    if Control[measuresHistoryControl]<>controlDisabled
    then SendMeasuresHistory(currentScenario.project);
    if Control[selectControl]<>controlDisabled
    then SendQueryDialogData(currentScenario.project);
  end;
  SendDomains('domains');
  // uploadable files
  if Assigned(currentScenario)
  then SendAddDownloadableFiles(currentScenario.project.fDownloadableFiles);

  // else send on login on scenario: avoid twice sending domains
  aOwnerProject.handleNewClient(Self);
end;

destructor TClient.Destroy;
var
  se: TClientSubscribable;
begin
  fOwnerProject.removeMemberFromAllGroups(Self);
  if Assigned(fClientEvent) then
  begin
    fClientEvent.onIntString.Clear;
    fClientEvent.unSubscribe;
    fClientEvent.signalIntString(actionDelete, '');
    fClientEvent.unPublish;
  end;
  TMonitor.Enter(fSubscribedElements);
  try
    for se in fSubscribedElements
    do se.HandleClientUnsubscribe(Self);
  finally
    TMonitor.Exit(fSubscribedElements);
  end;
  FreeAndNil(fSubscribedElements);
  if Assigned(fClientEvent) then
  begin
    fClientEvent := nil;
  end;
  fOwnerProject.handleRemoveClient(Self);
  FreeAndNil(fFileUploads);
  FreeAndNil(fFormResultHandlers);
  fCurrentScenario := nil;
  fRefScenario := nil;
  FreeAndNil(fControls);
  inherited;
end;

function TClient.getControl(const aControlName: string): string;
var
  found: Boolean;
begin
  TMonitor.Enter(controls);
  try
    found := controls.TryGetValue(aControlName, Result);
  finally
    TMonitor.Exit(controls);
  end;
  if not found then
  begin
    if Assigned(currentScenario)
    then Result := currentScenario.Control[aControlName] // does also check owning project
    else Result := '';
  end;
end;

function TClient.getSessionModel: TSessionModel;
begin
  Result := fOwnerProject.fSessionModel;
end;

function TClient.mapViewJSON: string;
begin
  if Assigned(fCurrentScenario)
  then Result :=
    '"view":{'+
      '"lat":'+Double.ToString(fCurrentScenario.fMapView.lat, dotFormat)+','+
      '"lon":'+Double.ToString(fCurrentScenario.fMapView.lon, dotFormat)+','+
      '"zoom":'+Double.ToString(fCurrentScenario.fMapView.zoom, dotFormat)+
    '}'
  else Result :=
    '"view":{'+
      '"lat":'+Double.ToString(fOwnerProject.fMapView.lat, dotFormat)+','+
      '"lon":'+Double.ToString(fOwnerProject.fMapView.lon, dotFormat)+','+
      '"zoom":'+Double.ToString(fOwnerProject.fMapView.zoom, dotFormat)+
    '}';
end;

procedure TClient.HandleClientCommand(const aJSONString: string);

  procedure selectScenario(aProject: TProject; aJSONObject: TJSONObject);
  var
    scenarioID: string;
    scenario: TScenario;
    jsonValue: TJSONValue;
    //  diffLayerID: string;
    //  diffLayer: TDiffLayer;
    //  ilp: TPair<string, TLayer>;
    //  refLayer: TLayer;
    //  diffPalette: TWDPalette;
  begin
    if isObjectValue(aJSONObject, 'currentScenario', jsonValue) then
    begin
      removeClient(fCurrentScenario);
      scenarioID := jsonValue.Value;
      TMonitor.Enter(aProject.scenarios);
      try
        if not aProject.scenarios.TryGetValue(scenarioID, scenario)
        then scenario := aProject.ReadScenario(scenarioID);
      finally
        TMonitor.Exit(aProject.scenarios);
      end;
      fCurrentScenario := scenario;
      addClient(fCurrentScenario);
      if Assigned(fCurrentScenario)
      then Log.WriteLn('client switched scenario to '+fCurrentScenario.elementID)
      else Log.WriteLn('client switched scenario to NONE');
    end;
    if isObjectValue(aJSONObject, 'referenceScenario', jsonValue) then
    begin
      removeClient(fRefScenario);
      scenarioID := jsonValue.Value;
      TMonitor.Enter(aProject.scenarios);
      try
        if not aProject.scenarios.TryGetValue(scenarioID, scenario)
        then scenario := aProject.ReadScenario(scenarioID{, fCurrentScenario});
      finally
        TMonitor.Exit(aProject.scenarios);
      end;
      fRefScenario := scenario;
      addClient(fRefScenario);
      if Assigned(fRefScenario)
      then Log.WriteLn('client switched ref scenario to '+fRefScenario.elementID)
      else Log.WriteLn('client switched ref scenario to NONE');
      {
        // todo: create list diff layers, charts and kpis
        if Assigned(fCurrentScenario) then
        begin
          for ilp in fCurrentScenario.Layers do
          begin
            if fRefScenario.Layers.TryGetValue(ilp.Key, refLayer) then
            begin
              diffLayerID := fProject.diffElementID(ilp.Value, refLayer);
              TMonitor.Enter(fProject.diffLayers);
              try
                if not fProject.diffLayers.TryGetValue(diffLayerID, diffLayer) then
                begin
                  diffLayer := TDiffLayer.Create(diffLayerID, ilp.Value, refLayer);
                  fProject.diffLayers.Add(diffLayerID, diffLayer);
                end;
                addClient(diffLayer);
              finally
                TMonitor.Exit(fProject.diffLayers);
              end;
            end;
          end;
        end;
      end
      else
      }
    end;
    SendDomains('updatedomains');
    UpdateSession(aProject);
  end;

  procedure selectObjects(aSelectObjects: TJSONValue);
  var
    t: string;
    m: string;
    g: TJSONObject;
    measure: TJSONObject;
    rStr: string;
    r: Double;
    x, y: Double;
    geometry: TWDGeometry;
    part: TWDGeometryPart;
    c, c2: TJSONArray;
    pointI: Integer;
    sca: TJSONArray;
    sc: TArray<string>;
    abla: TJSONArray;
    abl: TArray<string>;
    jsonZoom: TJSONValue;
    zoom: Double;
    oids: TArray<string>;
    i: Integer;
    resp: string;
    jsonSelectedObjects: TJSONArray;
    jsonQuery: TJSONArray;
  begin
    // decode selection and send back objects
    resp := '';
    // type
    t := aSelectObjects.getValue<string>('type', '');
    // mode
    m := aSelectObjects.getValue<string>('mode', '');
    // selectCategories
    sca := aSelectObjects.getValue<TJSONArray>('selectCategories');
    setLength(sc, sca.count);
    for i := 0 to sca.count-1
    do sc[i] := sca.Items[i].Value;
    // radius
    rStr := aSelectObjects.getValue<string>('radius', '');
    // geometry
    if aSelectObjects.TryGetValue<TJSONObject>('geometry', g) then
    begin
      if aSelectObjects.TryGetValue<TJSONArray>('activeBasicLayers', abla) then
      begin
        setLength(abl, abla.Count);
        for i := 0 to abla.Count - 1 do
          abl[i] := abla.Items[i].Value;
      end
      else
        setLength(abl, 0);
      c := g.Values['geometry'].getValue<TJSONArray>('coordinates');
      // select objects based on geometry
      // always polygon or point (radius)? -> simplyfy
      if c.Count=1 then
      begin
        // 1 part with coordinates
        c2 := c.Items[0] as TJSONArray;
        geometry := TWDGeometry.Create;
        try
          part := geometry.AddPart;
          for pointI := 0 to c2.Count-1 do
          begin
            x := Double.Parse(TJSONArray(c2.Items[pointI]).Items[0].tostring, dotFormat);
            y := Double.Parse(TJSONArray(c2.Items[pointI]).Items[1].tostring, dotFormat);
            part.AddPoint(x, y, NaN);
          end;
          // todo:
          if Assigned(fCurrentScenario) then
          begin
            resp := fCurrentScenario.SelectObjects(Self, t, m, sc, abl, geometry);
          end;
        finally
          geometry.Free;
        end;
      end
      else
      begin
        // must be point
        x := Double.Parse(TJSONArray(c).Items[0].tostring, dotFormat);
        y := Double.Parse(TJSONArray(c).Items[1].tostring, dotFormat);
        if aSelectObjects.TryGetValue<TJSONValue>('zoom', jsonZoom) then
        begin
          zoom := Double.Parse(jsonZoom.Value, dotFormat);
        end
        else
          zoom := 0;

        if rStr<>''
        then r := Double.Parse(rStr, dotFormat)
        else r := 0;
        // todo:
        if Assigned(fCurrentScenario) then
        begin
          resp := fCurrentScenario.SelectObjects(Self, t, m, sc, abl, x, y, r, zoom);
        end;
      end;
    end
    else if aSelectObjects.TryGetValue<TJSONObject>('measure', measure) then
    begin
      if Assigned(fCurrentScenario) then
      begin
        // decode objects from measure                         \
        if aSelectObjects.TryGetValue<TJSONArray>('selectedObjects', jsonSelectedObjects) then
        begin
          setLength(oids, jsonSelectedObjects.Count);
          for i := 0 to jsonSelectedObjects.Count-1
          do oids[i] := jsonSelectedObjects.Items[i].value;
        end
        else setLength(oids, 0);
        resp := fCurrentScenario.SelectObjects(Self, t, m, sc, oids);
      end;
    end
    else if aSelectObjects.TryGetValue<TJSONArray>('query', jsonQuery) then
    begin
      // select objects based on query
      if Assigned(fCurrentScenario)
      then resp := fCurrentScenario.SelectObjects(Self, t, m, sc, jsonQuery);
    end;
    if resp<>'' then
    begin
      signalString(resp);
    end;
  end;

  procedure selectObjectsProperties(aSelectObjectsProperties: TJSONValue; aRequestID: string);
  var
    sca: TJSONArray;
    sc: TArray<string>;
    i: Integer;
    so: TArray<string>;
    resp: string;
  begin
    // selectCategories
    sca := aSelectObjectsProperties.getValue<TJSONArray>('selectCategories');
    setLength(sc, sca.count);
    for i := 0 to sca.count-1
    do sc[i] := sca.Items[i].Value;
    // selectedObjects
    sca := aSelectObjectsProperties.getValue<TJSONArray>('selectedObjects');
    setLength(so, sca.count);
    for i := 0 to sca.count-1
    do so[i] := sca.Items[i].Value;
    if Assigned(fCurrentScenario) then
    begin
      resp := fCurrentScenario.selectObjectsProperties(Self, sc, so);
      if resp<>''
      then signalString('{"type": "dialogDataResponse", "payload":{"id":"' + aRequestID + '", "data": ' + resp + '}}')
      else
      begin
        signalString('{"type": "dialogDataResponse", "payload":{"id":"' + aRequestID + '", "data": ' + '{}' + '}}')
        // todo: signal message..
      end;
    end;
  end;

  procedure handleTypedMessage(aProject: TProject; const aMessageType: string; var aJSONObject: TJSONObject);
  var
    group: string;
    members: TGroup;
    member: TClient;
    command: string;
    payload: TJSONObject;
    uploadFolder: string;
    fuFileName: string;
    fuFileType: string;
    //fuFileBlockSize: Integer;
    fuFileBlockOffset: Integer;
    fuFileSize: Integer;
    cfui: TClientFileUploadInfo;
    F: File;
    fuFileContents: string;
    buffer: TIdBytes;
    //fileGenerator: TFileGenerator;
    fi: TDownloadableFileInfo;
    fuLayerID: string;
    element: TClientSubscribable;
    dataRequestType: string;
    requestData: TJSONObject;
    id: string;
    formParameters: TJSONValue;
    formContext: TJSONValue;
    ofdr: TFormDialogResultHandling;
    fr: TFormDialogResults;
    mvFollow: Boolean;
    mvLead: Boolean;
    mvViewJSON: TJSONObject;
    mvView: TMapView;
  begin
    if aMessageType='ccv' then // send to viewers
    begin
      if aJSONObject.TryGetValue<string>('payload.group', group) then
      begin
        members := TGroup.Create(false); // refs
        try
          TMonitor.Enter(aProject.groups);
          try
            if aProject.getGroupMembersNoLocking(group, members) then
            begin
              for member in members do
              begin
                if (member<>Self) and (member<>members[0])
                then member.signalString(aJSONObject.ToJSON);
              end;
            end;
          finally
            TMonitor.Exit(aProject.groups);
          end;
        finally
          members.Free;
        end;
      end;
    end
    else if aMessageType='ccp' then // send to presenter
    begin
      if aJSONObject.TryGetValue<string>('payload.group', group) then
      begin
        members := TGroup.Create(false); // refs
        try
          TMonitor.Enter(aProject.groups);
          try
            aProject.getGroupMembersNoLocking(group, members);
            if (members.Count>0) and (members[0]<>Self)
            then members[0].signalString(aJSONObject.ToJSON);
          finally
            TMonitor.Exit(aProject.groups);
          end;
        finally
          members.Free;
        end;
      end;
    end
    else if aMessageType='ccb' then // broadcast
    begin
      if aJSONObject.TryGetValue<string>('payload.group', group) then
      begin
        members := TGroup.Create(false); // refs
        try
          TMonitor.Enter(aProject.groups);
          try
            if aProject.getGroupMembersNoLocking(group, members) then
            begin
              for member in members do
              begin
                if member<>Self
                then member.signalString(aJSONObject.ToJSON);
              end;
            end;
          finally
            TMonitor.Exit(aProject.groups);
          end;
        finally
          members.Free;
        end;
      end;
    end
    else if aMessagetype='groupcontrol' then
    begin
      (*
      {
        "type":"groupcontrol"
        "payload":
        {
          "command":"presenteron"|"presenteroff"|"vieweron"|"vieweroff"|"leaveallgroups"
          ["group":""] (not on "leaveallgroups")
        }
      }
      answer:
      {
        "type":"groupcontrol"
        "payload":
        {
          "command": "..."
          ["group": "..."]
          "result":"success"|"groupalreadyexists"|"groupdoesnotexist"
        }
      }
      *)
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) and
         payload.TryGetValue<string>('command', command) and
         payload.TryGetValue<string>('group', group) then
      begin
        if command='presenteron' then
        begin
          if aProject.addGroup(group, Self)
          then payload.AddPair('result', 'success')
          else payload.AddPair('result', 'groupalreadyexists');
          signalString(aJSONObject.ToJSON);
          //Log.WriteLn(aJSONObject.ToJSON, llRemark);
        end
        else if command='presenteroff' then
        begin
          TMonitor.Enter(aProject.groups);
          try
            if aProject.isPresenterNoLocking(group, Self) and aProject.removeGroupNoLocking(group, nil)
            then payload.AddPair('result', 'success')
            else payload.AddPair('result', 'groupdoesnotexist');
            signalString(aJSONObject.ToJSON);
            //Log.WriteLn(aJSONObject.ToJSON, llRemark);
          finally
            TMonitor.Exit(aProject.groups);
          end;
        end
        else if command='vieweron' then
        begin
          if aProject.addGroupMember(group, Self)
          then payload.AddPair('result', 'success')
          else payload.AddPair('result', 'groupdoesnotexist');
          signalString(aJSONObject.ToJSON);
          //Log.WriteLn(aJSONObject.ToJSON, llRemark);
        end
        else if command='vieweroff' then
        begin
          if aProject.removeGroupMember(group, Self)
          then payload.AddPair('result', 'success')
          else payload.AddPair('result', 'groupdoesnotexist');
          signalString(aJSONObject.ToJSON);
          //Log.WriteLn(aJSONObject.ToJSON, llRemark);
        end
        else if command='leaveallgroups' then
        begin
          aProject.removeMemberFromAllGroups(Self);
          //Log.WriteLn(aJSONObject.ToJSON, llRemark);
        end
        else Log.WriteLn('Unknown on command "'+command+'" in "groupcontrol" message', llWarning);
      end;
    end
    else if aMessagetype='uploadFile' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) and
           payload.TryGetValue<string>('fileName', fuFileName) and
           payload.TryGetValue<Integer>('fileSize', fuFileSize) and
           payload.TryGetValue<Integer>('fileBlockOffset', fuFileBlockOffset) and
           payload.TryGetValue<string>('fileContents', fuFileContents) then
      begin
        // handle file upload
        uploadFolder := ExtractFilePath(ParamStr(0))+'uploads';
        ForceDirectories(uploadFolder);
        if fuFileSize<=MaxFileUploadSize then
        begin
          if not fFileUploads.TryGetValue(fuFileName, cfui) then
          begin
            cfui.fileName := fuFileName;
            cfui.fileSize := fuFileSize;
            cfui.filledSize := 0;
            cfui.path := uploadFolder+'\'+TGUID.NewGuid.ToString;
            // create file
            AssignFile(F, cfui.path);
            Rewrite(F, 1);
            CloseFile(F);
            Log.WriteLn('file upload: created new file: '+fuFileName+' ('+cfui.path+')', llNormal, 2);
          end;
          // check if we've got the next block
          if fufileBlockOffset=cfui.filledSize then
          begin
            Log.Progress('upload '+fuFileName+': '+fufileBlockOffset.ToString+'/'+fuFileSize.ToString+
              ' ('+Round(100.0*fufileBlockOffset/fuFileSize).tostring+'%)');
            // decode fuFileContents to binary
            buffer := TIdDecoderMIME.DecodeBytes(fuFileContents);
            // write block to file
            AssignFile(F, cfui.path);
            Reset(F, 1);
            try
              Seek(F, FileSize(F));
              BlockWrite(F, buffer[0], length(buffer));
            finally
              CloseFile(F);
            end;
            cfui.filledSize := cfui.filledSize+ length(buffer);
            if cfui.filledSize=cfui.fileSize then
            begin
              // remove file info
              fFileUploads.Remove(fuFileName);
              Log.WriteLn('file '+fuFileName+' uploaded to '+cfui.path);
              // todo: call handler that file is uploaded
              aProject.handleFileUpload(Self, cfui);
              // todo: remove for now: DeleteFile(cfui.path);
            end
            else fFileUploads.AddOrSetValue(fuFileName, cfui);
          end
          else
          begin
            DeleteFile(cfui.path);
            Log.WriteLn('File offset is not filled size: '+fufileBlockOffset.toString+' <> '+fuFileSize.ToString, llError);
            // todo: log message to client

            fFileUploads.Remove(fuFileName);
          end;
        end
        else
        begin
          Log.WriteLn('File upload exceeds max size: '+fuFileSize.ToString, llError);
          // todo: log message to client

          fFileUploads.Remove(fuFileName);
        end;
      end
      else Log.WriteLn('Invalid upload file command', llError);
    end
    else if aMessageType='downloadFile' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) and
         payload.TryGetValue<string>('fileName', fuFileName) then
      begin
        if not payload.TryGetValue<string>('fileType', fuFileType)
        then fuFileType := '';
        if aProject.fDownloadableFiles.TryGetValue(fuFileName, fi) then
        begin
          if (fuFileType='') and (length(fi.fileTypes)>0)
          then fuFileType := fi.fileTypes[0];
          if Assigned(fi.fileGenerator)
          then fi.fileGenerator(aProject, Self, fuFileName, fuFileType)
          else Log.WriteLn('No valid generator defined for downloadable file '+fuFileName+' ('+fuFileType+')', llError);
          // todo: log message to client
        end
        else
        begin
          Log.WriteLn('File generator not found for download of: '+fuFileName, llWarning);
          // todo: log message to client
        end;
      end
      else Log.WriteLn('Invalid download file command', llError);
    end
    else if aMessageType='updatelayerobject' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) and
           payload.TryGetValue<string>('layerid', fuLayerID) then
      begin
        element := sessionModel.FindElement(fuLayerID);
        if element is TLayerBase
        then (element as TLayerBase).handleUpdateLayerObject(self, payload);
      end
      else Log.WriteLn('Could not find element '+fuLayerID+' on updatelayerobject', llWarning);
    end
    else if aMessageType='dialogDataRequest' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) and
          payload.TryGetValue<string>('type', dataRequestType) and (dataRequestType = 'selectObjectsProperties') and
          payload.TryGetValue<string>('id', id) and
          payload.TryGetValue<TJSONObject>('data', requestData) then
      begin
        selectObjectsProperties(requestData, id);
      end;
    end
    else if aMessageType='formResult' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) and
           payload.TryGetValue<string>('id', id) and
           payload.TryGetValue<TJSONValue>('parameters', formParameters) then
      begin
        if not payload.TryGetValue<TJSONValue>('context', formContext)
        then formContext := nil;
        TMonitor.Enter(formResultHandlers);
        try
          if formResultHandlers.TryGetValue(id, ofdr) then
          begin
            try
              fr := TFormDialogResults.Create(id, formParameters, formContext);
              try
                ofdr.Handler(Self, fr);
              finally
                fr.Free;
              end;
            finally
              if ofdr.RunOnce
              then formResultHandlers.Remove(id);
            end;
          end
          else
          begin
            Log.WriteLn('Form '+id+' not found: cannot handle result', llWarning);
            // switch to old style handler?
          end;
        finally
          TMonitor.Exit(formResultHandlers);
        end;
      end
      else Log.WriteLn('Invalid form result for form '+id, llWarning);
    end
    else if aMessageType='subscribe' then
    begin
      if aJSONObject.TryGetValue<string>('payload', id)
      then addClient(sessionModel.FindElement(id));
    end
    else if aMessageType='unsubscribe' then
    begin
      if aJSONObject.TryGetValue<string>('payload', id)
      then removeClient(sessionModel.FindElement(id));
    end
    else if aMessageType='selectObjects' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload)
      then selectObjects(payload);
    end
    else if aMessageType='login' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload)
      then fOwnerProject.Login(Self, payload);
    end
    else if aMessageType='selectScenario' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload)
      then selectScenario(aProject, payload);
    end
    else if aMessageType='mapView' then
    begin
      if aJSONObject.TryGetValue<TJSONObject>('payload', payload) then
      begin
        if payload.TryGetValue<Boolean>('follow', mvFollow) then
        begin
          fFollow := mvFollow;
        end;
        if payload.TryGetValue<Boolean>('lead', mvLead) then
        begin
          if fLead<>mvLead then
          begin
            fLead := mvLead;
            // reset all other leaders in scenario
            (*
            if Assigned(fCurrentScenario) then
            begin
              fCurrentScenario.forEachSubscriber<TClient>(
                procedure(aScenarioClient: TClient)
                begin
                  if aScenarioClient<>Self then
                  begin
                    aScenarioClient.signalString('{"type":"session","payload":{"mapView":{"lead":false}}}');
                    aScenarioClient.fLead := False;
                  end;
                end);
            end;
            *)
          end;
        end;
        if payload.TryGetValue<TJSONObject>('view', mvViewJSON) then
        begin
          if fLead then
          begin
            mvView := TMapView.Create(
              mvViewJSON.GetValue<Double>('lat'),
              mvViewJSON.GetValue<Double>('lon'),
              mvViewJSON.GetValue<Double>('zoom'));
            if Assigned(fCurrentScenario)
            then fCurrentScenario.HandleNewMapView(Self, mvView);
          end;
        end;
      end;
    end;
    // handle all typed messages also on the connected project (if handler exists)
    aProject.handleTypedClientMessage(self, aMessageType, aJSONObject);
  end;

var
  project: TProject;
  jsonObject: TJSONObject;
  jsonPair: TJSONPair;
  messageType: string;
  token: string;
begin
  if Assigned(currentScenario)
  then project := currentScenario.project
  else project := fOwnerProject;
  try
    if Assigned(project) and Assigned(fClientEvent) then
    begin
      //Log.WriteLn('message from client: '+event.eventName+': '+aString);
      // process message
      jsonObject := TJSONObject.ParseJSONValue(aJSONString) as TJSONObject;
      try
        if not jsonObject.TryGetValue<string>('token', token)
        then token := '';
        if fOwnerProject.isAuthorized(Self, token) then
        begin
          fLastValidToken := token;
          if jsonObject.TryGetValue<string>('type', messageType)
          then handleTypedMessage(project, messageType, jsonObject)
          // todo: remove when transformed to new type of messages
          else if isObject(jsonObject, 'subscribe', jsonPair)
          then addClient(sessionModel.FindElement(jsonPair.JsonValue.Value))
          else if isObject(jsonObject, 'unsubscribe', jsonPair)
          then removeClient(sessionModel.FindElement(jsonPair.JsonValue.Value))
          else if isObject(jsonObject, 'selectObjects', jsonPair)
          then selectObjects(jsonPair.JsonValue)
          else if isObject(jsonObject, 'login', jsonPair)
          then fOwnerProject.Login(Self, jsonPair.JsonValue as TJSONObject)
          else if isObject(jsonObject, 'selectScenario', jsonPair)
          then selectScenario(project, jsonPair.JsonValue as TJSONObject)
          // todo: translate to handleTypedMessage
          else project.handleClientMessage(Self, fCurrentScenario, jsonObject);

          // todo: following have to be transformed to new type message

          // 'applyMeasures'
          //    in PublishServerDME: TUSDesignProject.handleClientMessage

          // 'applyMeasures'
          // 'scenarioRefresh'
          // 'applyObjectsProperties'
          //   in PublishServerEcodistrict: TEcodistrictProject.handleClientMessage

          // 'modelControl'
          //   in PublishServerMCLib: TMCProject.handleClientMessage

          // 'formResult'
          //    in PublishServerSantos: TSantosProject.handleClientMessage

          // 'simulationControl'
          // 'setupSimulation'
          // 'closeSimulation'
          //   in PublishServerSSM: TSSMProject.handleClientMessage

          // 'applyMeasures'
          // 'dialogDataRequest'
          // 'applyObjectsProperties'
          //   in PublishServerUS: TUSProject.handleClientMessage

          // 'formResult'
          // 'scenarioRefresh'
          //   in PublishingServerEffectsLib: TEffectsSessionProject.handleClientMessage
        end
        else
        begin
          fOwnerProject.HandleNotAuthorized(Self, jsonObject, token);
        end;
      finally
        jsonObject.Free;
      end;
    end;
  except
    on e: Exception
    do Log.WriteLn('exception in TClient.Create: fClientEvent.OnString: '+e.Message, llError);
  end;
end;

procedure TClient.HandleElementRemove(aElement: TClientSubscribable);
var
  i: Integer;
begin
  if Assigned(Self) and Assigned(fSubscribedElements) then
  begin
    TMonitor.Enter(fSubscribedElements);
    try
      if Assigned(Self) and Assigned(fSubscribedElements) then
      begin
        i := fSubscribedElements.IndexOf(aElement);
        if i>=0
        then fSubscribedElements.Delete(i);
      end;
    finally
      TMonitor.Exit(fSubscribedElements);
    end;
  end;
end;

procedure TClient.HandleScenarioRemove(aScenario: TScenario);
begin
  // todo: implement
end;

function TClient.referenceScenarioJSON: string;
begin
  if Assigned(fRefScenario)
  then Result := '"referenceScenario":"'+fRefScenario.id+'"'
  else Result := '';
end;

procedure TClient.removeClient(aElement: TClientSubscribable);
begin
  if Assigned(aElement) then
  begin
    UnsubscribeFrom(aElement);
    aElement.HandleClientUnsubscribe(Self);
  end;
end;

procedure TClient.SendRemoveDownloadableFile(const aNames: TArray<string>);
var
  n: string;
  names: string;
begin
  names := '';
  for n in aNames
  do jsonAdd(names, '"'+n+'"'); // todo: usejsonString?
  signalString('{"type":"fileDownload","payload":{"remove":['+names+']}}');
end;

function compareLayerNames(const aLayer1, aLayer2: TLayerBase): Integer;
begin
  Result := AnsiCompareText(aLayer1.name, aLayer2.name);
end;

function ConvertOptions(const aOptionsArray: TArray<TArray<string>>; aOptionsDictionary: TDictionary<string, string>): Boolean;
var
  o: TArray<string>;
  v: string;
  i: Integer;
begin
  Result := True;
  for o in aOptionsArray do
  begin
    if length(o)=2
    then aOptionsDictionary.AddOrSetValue(o[0], o[1])
    else
    begin
      v := '';
      for i := 0 to length(o)-1
      do jsonAdd(v, o[i]);
      Log.WriteLn('Invalid options in ConvertOptions: '+v, llWarning);
      Result := False;
    end;
  end;
end;

{ TClient }

procedure TClient.SendErrorMessage(const aMessage: string);
begin
  signalString('{"type":"connection","payload":{"message":"'+aMessage+'"}}');
end;

procedure TClient.SendLegend(const aElementID, aTimeStamp, aLegendJSON: string);
begin
  signalString('{"type":"refresh","payload":{"id":"'+aElementID+'","timestamp":"'+aTimeStamp+'","legend":{'+aLegendJSON+'}}}');
end;

procedure TClient.SendMeasures(aProject: TProject);
var
  mj: string;
begin
  try
    mj := '{"type":"measures", "payload":'+aProject.measuresJSON+'}';
    signalString(mj);
  except
    Log.WriteLn('Could not read measures from database', llError);
    mj := '{"type":"measures", "payload":{}}';
    signalString(mj);
    SendErrorMessage('Could not read measures');
  end;
end;

procedure TClient.SendMeasuresHistory(aProject: TProject);
begin
  signalString('{"type":"addhistorymeasures","payload":['+aProject.getMeasuresHistoryJSON+']}');
end;

procedure TClient.SendMessage(const aMessage: string; aMessageType: TMessageType; aMessageTimeOutMS: Integer);
var
  mtype: string;
  mTimeOut: string;
begin
  // message type
  case aMessagetype of
    TMessageType.mtError:   mtype := ',"messageType":"error"';
    TMessageType.mtWarning: mtype := ',"messageType":"warning"';
    TMessageType.mtSucces:  mtype := ',"messageType":"succes"';
  else
                            mtype := '';
  end;
  // message time out
  if aMessageTimeOutMS>=0
  then mTimeOut := ',"messageTimeOut":'+aMessageTimeOutMS.toString
  else mTimeOut := '';
  // signal the message command
  signalString('{"type":"connection","payload":{"message":"'+aMessage+'"'+mtype+mTimeOut+'}}');
end;

procedure TClient.SendPreview(const aElementID, aPreviewBase64: string);
begin
  signalString('{"type":"refresh","payload":{"id":"'+aElementID+'","preview":"'+aPreviewBase64+'"}}');
end;

procedure TClient.sendQueryDialogData(aProject: TProject);
begin
  signalString('{"type":"queryDialogData","payload":'+aProject.queryDialogDataJSON+'}');
end;

procedure TClient.SendRefresh(const aElementID, aTimeStamp, aObjectsTilesLink: string);
begin
  signalString('{"type":"refresh","payload":{"id":"'+aElementID+'","timestamp":"'+aTimeStamp+'","tiles":"'+aObjectsTilesLink+'"}}');
end;

procedure TClient.SendRefreshDiff(const aElementID, aTimeStamp, aDiff: string);
begin
  signalString('{"type":"refresh","payload":{"id":"'+aElementID+'","timestamp":"'+aTimeStamp+'","diff":{'+aDiff+'}}}');
end;

procedure TClient.SendRefreshRef(const aElementID, aTimeStamp, aRef: string);
begin
  // todo: how to see that a layer is a ref layer for this client?
  signalString('{"type":"refresh","payload":{"id":"'+aElementID+'","timestamp":"'+aTimeStamp+'","ref":{'+aRef+'}}}');
end;

//procedure TClient.SendSelectionEnabled;
//begin
//  signalString(
//    '{"session":{'+
//      '"selectionEnabled":'+fProject.getControl('selectionEnabled')+
//    '}}');
//end;

procedure TClient.SendSession(aProject: TProject);
begin
  signalString(
    '{'+
      '"type": "session",'+
      '"payload":'+
      '{'+
        '"description":"'+sessionDescription(aProject)+'",'+
        jsonAddCommaOnNonEmpty(activeScenarioJSON)+
        jsonAddCommaOnNonEmpty(referenceScenarioJSON)+
        jsonAddCommaOnNonEmpty(controlsJSON)+
        jsonAddCommaOnNonEmpty(mapViewJSON)+
        '"scenarios":'+aProject.scenarioLinks.JSON+
      '}'+
    '}');
end;

procedure TClient.SendView(aLat, aLon, aZoom: Double);
begin
  if aZoom.IsNan
  then signalString(
         '{"type":"session","payload":{"view":{'+
           '"lat":'+Double.ToString(aLat, dotFormat)+','+
           '"lon":'+Double.ToString(aLon, dotFormat)+'}}}')
  else signalString(
         '{"type":"session","payload":{"view":{'+
           '"lat":'+Double.ToString(aLat, dotFormat)+','+
           '"lon":'+Double.ToString(aLon, dotFormat)+','+
           '"zoom":'+Double.ToString(aZoom, dotFormat)+'}}}');
end;

function TClient.sessionDescription(aProject: TProject): string;
begin
  Result := aProject.ProjectName;
  if Assigned(fCurrentScenario) then
  begin
    if fCurrentScenario.name<>''
    then Result := Result+' - '+fCurrentScenario.name;
    if fCurrentScenario.description<>'' then
    begin
      if fCurrentScenario.name<>''
      then Result := Result+', '+fCurrentScenario.description
      else Result := Result+' - '+fCurrentScenario.description;
    end;
  end;
end;

procedure TClient.setCanCopyScenario(const Value: Boolean);
begin
  if fCanCopyScenario<>Value then
  begin
    fCanCopyScenario := Value;
    if fCanCopyScenario
    then signalString('{"type": "canCopyScenario", "payload":1}')
    else signalString('{"type": "canCopyScenario", "payload":0}');
  end;
end;

procedure TClient.setControl(const aControlName, aControlValue: string);
begin
  TMonitor.Enter(fControls);
  try
    fControls.AddOrSetValue(aControlName, aControlValue);
  finally
    TMonitor.Exit(fControls);
  end;
end;

procedure TClient.setCurrentScenario(const aValue: TScenario);
begin
  fCurrentScenario := aValue;
  SendDomains('updatedomains');
  if Assigned(fCurrentScenario)
  then UpdateSession(fCurrentScenario.project)
  else UpdateSession(nil);
end;

procedure TClient.setRefScenario(const aValue: TScenario);
begin
  fRefScenario := aValue;
  SendDomains('updatedomains');
  if Assigned(fCurrentScenario)
  then UpdateSession(fCurrentScenario.project)
  else UpdateSession(nil);
end;

procedure TClient.signalControl(const aControlName: string);
begin
  signalString('{"type":"session","payload":{"'+aControlName+'":'+Control[aControlName]+'}}');
end;

procedure TClient.signalControls;
begin
  signalString('{"type":"session","payload":{'+controlsJSON+'}}');
end;

procedure TClient.signalString(const aString: string);
var
  stream: TStream;
begin
  // signal large string via stream
  if length(aString)>imbMaximumPayloadSize/10 then
  begin
    stream := TStringStream.Create(UTF8String(aString));
    try
      stream.Position := 0;
      fClientEvent.signalStream('string', stream);
    finally
      stream.Free;
    end;
    {$IFDEF DEBUG_WEBCLIENT}
    Log.WriteLn('Streamed to client '+Self.fClientID+': '+length(aString).toString ,llRemark);
    Log.WriteLn(Copy(aString, 1, 100), llRemark, 1);
    {$ENDIF}
  end
  else
  begin
    fClientEvent.signalString(aString);
    {$IFDEF DEBUG_WEBCLIENT}
    Log.WriteLn('Send to client '+Self.fClientID+': '+length(aString).toString ,llRemark);
    Log.WriteLn(Copy(aString, 1, 100), llRemark,1);
    {$ENDIF}
  end;
end;

procedure TClient.UpdateSession(aProject: TProject);
var
  scenariosJSON: string;
begin
  if Assigned(aProject)
  then scenariosJSON := '"scenarios":'+aProject.fScenarioLinks.JSON
  else scenariosJSON := '"scenarios":[]';
  signalString(
    '{"type": "session", "payload":{'+
      '"description":"'+sessionDescription(aProject)+'",'+
      jsonAddCommaOnNonEmpty(activeScenarioJSON)+
      jsonAddCommaOnNonEmpty(referenceScenarioJSON)+
      scenariosJSON+
    '}}');
end;

{ TScenarioElement }

constructor TScenarioElement.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aDisplayGroup: string);
begin
  inherited Create;
  fScenario := aScenario;
  fDomain := aDomain;
  fID := aID;
  fName := aName;
  fDescription := aDescription;
  fDefaultLoad := aDefaultLoad;
  fDisplayGroup := aDisplayGroup;
  //
//  fOutputEvent := fScenario.Project.Connection.publish(fScenario.Project.ProjectEvent.eventName+'.'+fScenario.ID+'.'+ID, false);
end;

destructor TScenarioElement.Destroy;
begin
//  if Assigned(fOutputEvent) then
//  begin
//    fOutputEvent.unPublish;
//    fOutputEvent := nil;
//  end;
  inherited;
end;

function TScenarioElement.getElementID: string;
begin
  Result := ID;
  if Assigned(scenario)
  then Result := fScenario.elementID+sepElementID+Result;
end;

function TScenarioElement.getJSON: string;
begin
  Result :=
    '"domain":"'+domain+'",'+
    '"id":"'+getElementID+'",'+
    '"name":"'+name+'",'+
    '"description":"'+description+'",'+
    '"displayGroup":"'+fDisplayGroup+'",'+
    '"show":'+Ord(defaultLoad).ToString; // todo: change to float -> opacity
end;

{ TLayerObject }

constructor TLayerObject.Create(aLayer: TLayer);
begin
  inherited Create;
  fLayer := aLayer;
end;

{function TLayerObject.distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double): Double;
begin
  Result := Infinity;
end;}

function TLayerObject.getExtent: TWDExtent;
begin
  Result := TWDExtent.Create;
end;

function TLayerObject.getValidGeometry: Boolean;
begin
  Result := False;
end;

procedure TLayerObject.translate(dx, dy: Double);
begin
  // nothing to translate on base class
end;

function TLayerObject.valueToColors(aValue: Double): TGeoColors;
begin
  if Assigned(fLayer.tilerLayer) and Assigned(fLayer.tilerLayer.palette)
  then result := fLayer.tilerLayer.palette.ValueToColors(aValue)
  else if Assigned(fLayer.fPalette)
  then result := fLayer.fPalette.ValueToColors(aValue)
  else result := TGeoColors.Create($FF000000); // black
end;

{ TLayerMarker }

constructor TLayerMarker.Create(aLayer: TLayer; const aID: TWDID; aLat, aLon: Double; const aTitle: string; aDraggable: Boolean{; aRadius: Double});
begin
  inherited Create(aLayer, aID);
  fGeometryPoint := TWDGeometryPoint.Create(aLon, aLat, NaN);
  fOpacity := 1.0;
  fTitle := aTitle;
  fAlt := '';
  fDraggable := aDraggable;
  fRiseOnHover := False;
  fRiseOffset := 0;
  //fRadius := aRadius;
end;

function TLayerMarker.distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double;
begin
  Result := aDistanceLatLon.distanceInMeters(Abs(fGeometryPoint.y-aY), Abs(fGeometryPoint.x-aX));
end;

function TLayerMarker.getExtent: TWDExtent;
begin
  Result.Init(fGeometryPoint.x, fGeometryPoint.y);
end;

function TLayerMarker.getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string;
var
  attr: string;
begin
  if aExtraJSON2DAttributes<>''
  then attr := ','+aExtraJSON2DAttributes
  else attr := '';
  Result :=
    '{'+
      '"id":"'+string(ID)+'"'+','+
      '"lat":'+DoubleToJSON(fGeometryPoint.y)+','+
      '"lng":'+DoubleToJSON(fGeometryPoint.x)+','+
      '"opacity":'+DoubleToJSON(fOpacity)+','+
      '"title":"'+fTitle+'",'+
      '"alt":"'+fAlt+'",'+
      '"draggable":'+Ord(fDraggable).ToString+','+
      '"riseOnHover":'+Ord(fRiseOnHover).ToString+','+
      '"riseOffset":'+fRiseOffset.ToString+{','+
      '"markerType":"'+aType+'"'+','+
      '"radius":'+DoubleToJSON(fRadius)+}
      attr+
    '}';
end;

function TLayerMarker.getValidGeometry: Boolean;
begin
  Result := Assigned(fGeometryPoint);
end;

function TLayerMarker.intersects(aGeometry: TWDGeometry): Boolean;
begin
  Result := aGeometry.PointInAnyPart(fGeometryPoint.x, fGeometryPoint.y);
end;

procedure TLayerMarker.translate(dx, dy: Double);
begin
  fGeometryPoint.translate(dx, dy);
end;

{ TGeometryPointLayerObject }

constructor TGeometryPointLayerObject.Create(aLayer: TLayer; const aID: TWDID; aGeometryPoint: TWDGeometryPoint; aValue: Double);
begin
  inherited Create(aLayer, aID);
  fGeometryPoint := aGeometryPoint;
  fValue := aValue;
end;

destructor TGeometryPointLayerObject.Destroy;
begin
  FreeAndNil(fGeometryPoint);
  inherited;
end;

function TGeometryPointLayerObject.distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double;
begin
  Result := aDistanceLatLon.distanceInMeters(Abs(fGeometryPoint.y-aY), Abs(fGeometryPoint.x-aX));
end;

function TGeometryPointLayerObject.Encode: TByteBuffer;
begin
  Result :=
    TByteBuffer.bb_tag_Double(icehTilerValue, value)+
    TByteBuffer.bb_tag_rawbytestring(icehTilerGeometryPoint, fGeometryPoint.Encode)+
    inherited Encode;
end;

function TGeometryPointLayerObject.getExtent: TWDExtent;
begin
  Result.Init(fGeometryPoint.x, fGeometryPoint.y);
end;

function TGeometryPointLayerObject.getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string;
var
  colors: TGeoColors;
  opacity: double;
  attr: string;
begin
  if Assigned(fGeometryPoint) then
  begin
    if aExtraJSON2DAttributes<>''
    then attr := ','+aExtraJSON2DAttributes
    else attr := '';
    colors := valueToColors(fValue);
    opacity := (colors.mainColor shr 24)/$FF;
    Result := '{ '+
        '"type":"Feature",'+
        '"geometry":{'+
          '"type":"'+fLayer.GeometryType+'",'+
          '"coordinates":'+fGeometryPoint.JSON2D[aType]+
        '},'+
        '"properties":{'+
          '"id":"'+string(ID)+'",'+
          '"color": "'+ColorToJSON(colors.mainColor)+'",'+
          '"fillOpacity":'+DoubleToJSON(opacity)+
          attr+
        '}'+
    	'}';
  end
  else Result := '';
end;

function TGeometryPointLayerObject.getValidGeometry: Boolean;
begin
  Result := Assigned(fGeometryPoint);
end;

function TGeometryPointLayerObject.intersects(aGeometry: TWDGeometry): Boolean;
begin
  Result := aGeometry.PointInAnyPart(fGeometryPoint.x, fGeometryPoint.y);
end;

procedure TGeometryPointLayerObject.translate(dx, dy: Double);
begin
  fGeometryPoint.translate(dx, dy);
end;

{ TGeometryPointHeightLayerObject }
{
constructor TGeometryPointHeightLayerObject.Create(aLayer: TLayer; const aID: TWDID; aGeometryPoint: TWDGeometryPoint; aValue,
  aHeight: Double);
begin
  inherited Create(aLayer, aID, aGeometryPoint, aValue);
  fHeight := aHeight;
end;

function TGeometryPointHeightLayerObject.Encode: TByteBuffer;
begin
  Result :=
    TByteBuffer.bb_tag_Double(icehTilerValue2, height)+
    inherited Encode;
end;
}

{ TGeometryLayerPOIObject }

constructor TGeometryLayerPOIObject.Create(aLayer: TLayer; const aID: TWDID; aPOI: Integer; aGeometryPoint: TWDGeometryPoint);
begin
  inherited Create(aLayer, aID);
  fPOI := aPOI;
  fGeometryPoint := aGeometryPoint;
end;

destructor TGeometryLayerPOIObject.Destroy;
begin
  FreeAndNil(fGeometryPoint);
  inherited;
end;

function TGeometryLayerPOIObject.distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double;
begin
  Result := aDistanceLatLon.distanceInMeters(Abs(fGeometryPoint.y-aY), Abs(fGeometryPoint.x-aX));
end;

function TGeometryLayerPOIObject.Encode: TByteBuffer;
begin
  Result :=
    TByteBuffer.bb_tag_int32(icehTilerPOI, poi)+
    TByteBuffer.bb_tag_rawbytestring(icehTilerGeometryPoint, fGeometryPoint.Encode)+
    inherited Encode;
end;

function TGeometryLayerPOIObject.getExtent: TWDExtent;
begin
  Result.Init(fGeometryPoint.x, fGeometryPoint.y);
  // todo: inflate for size of poi?
end;

function TGeometryLayerPOIObject.getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string;
var
  attr: string;
begin
  if Assigned(fGeometryPoint) then
  begin
    if aExtraJSON2DAttributes<>''
    then attr := ','+aExtraJSON2DAttributes
    else attr := '';
    Result := '{ '+
      '"type":"Feature",'+
      '"geometry":{'+
        '"type":"'+fLayer.GeometryType+'",'+
        '"coordinates":'+fGeometryPoint.JSON2D[aType]+
      '},'+
      '"properties":{'+
        '"id":"'+string(ID)+'",'+
        '"poi": "'+fPOI.ToString+'"'+
        attr+
      '}'+
    '}'
  end
  else
    Result := '';
end;

function TGeometryLayerPOIObject.getValidGeometry: Boolean;
begin
  Result := Assigned(fGeometryPoint);
end;

function TGeometryLayerPOIObject.intersects(aGeometry: TWDGeometry): Boolean;
begin
  Result := aGeometry.PointInAnyPart(fGeometryPoint.x, fGeometryPoint.y);
end;

procedure TGeometryLayerPOIObject.translate(dx, dy: Double);
begin
  fGeometryPoint.translate(dx, dy);
end;

{ TGeometryLayerObject }

constructor TGeometryLayerObject.Create(aLayer: TLayer; const aID: TWDID; aGeometry: TWDGeometry; aValue: Double);
begin
  inherited Create(aLayer, aID);
  fGeometry := aGeometry;
  fValue := aValue;
end;

destructor TGeometryLayerObject.Destroy;
begin
  FreeAndNil(fGeometry);
  inherited;
end;

// http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
function pDistance(const aDistanceLatLon: TDistanceLatLon; x, y, x1, y1, x2, y2: Double): Double;
var
  A, B, C, D: Double;
  dot, len_sq: Double;
  param: Double;
  xx, yy: Double;
  dx, dy: Double;
begin
  A := (x - x1)*aDistanceLatLon.m_per_deg_lon;
  B := (y - y1)*aDistanceLatLon.m_per_deg_lat;
  C := (x2 - x1)*aDistanceLatLon.m_per_deg_lon;
  D := (y2 - y1)*aDistanceLatLon.m_per_deg_lat;

  dot := A * C + B * D;
  len_sq := C * C + D * D;

  if (len_sq <> 0) //in case of 0 length line
  then param := dot / len_sq
  else param := -1;

  if (param < 0) then
  begin
    xx := x1;
    yy := y1;
  end
  else if (param > 1) then
  begin
    xx := x2;
    yy := y2;
  end
  else
  begin
    xx := x1 + param * (x2 - x1);
    yy := y1 + param * (y2 - y1);
  end;

  dx := (x - xx)*aDistanceLatLon.m_per_deg_lon;
  dy := (y - yy)*aDistanceLatLon.m_per_deg_lat;
  Result := sqrt(dx * dx + dy * dy);
end;

// https://math.stackexchange.com/a/274728
// returns the side a point is relative to a line, values are dependant on the orientation of the coordinate system that is used
// function is designed that with lat as y and lon as x this will return -1 for left, 0 for on the line and +1 for right
function pSide(const x, y, x1, y1, x2, y2: Double): Integer;
var
  d: Double;
begin
  d := (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1);
  if d < 0  then
    Result := -1
  else if d > 0 then
    Result := 1
  else
    Result := 0;
end;

function TGeometryLayerObject.distance(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; out aSide: Integer): Double;
var
  part: TWDGeometryPart;
  d: Double;
  p: Integer;
  x1,y1,x2,y2: Double;
begin
  aSide := 0;
  if (fLayer.fGeometryType='MultiPolygon') and fGeometry.PointInAnyPart(aX, aY)
  then Result := 0
  else
  begin
    Result := Infinity;
    for part in fGeometry.parts do
    begin
      if part.points.Count>0 then
      begin
        if part.points.Count>1 then
        begin
          // process all segments
          x2 := part.points[0].x;
          y2 := part.points[0].y;
          for p := 1 to part.points.Count-1 do
          begin
            x1 := x2;
            y1 := y2;
            x2 := part.points[p].x;
            y2 := part.points[p].y;
            // distance to line segment
            d := pDistance(aDistanceLatLon, aX, aY, x1, y1, x2, y2);
            if Result>d then
            begin
              Result := d;
              aSide := pSide(aX, aY, x1, y1, x2, y2);
            end;
          end;
        end
        else
        begin
          d := aDistanceLatLon.distanceInMeters(Abs(part.points[0].y-aY), Abs(part.points[0].x-aX));
          if Result>d then
          begin
            Result := d;
            aSide := 0;
          end;
        end;
      end;
    end;
  end;
end;

function TGeometryLayerObject.Encode: TByteBuffer;
begin
  Result :=
    TByteBuffer.bb_tag_Double(icehTilerValue, value)+
    TByteBuffer.bb_tag_rawbytestring(icehTilerGeometry, fGeometry.Encode)+
    inherited Encode;
end;

function TGeometryLayerObject.getExtent: TWDExtent;
begin
  Result := TWDExtent.FromGeometry(fGeometry);
end;

function TGeometryLayerObject.getJSON2D(const aSide: Integer; const aType, aExtraJSON2DAttributes: string): string;
var
  colors: TGeoColors;
  fillOpacity: double;
  attr, idstring, coordinates: string;
  strokeOpacity: double;
begin
  if Assigned(fGeometry) then
  begin
    if aExtraJSON2DAttributes<>''
    then attr := ','+aExtraJSON2DAttributes
    else attr := '';
    colors := valueToColors(fValue);
    fillOpacity := (colors.fillColor shr 24)/$FF;
    if colors.outlineColor<>0
    then strokeOpacity := (colors.outlineColor shr 24)/$FF
    else strokeOpacity := fillOpacity;
    idstring := string(ID);
    coordinates := fGeometry.JSON2D[aType];
    if aSide <> 0 then
    begin
      if aSide < 0 then
      begin
        idstring := 'L-' + string(ID);
        if (aType = gtLinestring) or (aType = gtMultiPoint) then
          coordinates := '[' + fGeometry.JSON2DSide[aSide, 0.0005] + ',' + coordinates.Substring(1); //TODO: dynamic factor and/or x/y on seperate factor?
      end
      else if aSide > 0 then
      begin
        idstring := 'R-' + string(ID);
        if (aType = gtLinestring) or (aType = gtMultiPoint) then
          coordinates := coordinates.Substring(0, coordinates.Length - 1) + ',' + fGeometry.JSON2DSide[aSide, 0.0005] + ']';
      end;
    end;
    Result := '{ '+
        '"type":"Feature",'+
        '"geometry":{'+
          '"type":"'+fLayer.GeometryType+'",'+
          '"coordinates":'+coordinates+
        '},'+
        '"properties":{'+
          '"id":"'+idstring+'",'+
          '"color": "'+ColorToJSON(colors.mainColor)+'",'+
          '"opacity":'+DoubleToJSON(strokeOpacity)+','+
          '"fillOpacity":'+DoubleToJSON(fillOpacity)+
          attr+
        '}'+
    	'}';
  end
  else Result := '';
end;

function TGeometryLayerObject.getValidGeometry: Boolean;
begin
  Result := Assigned(fGeometry);
end;

function TGeometryLayerObject.intersects(aGeometry: TWDGeometry): Boolean;
var
  part: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  for part in fGeometry.parts do
  begin
    for point in part.points do
    begin
      if aGeometry.PointInAnyPart(point.x, point.y)
      then Exit(True);
    end;
  end;
  Exit(False);
end;

procedure TGeometryLayerObject.translate(dx, dy: Double);
begin
  fGeometry.translate(dx, dy);
end;

{ TLayerUpdateObject }

constructor TLayerUpdateObject.Create(const aID: TWDID);
begin
  inherited Create;
  fID := aID;
  fAttributes := TDictionary<string, string>.Create;
end;

destructor TLayerUpdateObject.Destroy;
begin
  FreeAndNil(fAttributes);
  inherited;
end;

{ TAttrNameValue }

class function TAttrNameValue.Create(const aName, aValue: string): TAttrNameValue;
begin
  Result.name := aName;
  Result.value := aValue;
end;

{ TLayerBase }

constructor TLayerBase.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean;
  const aDisplayGroup: string; aShowInDomains: Boolean; aBasicLayer: Boolean; aOpacity: Double;
  const aLegendJSON: string; const aPreviewBase64: string);
begin
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, aDisplayGroup);
  fShowInDomains := aShowInDomains;
  fBasicLayer := aBasicLayer;
  fOpacity := aOpacity;
  fLegendJSON := aLegendJSON;
  fPreviewBase64 := aPreviewBase64;
end;

function TLayerBase.getJSON: string;
begin
  Result := inherited getJSON;
  jsonAddInteger(Result, 'basic', Ord(basicLayer));
  jsonAddDouble(Result, 'opacity', opacity);
  jsonAddString(Result, 'preview', previewBase64);
  jsonAddStruct(Result, 'legend', legendJSON);
  if basicLayer then
    jsonAddString(Result, 'basicID', id);
end;

function TLayerBase.getPreviewBase64: string;
begin
  Result := fPreviewBase64;
end;

procedure TLayerBase.handleUpdateLayerObject(aClient: TClient; aPayload: TJSONObject);
begin
  // default no action
end;

function TLayerBase.LoadPreviewFromCache(const aPreviewFileName: string): Boolean;
var
  previewsFolder: string;
  previewFilename: string;
begin
  // try to load preview from cache
  try
    previewsFolder := ExtractFilePath(ParamStr(0))+'previews';
    if aPreviewFileName=''
    then previewFilename := previewsFolder+'\'+elementID+'.png'
    else previewFilename := previewsFolder+'\'+aPreviewFileName;
    if FileExists(previewFilename) then
    begin
      previewBase64 := PNGFileToBase64(previewFilename);
      Result := True;
    end
    else
    begin
      Result := False;
    end;
  except
    Result := False;
  end;
end;

procedure TLayerBase.RegisterLayer;
begin
  // default no action
end;

procedure TLayerBase.setLegendJSON(const aLegendJSON: string);
begin
  fLegendJSON := aLegendJSON;
end;

procedure TLayerBase.setPreviewBase64(const aValue: string);
begin
  fPreviewBase64 := aValue;
end;

{ TExternalTilesLayer }

constructor TExternalTilesLayer.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription, aTilesURL: string;
  aDefaultLoad: Boolean; const aDisplayGroup: string; aShowInDomains, aBasicLayer: Boolean; aOpacity: Double; const aLegendJSON,
  aPreviewBase64: string);
begin
  inherited Create(
    aScenario, aDomain, aID, aName, aDescription,
    aDefaultLoad, aDisplayGroup, aShowInDomains, aBasicLayer, aOpacity, aLegendJSON, aPreviewBase64);
  fTilesURL := aTilesURL;
end;

function TExternalTilesLayer.getJSON: string;
begin
  Result := inherited getJSON;
  jsonAddString(Result, 'tiles', tilesURL);
  jsonAddString(Result, 'type', ltTile);
end;

{ TSimpleObject }

procedure TSimpleObject.addOptionColor(const aName: string; aColor: TAlphaRGBPixel);
begin
  fOptions.AddOrSetValue(aName, '"'+ColorToJSON(aColor)+'"');
end;

procedure TSimpleObject.addOptionDouble(const aName: string; aValue: Double);
begin
  fOptions.AddOrSetValue(aName, DoubleToJSON(aValue));
end;

procedure TSimpleObject.addOptionGeoColor(const aColor: TGeoColors);
// todo: make same as TGeoColors.toJSON (in WorldDataCode)
begin
  fOptions.AddOrSetValue('color', '"'+ColorToJSON(aColor.outlineColor)+'"');
  if APOpacity(aColor.outlineColor)<>1.0
  then fOptions.AddOrSetValue('opacity', OpacityToJSON(aColor.outlineColor));
  if aColor.fillColor<>0 then
  begin
    fOptions.AddOrSetValue('fillColor', '"'+ColorToJSON(aColor.fillColor)+'"');
    fOptions.AddOrSetValue('fillOpacity', OpacityToJSON(aColor.fillColor));
  end
  else
  begin
    // leaflets default fillOpacity is 0.2, but legend assumes 1.0 if no fillColor is specified
    // already default: fOptions.AddOrSetValue('fillColor', '"'+ColorToJSON(aColor.outlineColor)+'"');
    fOptions.AddOrSetValue('fillOpacity', OpacityToJSON(aColor.outlineColor));
  end;
end;

procedure TSimpleObject.addOptionInteger(const aName: string; aValue: Integer);
begin
  fOptions.AddOrSetValue(aName, aValue.ToString);
end;

procedure TSimpleObject.addOptionString(const aName, aValue: string);
begin
  fOptions.AddOrSetValue(aName, '"'+aValue+'"');
end;

procedure TSimpleObject.addOptionStructure(const aName, aValue: string);
begin
  fOptions.AddOrSetValue(aName, aValue); // for now just pass straight as string (caller has to enclose in {} or [] ..)
end;

procedure TSimpleObject.addPropertyDouble(const aName: string; aValue: Double);
begin
  fOtherProperties.AddOrSetValue(aName, DoubleToJSON(aValue));
end;

procedure TSimpleObject.addPropertyInteger(const aName: string; aValue: Integer);
begin
  fOtherProperties.AddOrSetValue(aName, aValue.ToString);
end;

procedure TSimpleObject.addPropertyString(const aName, aValue: string);
begin
  fOtherProperties.AddOrSetValue(aName, '"'+aValue+'"');
end;

procedure TSimpleObject.addPropertyStructure(const aName, aValue: string);
begin
  fOtherProperties.AddOrSetValue(aName, aValue); // for now just pass straight as string (caller has to enclose in {} or [] ..)
end;

constructor TSimpleObject.Create(aSimpleLayer: TSimpleLayer; const aID, aClassName: string;
  aGeometry: TWDGeometryBase; const aGeometryType: string;
  const aOptions: TArray<TArray<string>>;
  const aOtherProperties: TArray<TArray<string>>);
begin
  inherited Create;
  fSimpleLayer := aSimpleLayer;
  fID := aID;
  fGeometry := aGeometry;
  fGeometryType := aGeometryType;
  fOptions := TDictionary<string, string>.Create;
  ConvertOptions(aOptions, fOptions);
  fOtherProperties := TDictionary<string, string>.Create;
  ConvertOptions(aOtherProperties, fOtherProperties);
  // add class name etc. to other properties
  fOtherProperties.AddOrSetValue(sojnObjectType, '"'+aClassName+'"');
end;

constructor TSimpleObject.Create(aSimpleLayer: TSimpleLayer; const aID, aClassName: string;
  aLat, aLon, aHeight: Double;
  const aOptions: System.TArray<TSimpleNameValue>;
  const aOtherProperties: System.TArray<TSimpleNameValue>);
begin
  Create(aSimpleLayer, aID, aClassName,
    TWDGeometryPoint.Create(aLon, aLat, aHeight), gtPoint,
    aOptions,
    aOtherProperties);
end;

destructor TSimpleObject.Destroy;
begin
  FreeAndNil(fOptions);
  FreeAndNil(fGeometry);
  FreeAndNil(fOtherProperties);
  inherited;
end;

function TSimpleObject.jsonGeometry: string;
begin
  Result := '"'+sojnGeometry+'"'+':'+jsonGeometryValue;
end;

function TSimpleObject.jsonGeometryValue: string;
begin
  Result := fGeometry.JSONLatLon[fGeometryType];
end;

function TSimpleObject.jsonNewObject: string;
begin
  Result := '';
  jsonAdd(Result, jsonOptions);
  jsonAdd(Result, jsonGeometry);
  jsonAdd(Result, jsonOtherProperties);
end;

function TSimpleObject.jsonOptions: string;
begin
  Result := '"'+sojnOptions+'":'+jsonOptionsValue;
end;

function TSimpleObject.jsonOptionsValue: string;
var
  o: TPair<string, string>;
begin
  Result := '';
  for o in fOptions
  do jsonAdd(Result, '"'+o.Key+'":'+o.Value);
  Result := '{'+Result+'}';
end;

function TSimpleObject.jsonOtherProperties: string;
var
  op: TPair<string, string>;
begin
  Result := '';
  for op in fOtherProperties
  do jsonAdd(Result, '"'+op.Key+'":'+op.Value);
end;

procedure TSimpleObject.setGeometry(const aValue: TWDGeometryBase);
begin
  if fGeometry<>aValue then
  begin
    fGeometry.Free;
    fGeometry := aValue;
  end;
end;

{ TCircleMarker }

constructor TCircleMarker.Create(aSimpleLayer: TSimpleLayer; const aID: string; aLat, aLon, aRadius, aHeight: Double;
  const aOptions, aOtherProperties: TArray<TArray<string>>);
begin
  inherited Create(aSimpleLayer, aID, 'L.circleMarker', TWDGeometryPoint.Create(aLon, aLat, aHeight), gtPoint, aOptions, aOtherProperties);
  addOptionDouble(sojnRadius, aRadius);
end;

function TCircleMarker.getRadius: Double;
var
  optionValue: string;
begin
  if fOptions.TryGetValue(sojnRadius, optionValue)
  then Result := Double.Parse(optionValue, dotFormat)
  else Result := 10; // leaflet default
end;

procedure TCircleMarker.setRadius(const aValue: Double);
begin
  addOptionDouble(sojnRadius, aValue);
end;

{ TSimpleLayer }

procedure TSimpleLayer.AddObject(aObject: TSimpleObject; const aInitialization: string);
var
  pendingInitialization: string;
  o: TSimpleObject;
begin
  TMonitor.Enter(fObjects);
  try
    TMonitor.Enter(fObjectsAdded);
    try
      if fObjects.TryGetValue(aObject.id, o) then
      begin
        // are we talking about the same object?
        if aObject<>o then
        begin
          // remove previous (different) object
          fObjectsAdded.Remove(o.id);
          // remove changes for previous object
          TMonitor.Enter(fObjectsUpdated);
          try
            fObjectsUpdated.Remove(o.id);
          finally
            TMonitor.Exit(fObjectsUpdated);
          end;
          // add new object
          fObjects.AddOrSetValue(aObject.id, aObject);
        end;
      end
      else fObjects.Add(aObject.id, aObject);
      // signal the changes
      if not fObjectsAdded.TryGetValue(aObject.id, pendingInitialization)
      then pendingInitialization := '';
      jsonAdd(pendingInitialization, aInitialization);
      fObjectsAdded.AddOrSetValue(aObject.id, aInitialization);
    finally
      TMonitor.Exit(fObjectsAdded);
    end;
    // remove pending removes
    TMonitor.Enter(fObjectsRemoved);
    try
      fObjectsRemoved.Remove(aObject.id);
    finally
      TMonitor.Exit(fObjectsRemoved);
    end;
    triggerLayerUpdate;
  finally
    TMonitor.Exit(fObjects);
  end;
end;

constructor TSimpleLayer.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string;
  const aOptions: TArray<TArray<string>>;
  const aOtherProperties: TArray<TArray<string>>;
  aDefaultLoad: Boolean; const aDisplayGroup: string; aShowInDomains: Boolean;
  aBasicLayer: Boolean; aOpacity: Double; const aLegendJSON: string; aLayerUpdateFrequency: Double);
begin
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, aDisplayGroup, aShowInDomains, aBasicLayer, aOpacity, aLegendJSON);
  fObjects := TObjectDictionary<string, TSimpleObject>.Create([doOwnsValues]);
  fObjectsAdded := TDictionary<string, string>.Create;
  fObjectsUpdated := TObjectDictionary<string, TDictionary<string, string>>.Create([doOwnsValues]);
  fObjectsRemoved := TDictionary<string, string>.Create;
  fOptions := TDictionary<string, string>.Create;
  ConvertOptions(aOptions, fOptions);
  fOtherProperties := TDictionary<string, string>.Create;
  ConvertOptions(aOtherProperties, fOtherProperties);
  fLayerUpdateTimer := scenario.project.Timers.CreateInactiveTimer;
  fLayerUpdateTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond/aLayerUpdateFrequency);
  //LoadPreviewFromCache; // todo: check other derived classes might still be manual done there..
end;

destructor TSimpleLayer.Destroy;
begin
  if Assigned(fLayerUpdateTimer) then
  begin
    fLayerUpdateTimer.Cancel;
    fLayerUpdateTimer := nil;
  end;
  FreeAndNil(fObjectsAdded);
  FreeAndNil(fObjectsUpdated);
  FreeAndNil(fObjectsRemoved);
  FreeAndNil(fObjects);
  FreeAndNil(fOptions);
  FreeAndNil(fOtherProperties);
  inherited;
end;

function TSimpleLayer.getJSON: string;
begin
  Result := inherited getJSON;
  jsonAddString(Result, 'type', ltSimple);
  jsonAdd(Result, jsonOptions);
  jsonAdd(Result, jsonOtherProperties);
end;

function TSimpleLayer.HandleClientSubscribe(aClient: TClient): Boolean;
var
  isop: TPair<string, TSimpleObject>;
  _json: string;
begin
  Result := inherited;
  _json := '';
  TMonitor.Enter(objects);
  try
    for isop in objects do
    begin
      jsonAdd(_json, '{"newobject":{"id":"'+isop.Key+'"');
      jsonAdd(_json, isop.Value.jsonNewObject);
      _json := _json+'}}';
    end;
  finally
    TMonitor.Exit(objects);
  end;
  if _json<>'' then
  begin
    _json := '{"type":"updatelayer","payload":{"id":"'+ElementID+'","data":['+_json+']}}';
    aClient.signalString(_json);
  end;
end;

procedure TSimpleLayer.handleLayerUpdateTrigger(aTimer: TTimer; aTime: THighResTicks);
var
  iap: TPair<string, string>;
  iadp: TPair<string, TDictionary<string, string>>;
  _json: string;
  avp: TPair<string, string>;
begin
  _json := '';
  // process added objects
  TMonitor.Enter(fObjectsAdded);
  try
    for iap in fObjectsAdded do
    begin
      jsonAdd(_json, '{"newobject":{"id":"'+iap.Key+'"');
      jsonAdd(_json, iap.Value);
      _json := _json+'}}';
    end;
    fObjectsAdded.Clear;
  finally
    TMonitor.Exit(fObjectsAdded);
  end;
  // process object updates
  TMonitor.Enter(fObjectsUpdated);
  try
    for iadp in fObjectsUpdated do
    begin
      jsonAdd(_json, '{"updateobject":{"id":"'+iadp.Key+'"');
      for avp in iadp.Value
      do jsonAdd(_json, '"'+avp.key+'":'+avp.value);
      _json := _json+'}}';
      end;
    fObjectsUpdated.Clear;
  finally
    TMonitor.Exit(fObjectsUpdated);
  end;
  // process removed objects
  TMonitor.Enter(fObjectsRemoved);
  try
    for iap in fObjectsRemoved do
    begin
      jsonAdd(_json, '{"removeobject":{"id":"'+iap.Key+'"');
      // values are not processed, we use a dictionary to quickly find the id to avoid duplicate removals
      _json := _json+'}}';
    end;
    fObjectsRemoved.Clear;
  finally
    TMonitor.Exit(fObjectsRemoved);
  end;
  if _json<>'' then
  begin
    _json := '{"type":"updatelayer","payload":{"id":"'+ElementID+'","data":['+_json+']}}';
    forEachSubscriber<TClient>(
      procedure(aClient: TClient)
      begin
        if IsReceivingLayerUpdates(aClient)
        then aClient.signalString(_json);
      end);
  end;
end;

function TSimpleLayer.IsReceivingLayerUpdates(aClient: TClient): Boolean;
begin
  // default all clients receive updates
  Result := True;
end;

function TSimpleLayer.jsonOptions: string;
begin
  Result := '"'+sojnOptions+'":'+jsonOptionsValue;
end;

function TSimpleLayer.jsonOptionsValue: string;
var
  o: TPair<string, string>;
begin
  Result := '';
  for o in fOptions
  do jsonAdd(Result, '"'+o.Key+'":'+o.Value);
  Result := '{'+Result+'}';
end;

function TSimpleLayer.jsonOtherProperties: string;
var
  op: TPair<string, string>;
begin
  Result := '';
  for op in fOtherProperties
  do jsonAdd(Result, '"'+op.Key+'":'+op.Value);
end;

procedure TSimpleLayer.RemoveObject(aObject: TSimpleObject);
begin
  TMonitor.Enter(fObjects);
  try
    // add to objects to be removed
    TMonitor.Enter(fObjectsRemoved);
    try;
      fObjectsRemoved.Add(aObject.ID, '');
    finally
      TMonitor.Exit(fObjectsRemoved);
    end;
    // remove pending additions
    TMonitor.Enter(fObjectsAdded);
    try;
      fObjectsAdded.Remove(aObject.ID);
    finally
      TMonitor.Exit(fObjectsAdded);
    end;
    // remove pending udpates
    TMonitor.Enter(fObjectsUpdated);
    try
      fObjectsUpdated.Remove(aObject.ID);
    finally
      TMonitor.Exit(fObjectsUpdated);
    end;
    triggerLayerUpdate;

    fObjects.Remove(aObject.ID);
  finally
    TMonitor.Exit(fObjects);
  end;
end;

procedure TSimpleLayer.setLegendJSON(const aLegendJSON: string);
var
  _json: string;
begin
  if fLegendJSON<>aLegendJSON then
  begin
    inherited;
    _json := '{"type":"updatelayer","payload":{"id":"'+ElementID+'","legend":{'+legendJSON+'}}';
      forEachSubscriber<TClient>(procedure(aClient: TClient)
        begin
          aClient.signalString(_json);
        end);
  end;
end;

procedure TSimpleLayer.setPreviewBase64(const aValue: string);
var
  _json: string;
begin
  if fPreviewBase64<>aValue then
  begin
    inherited;
    _json := '{"type":"updatelayer","payload":{"id":"'+ElementID+'","preview":"'+previewBase64+'"}}';
    forEachSubscriber<TClient>(
      procedure(aClient: TClient)
      begin
        aClient.signalString(_json);
      end);
  end;
end;

procedure TSimpleLayer.triggerLayerUpdate;
begin
  fLayerUpdateTimer.Arm(fLayerUpdateTimer.MaxPostponeDelta, handleLayerUpdateTrigger);
end;

function TSimpleLayer.UpdateObject(aObject: TSimpleObject; const aName, aJSONValue: string): Boolean;
var
  o: TSimpleObject;
  pendingUpdates: TDictionary<string, string>;
begin
  TMonitor.Enter(fObjects);
  try
    // first check if we are given a valid and active object
    if fObjects.TryGetValue(aObject.id, o) and (aObject=o) then
    begin
      // todo: removed next line, should be done by caller
      //aObject.addPropertyStructure(aName, aJSONValue);
      // add the changes
      TMonitor.Enter(fObjectsUpdated);
      try
        if not fObjectsUpdated.TryGetValue(aObject.id, pendingUpdates) then
        begin
          pendingUpdates := TDictionary<string, string>.Create;
          fObjectsUpdated.Add(aObject.id, pendingUpdates);
        end;
        pendingUpdates.AddOrSetValue(aName, aJSONValue);
      finally
        TMonitor.Exit(fObjectsUpdated);
      end;
      triggerLayerUpdate;
      // return that we found the object to update
      Result := True;
    end
    else Result := False;
  finally
    TMonitor.Exit(fObjects);
  end;
end;

{ TLayer }

procedure TLayer.addDiffLayer(aDiffLayer: TDiffLayer);
var
  i: Integer;
begin
  TMonitor.Enter(fDependentDiffLayers);
  try
    i := fDependentDiffLayers.IndexOf(aDiffLayer);
    if i<0 then
    begin
      fDependentDiffLayers.Add(aDiffLayer);
      // subscribe all clients to this diff layer also
      //TODO: rework layer subscription
      {forEachClient(
        procedure(aClient: TClient)
        begin
          aDiffLayer.HandleClientSubscribe(aClient);
        end);
        }
    end;
  finally
    TMonitor.Exit(fDependentDiffLayers);
  end;
end;

function TLayer.AddObject(aObject: TLayerObjectWithID): TLayerObjectWithID;
begin
  fObjectsLock.BeginWrite;
  try
    fObjects.AddOrSetValue(aObject.id, aObject);
    signalObject(aObject);
  finally
    fObjectsLock.EndWrite;
  end;
  Result := aObject;
  triggerLayerUpdate;
end;

procedure TLayer.AddObjectAttribute(const aID: TWDID; const aAttributes: TArray<TAttrNameValue>);
var
  luo: TLayerUpdateObject;
  nv: TAttrNameValue;
begin
  TMonitor.Enter(fObjectsAdded);
  try
    if not fObjectsAdded.TryGetValue(aID, luo) then
    begin
      luo := TLayerUpdateObject.Create(aID);
      fObjectsAdded.Add(aID, luo);
    end;
    for nv in aAttributes do
    begin
      luo.attributes.AddOrSetValue(nv.name, nv.value);
    end;
  finally
    TMonitor.Exit(fObjectsAdded);
  end;
  triggerLayerUpdate;
end;

procedure TLayer.ClearObjects;
var
  IDs: TArray<TWDID>;
  o: TLayerObjectWithID;
  _id: TWDID;
begin
  fObjectsLock.BeginWrite;
  try
    TMonitor.Enter(fObjectsAdded);
    try
      TMonitor.Enter(fObjectsUpdated);
      try
        TMonitor.Enter(fObjectsDeleted);
        try
          IDs := fObjects.Keys.ToArray;
          for _id in IDs do
          begin
            o := fObjects[_id];
            fObjectsAdded.Remove(_id);
            fObjectsUpdated.Remove(_id);
            fObjects.ExtractPair(_id); // extract from active list
            fObjectsDeleted.Add(o); // add to deleted list
            signalNoObject(o);
          end;
        finally
          TMonitor.Exit(fObjectsDeleted);
        end;
      finally
        TMonitor.Exit(fObjectsUpdated);
      end;
    finally
      TMonitor.Exit(fObjectsAdded);
    end;
  finally
    fObjectsLock.EndWrite;
  end;
  triggerLayerUpdate;
end;

constructor TLayer.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean;
  const aObjectTypes, aGeometryType, aLayerType: string; aShowInDomains: Boolean; aDiffRange: Double;
  aBasicLayer: Boolean; aOpacity: Double; const aDisplayGroup: string; const
  aLegendJSON: string; const aPreviewBase64: string; aPalette: TWDPalette;
  aLayerUpdateFrequency: Double;
  aTilerMaxRefreshTime: Integer);
begin
  if not Assigned(aScenario.Subscribers)
  then log.WriteLn('TLayer.Create: aScenario.Subscribers=nil', llError);

  inherited Create(
    aScenario, aDomain, aID, aName, aDescription,
    aDefaultLoad, aDisplayGroup, aShowInDomains, aBasicLayer, aOpacity,
    aLegendJSON, aPreviewBase64);
  fPalette := aPalette;
  fObjectTypes := aObjectTypes;
  fGeometryType := aGeometryType;
  fDiffRange := aDiffRange;
  fLayerType := aLayerType;
  fObjects := TObjectDictionary<TWDID, TLayerObjectWithID>.Create([doOwnsValues]);

  fObjectsAdded := TObjectDictionary<TWDID, TLayerUpdateObject>.Create([doOwnsValues]);// <TLayerObjectWithID>.Create(False); // refs
  fObjectsUpdated := TObjectDictionary<TWDID, TLayerUpdateObject>.Create([doOwnsValues]); // owns
  fObjectsDeleted := TObjectList<TLayerObjectWithID>.Create(True); // owns

  fExtraJSON2DAttributes := '';

  fObjectsLock.Create;
  fDependentDiffLayers := TObjectList<TDiffLayer>.Create(False);
  fQuery := '';
  fTilerLayer := nil;

  fTilerRefreshTime := DefaultTilerRefreshTime;
  fTilerPreviewTime := DefaultTilerPreviewTime;
  fPreviewRequestTimer := scenario.project.Timers.SetTimer(
    procedure (aTimer: TTImer; aTime: THighResTicks)
    begin
      if Assigned(fTilerLayer) then
      begin
        Log.WriteLn('triggered preview timer for '+elementID);
        fTilerLayer.signalRequestPreview;
      end
      else Log.WriteLn('## triggered preview timer for '+elementID+' without having a tiler layer', llError);
    end);
  fSendRefreshTimer := scenario.project.Timers.CreateInactiveTimer;// SetTimer(handleLayerRefresh);
  fSendRefreshTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond*aTilerMaxRefreshTime);
  fLayerUpdateTimer := scenario.project.Timers.CreateInactiveTimer;
  fLayerUpdateTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond/aLayerUpdateFrequency);
end;

destructor TLayer.Destroy;
begin
  if Assigned(fSendRefreshTimer) then
  begin
    fSendRefreshTimer.Cancel;
    fSendRefreshTimer := nil;
  end;
  if Assigned(fPreviewRequestTimer) then
  begin
    fPreviewRequestTimer.Cancel;
    fPreviewRequestTimer := nil;
  end;
  if Assigned(fLayerUpdateTimer) then
  begin
    fLayerUpdateTimer.Cancel;
    fLayerUpdateTimer := nil;
  end;
  FreeAndNil(fTilerLayer);
  inherited;
  FreeAndNil(fObjectsAdded);
  FreeAndNil(fObjectsUpdated);
  FreeAndNil(fObjectsDeleted);
  FreeAndNil(fObjects);
  FreeAndNil(fDependentDiffLayers);
  FreeAndNil(fPalette);
end;

function TLayer.ExtractObject(aObject: TLayerObjectWithID): TLayerObjectWithID;
var
  p: TPair<TWDID, TLayerObjectWithID>;
begin
  fObjectsLock.BeginWrite;
  try
    p := fObjects.ExtractPair(aObject.id);
    Result := p.Value;
  finally
    fObjectsLock.EndWrite;
  end;
end;

function TLayer.findNearestObject(const aDistanceLatLon: TDistanceLatLon; aX, aY: Double; var aDistance: Double; out aSide: Integer): TLayerObjectWithID;
// initialize aDistance to Infinity to get nearest object
var
  o: TPair<TWDID, TLayerObjectWithID>;
  d: double;
  curSide: Integer;
begin
  Result := nil;
  aSide := 0;
  fObjectsLock.BeginRead;
  try
    for o in  fObjects do
    begin
      d := o.value.distance(aDistanceLatLon, aX, aY, curSide);
      if d=0 then
      begin
        aDistance := d;
        aSide := curSide;
        Result := o.Value;
        break;
      end
      else
      begin
        if aDistance>d then
        begin
          aDistance := d;
          aSide := curSide;
          Result := o.Value;
        end;
      end;
    end;
  finally
    fObjectsLock.EndRead;
  end;
end;

function TLayer.FindObject(const aID: TWDID; out aObject: TLayerObjectWithID): Boolean;
begin
  fObjectsLock.BeginRead;
  try
    Result := fObjects.TryGetValue(aID, aObject);
  finally
    fObjectsLock.EndRead;
  end;
end;

function TLayer.findObjectsInCircle(const aDistanceLatLon: TDistanceLatLon; aX, aY, aRadius: Double; var aObjectsJSON: string): Integer;
var
  o: TPair<TWDID, TLayerObjectWithID>;
  d: double;
  side: Integer; //unused for now!
begin
  Result := 0;
  fObjectsLock.BeginRead;
  try
    for o in fObjects do
    begin
      d := o.Value.distance(aDistanceLatLon, aX, aY, side);
      if d<=aRadius then
      begin
        jsonAdd(aObjectsJSON, o.Value.JSON2D[0, Self.fGeometryType, '']);
        Result := Result+1;
      end;
    end;
  finally
    fObjectsLock.EndRead;
  end;
end;

function TLayer.findObjectsInGeometry(const aGeometryExtent: TWDExtent; aGeometry: TWDGeometry; var aObjectsJSON, aObjectsID: string): Integer;
var
  o: TPair<TWDID, TLayerObjectWithID>;
begin
  Result := 0;
  fObjectsLock.BeginRead;
  try
    for o in fObjects do
    begin
      if aGeometryExtent.Intersects(o.Value.Extent) then
      begin
        if o.Value.intersects(aGeometry) then
        begin
          jsonAdd(aObjectsJSON, o.Value.JSON2D[0, Self.fGeometryType, '']);
          jsonAdd(aObjectsID, string(o.Value.ID));
          Result := Result+1;
        end;
      end;
    end;
  finally
    fObjectsLock.EndRead;
  end;
end;

procedure TLayer.ReadObjectsDBSVGPaths(aQuery: TDataSet; aDefaultValue: Double);
var
  _id: TWDID;
  _path: string;
  _value: Double;
  obj: TLayerObjectWithID;
begin
  aQuery.Open;
  fObjectsLock.BeginWrite;
  try
    if Assigned(tilerLayer)
    then tilerLayer.signalSliceAction(tsaClearSlice); //force clear of our current slice
    fObjects.Clear;
    while not aQuery.Eof do
    begin
      try
        _id := aQuery.Fields[0].AsAnsiString;
        if not aQuery.Fields[1].IsNull then
        begin
          _path := aQuery.Fields[1].Value;
          if (aQuery.Fields.Count>2) and not aQuery.Fields[2].IsNull
          then _value := aQuery.Fields[2].AsFloat
          else _value := aDefaultValue;
          if _path.StartsWith('cx=')
          then obj := TGeometryPointLayerObject.Create(Self, _id, TWDGeometryPoint.CreateFromSVGPath(_path), _value)
          else obj := TGeometryLayerObject.Create(Self, _id, TWDGeometry.CreateFromSVGPath(_path), _value);
          fObjects.Add(obj.id, obj);
        end;
      except
        on e: Exception
        do Log.WriteLn(elementID+': exception in ReadObjectsDBSVGPaths: '+e.Message, llError);
      end;
      aQuery.Next;
    end;
  finally
    fObjectsLock.EndWrite;
  end;
end;

procedure TLayer.RegisterLayer;
begin
  FreeAndNil(fTilerLayer); // default to not-registering (not enough information)
end;

procedure TLayer.RegisterOnTiler(aPersistent: Boolean; aSliceType: Integer; const aDescription: string; aEdgeLengthInMeters: Double; aPalette: TWDPalette);
begin
  fTilerLayer.Free;
  // recreate tiler layer definition
  fTilerLayer := TTilerLayer.Create(
    scenario.project.Connection, elementID, aSliceType,
    handleTilerInfo, handleTilerRefresh, handleTilerPreview,
    aPalette);
  fPreviewBase64 := fTilerLayer.previewAsBASE64;
  // trigger registration
  fTilerLayer.signalRegisterLayer(scenario.project.tiler, aDescription, aPersistent, aEdgeLengthInMeters);
end;

procedure TLayer.RegisterSlice;
begin
  // default no action
end;

procedure TLayer.removeDiffLayer(aDiffLayer: TDiffLayer);
var
  i: Integer;
begin
  TMonitor.Enter(fDependentDiffLayers);
  try
    i := fDependentDiffLayers.IndexOf(aDiffLayer);
    if i>=0 then
    begin
      fDependentDiffLayers.Delete(i);
      // unsubscribe all clients from this diff layer also
      //TODO: rework layer subscription
      {
      forEachClient(
        procedure(aClient: TClient)
        begin
          aDiffLayer.HandleClientUnsubscribe(aClient);
        end);
      }
    end;
  finally
    TMonitor.Exit(fDependentDiffLayers);
  end;
end;

procedure TLayer.RemoveObject(aObject: TLayerObjectWithID);
begin
  fObjectsLock.BeginWrite;
  try
    //fObjectsAdded.Remove(aObject);
    TMonitor.Enter(fObjectsAdded);
    try
      fObjectsAdded.Remove(aObject.ID);
    finally
      TMonitor.Exit(fObjectsAdded);
    end;
    TMonitor.Enter(fObjectsUpdated);
    try
      fObjectsUpdated.Remove(aObject.ID);
    finally
      TMonitor.Exit(fObjectsUpdated);
    end;
    fObjects.ExtractPair(aObject.id); // extract from active list
    TMonitor.Enter(fObjectsDeleted);
    try
      fObjectsDeleted.Add(aObject); // add to deleted list
    finally
      TMonitor.Exit(fObjectsDeleted);
    end;
    signalNoObject(aObject);
  finally
    fObjectsLock.EndWrite;
  end;
  triggerLayerUpdate;
end;

function TLayer.RemoveObjectOnID(const aID: TWDID): Boolean;
var
  o: TLayerObjectWithID;
begin
  fObjectsLock.BeginWrite;
  try
    if fObjects.TryGetValue(aID, o) then
    begin
      TMonitor.Enter(fObjectsAdded);
      try
        fObjectsAdded.Remove(o.ID);
      finally
        TMonitor.Exit(fObjectsAdded);
      end;
      TMonitor.Enter(fObjectsUpdated);
      try
        fObjectsUpdated.Remove(o.ID);
      finally
        TMonitor.Exit(fObjectsUpdated);
      end;
      fObjects.ExtractPair(o.id); // extract from active list
      TMonitor.Enter(fObjectsDeleted);
      try
        fObjectsDeleted.Add(o); // add to deleted list
      finally
        TMonitor.Exit(fObjectsDeleted);
      end;
      signalNoObject(o);
      Result := True;
    end
    else Result := False;
  finally
    fObjectsLock.EndWrite;
  end;
  if Result
  then triggerLayerUpdate;
end;

procedure TLayer.RemoveObjects(const aIDs: TArray<TWDID>);
var
  IDs: TArray<TWDID>;
  o: TLayerObjectWithID;
  _id: TWDID;
begin
  fObjectsLock.BeginWrite;
  try
    TMonitor.Enter(fObjectsAdded);
    try
      TMonitor.Enter(fObjectsUpdated);
      try
        TMonitor.Enter(fObjectsDeleted);
        try
          for _id in IDs do
          begin
            if fObjects.TryGetValue(_id, o) then
            begin
              fObjectsAdded.Remove(_id);
              fObjectsUpdated.Remove(_id);
              fObjects.ExtractPair(_id); // extract from active list
              fObjectsDeleted.Add(o); // add to deleted list
              signalNoObject(o);
            end
            else Log.WriteLn('TLayer.RemoveObjects on '+string(UTF8String(ElementID))+': object '+string(UTF8String(_id))+' not found', llWarning);
          end;
        finally
          TMonitor.Exit(fObjectsDeleted);
        end;
      finally
        TMonitor.Exit(fObjectsUpdated);
      end;
    finally
      TMonitor.Exit(fObjectsAdded);
    end;
  finally
    fObjectsLock.EndWrite;
  end;
  triggerLayerUpdate;
end;

const
  MaxBufferLength = 2048;

procedure TLayer.signalObjects(aSender: TObject; aObjects: TObjectDictionary<TWDID, TLayerObjectWithID>);
var
  timeStamp: TDateTime;
  iop: TPair<TWDID, TLayerObjectWithID>;
  buffer: TByteBuffer;

  procedure Commit;
  begin
    if buffer<>'' then
    begin
      fTilerLayer.signalData(buffer, timeStamp);
      buffer := '';
    end;
  end;

begin
  if Assigned(fTilerLayer) then
  begin
    // todo: replace with batch buffer
    timeStamp := 0; // todo:
    fTilerLayer.signalPalette(timeStamp);
    // signal objects (which will trigger creation of layer if not already done above)
    buffer := '';
    for iop in aObjects do
    begin
      buffer := buffer+iop.Value.encode;
      if length(buffer)> MaxBufferLength
      then Commit;
    end;
    Commit;
  end;
end;

procedure TLayer.signalNoObject(aObject: TLayerObjectWithID);
var
  timeStamp: TDateTime;
begin
  timeStamp := 0; // todo:
  if Assigned(fTilerLayer)
  then fTilerLayer.signalData(aObject.EncodeRemove, timeStamp);
end;

procedure TLayer.signalObject(aObject: TLayerObjectWithID);
var
  timeStamp: TDateTime;
begin
  timeStamp := 0; // todo:
  if Assigned(fTilerLayer)
  then fTilerLayer.signalData(aObject.encode, timeStamp);
end;

procedure TLayer.signalObjects(aSender: TObject);
begin
  signalObjects(aSender, fObjects);
end;

//function TLayer.signalObjectsInGeometry(const aGeometryExtent: TWDExtent;
//  aGeometry: TWDGeometry; aTilerLayer: TTilerLayer): Integer;
//var
//  o: TPair<TWDID, TLayerObjectWithID>;
//begin
//  Result := 0;
//  for o in fObjects do
//  begin
//    if aGeometryExtent.Intersects(o.Value.Extent) then
//    begin
//      if o.Value.intersects(aGeometry) then
//      begin
//        aTilerLayer.signalData(o.Value.Encode);
//        Result := Result+1;
//      end;
//    end;
//  end;
//end;

function TLayer.SliceType: Integer;
begin
  Result := stGeometry; // default
end;

procedure TLayer.triggerLayerUpdate;
begin
  fLayerUpdateTimer.Arm(fLayerUpdateTimer.MaxPostponeDelta, handleLayerUpdateTrigger);
end;

function TLayer.uniqueObjectsTilesLink: string;
begin
  if Assigned(fTilerLayer)
  then Result := fTilerLayer.URLTimeStamped
  else Result := '';
end;

procedure TLayer.UpdateObjectAttribute(const aID: TWDID; const aAttribute, aValue: string);
var
  luo: TLayerUpdateObject;
begin
  TMonitor.Enter(fObjectsUpdated);
  try
    if not fObjectsUpdated.TryGetValue(aID, luo) then
    begin
      luo := TLayerUpdateObject.Create(aID);
      fObjectsUpdated.Add(aID, luo);
    end;
    luo.attributes.AddOrSetValue(aAttribute, aValue);
  finally
    TMonitor.Exit(fObjectsUpdated);
  end;
  triggerLayerUpdate;
end;

function TLayer.getJSON: string;
begin
  Result := inherited getJSON+','+
    '"objectTypes":['+objectTypes+'],'+
    '"tiles":"'+uniqueObjectsTilesLink+'",'+
    '"type":"'+layerType+'"';
  // todo: exception for geometry type Point should prob. always be tested
  if (not Assigned(fTilerLayer)) or ((objects.Count<=MaxDirectSendObjectCount) and (fGeometryType<>gtPoint))
  then Result := Result+',"objects": '+objectsJSON;
end;

function TLayer.getObjectsJSON: string;
var
  iop: TPair<TWDID, TLayerObjectWithID>;
begin
  Result := '';
  for iop in objects do
  begin
    if iop.Value.ValidGeometry
    then jsonAdd(Result, iop.Value.JSON2D[0, fGeometryType, fExtraJSON2DAttributes]);
  end;
  // todo:
  if fLayerType<>ltMarker //fGeometryType<>'Marker'
  then Result := geoJsonFeatureCollection(Result) // geojson formatted features
  else Result := '['+Result+']'; // straight through as array
end;

function TLayer.getPreviewBase64: string;
begin
  if Assigned(fTilerLayer)
  then Result := fTilerLayer.previewAsBASE64
  else Result := inherited;
end;

function TLayer.getRefJSON: string;
begin
  Result := '"id":"'+getElementID+'","tiles":"'+uniqueObjectsTilesLink+'"';
  if layerType<>''
  then Result := Result+',"type":"'+layerType+'"';
  if legendJSON<>''
  then Result := Result+',"legend":{'+legendJSON+'}';
  if (objects.Count<=MaxDirectSendObjectCount) and (fGeometryType<>gtPoint)
  then Result := Result+',"objects": '+objectsJSON;
end;

function TLayer.HandleClientSubscribe(aClient: TClient): Boolean;
var
  legendJSON: string;
begin
  Result := inherited;

  if Assigned(fTilerLayer) and (fTilerLayer.URL <> '') then
    aClient.SendRefresh(elementID, '', fTilerLayer.URLTimeStamped);

  legendJSON := Self.legendJSON;
  if legendJSON <> '' then
    aClient.SendLegend(elementID, '', Self.legendJSON);
  // handle subscribe on dependent diff layers
  {
  TMonitor.Enter(fDependentDiffLayers);
  try
    for diffLayer in fDependentDiffLayers
    do diffLayer.HandleClientSubscribe(aClient);
  finally
    TMonitor.Exit(fDependentDiffLayers);
  end;
  }
end;

function TLayer.HandleClientUnsubscribe(aClient: TClient): Boolean;
//var
  //diffLayer: TDiffLayer;
begin
  Result := inherited;
  // handle unsubscribe on dependent diff layers
  {
  TMonitor.Enter(fDependentDiffLayers);
  try
    for diffLayer in fDependentDiffLayers
    do diffLayer.HandleClientUnsubscribe(aClient);
  finally
    TMonitor.Exit(fDependentDiffLayers);
  end;
  }
end;

procedure TLayer.handleLayerUpdateTrigger(aTimer: TTimer; aTime: THighResTicks);
var
  o: TLayerObjectWithID;
  iluop: TPair<TWDID, TLayerUpdateObject>;
  anvp: TPair<string, string>;
  _json: string;
  ilop: TPair<TWDID, TLayerObjectWithID>;
begin
  _json := '';
  fObjectsLock.BeginRead;
  try
    if fLayerType=ltGeo then
    begin
      _json := '';
      for ilop in fObjects
      do jsonAdd(_json, ilop.Value.JSON2D[0, geometryType, fExtraJSON2DAttributes]);
      _json := '{"objects":{"features":['+_json+']}}';
      // clear fObjectsAdded etc.: not handled in case of ltGeo
      TMonitor.Enter(fObjectsAdded);
      try
        fObjectsAdded.Clear;
      finally
        TMonitor.Exit(fObjectsAdded);
      end;
      // process object updates
      TMonitor.Enter(fObjectsUpdated);
      try
        fObjectsUpdated.Clear;
      finally
        TMonitor.Exit(fObjectsUpdated);
      end;
      TMonitor.Enter(fObjectsDeleted);
      try
        fObjectsDeleted.Clear;
      finally
        TMonitor.Exit(fObjectsDeleted);
      end;
    end
    else
    begin
      // process add objects
      TMonitor.Enter(fObjectsAdded);
      try
        for iluop in fObjectsAdded do
        begin
          jsonAdd(_json, '{"newobject":{"id":"'+string(UTF8String(iluop.Key))+'"');
          for anvp in iluop.Value.attributes
          do _json := _json+',"'+anvp.Key+'":'+anvp.Value;
          _json := _json+'}}';
        end;
        fObjectsAdded.Clear;
      finally
        TMonitor.Exit(fObjectsAdded);
      end;
      // process object updates
      TMonitor.Enter(fObjectsUpdated);
      try
        for iluop in fObjectsUpdated do
        begin
          jsonAdd(_json, '{"updateobject":{"id":"'+string(UTF8String(iluop.Key))+'"');
          for anvp in iluop.Value.attributes
          do _json := _json+',"'+anvp.Key+'":'+anvp.Value;
          _json := _json+'}}';
        end;
        fObjectsUpdated.Clear;
      finally
        TMonitor.Exit(fObjectsUpdated);
      end;
      TMonitor.Enter(fObjectsDeleted);
      try
        for o in fObjectsDeleted
        do jsonAdd(_json, '{"removeobject":{"id":"'+string(UTF8String(o.ID))+'"}}');
        fObjectsDeleted.Clear;
      finally
        TMonitor.Exit(fObjectsDeleted);
      end;
      _json := '['+_json+']';
    end;
  finally
    fObjectsLock.EndRead;
  end;
  if _json<>'' then
  begin
    // too: check if using tiles? or over whole function?
    _json := '{"type":"updatelayer","payload":{"id":"'+ElementID+'","data":'+_json+'}}';
    forEachSubscriber<TClient>(procedure(aClient: TClient)
      begin
        aClient.signalString(_json);
      end);
  end;
end;

procedure TLayer.handleRefreshTrigger(aTimeStamp: TDateTime);
var
  timeStampStr: string;
  tiles: string;
begin
  try
    if aTimeStamp<>0
    then timeStampStr := FormatDateTime(publisherDateTimeFormat, aTimeStamp)
    else timeStampStr := '';
    tiles := uniqueObjectsTilesLink;

    Log.WriteLn('TLayer.handleRefreshTrigger for '+elementID+' ('+timeStampStr+'): '+tiles);

    // signal refresh to layer client
    forEachSubscriber<TClient>(
      procedure(aClient: TClient)
      begin
        Log.WriteLn('TLayer.handleRefreshTrigger for '+elementID+', direct subscribed client: '+aClient.fClientID, llNormal, 1);
        aClient.SendRefresh(elementID, timeStampStr, tiles);
      end);
    scenario.LayerRefreshed(tilerLayer.ID, elementID, aTimeStamp, Self);
  except
    on E: Exception
    do Log.WriteLn('Exception in TLayer.handleRefreshTrigger: '+e.Message, llError);
  end;
end;

procedure TLayer.handleTilerInfo(aTilerLayer: TTilerLayer);
var
  dl: TDiffLayer;
begin
  RegisterSlice;
  AddCommandToQueue(Self, Self.signalObjects);

  TMonitor.Enter(fDependentDiffLayers);
  try
    for dl in fDependentDiffLayers
    do dl.handleSubLayerInfo(Self);
  finally
    TMonitor.Exit(fDependentDiffLayers);
  end;
end;

procedure TLayer.handleTilerPreview(aTilerLayer: TTilerLayer);
var
  pvBASE64: string;
begin
  pvBASE64 := PreviewBase64;
  // layer clients
  forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      aClient.SendPreview(elementID, pvBASE64);
    end);
  // scenario clients
  scenario.forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      if not Subscribers.ContainsKey(aClient)
      then aClient.SendPreview(elementID, pvBASE64);
    end);
  Log.WriteLn('send normal preview on '+elementID);
end;

procedure TLayer.handleTilerRefresh(aTilerLayer: TTilerLayer; aTimeStamp: TDateTime; aImmediate: Boolean);
begin
  try
    if Assigned(fSendRefreshTimer) then
    begin
      if aImmediate  then
      begin
        fSendRefreshTimer.Arm(DateTimeDelta2HRT(dtOneSecond*ImmediateTilerRefreshTime),
          procedure(aTimer: TTimer; aTime: THighResTicks)
          begin
            handleRefreshTrigger(aTimeStamp);
          end);
      end
      else
      begin
        fSendRefreshTimer.Arm(DateTimeDelta2HRT(dtOneSecond*tilerRefreshTime),
          procedure(aTimer: TTimer; aTime: THighResTicks)
          begin
            handleRefreshTrigger(aTimeStamp);
          end);
      end;
    end
    else handleRefreshTrigger(aTimeStamp);
    // refresh preview also
    if Assigned(fPreviewRequestTimer)
    then fPreviewRequestTimer.DueTimeDelta := DateTimeDelta2HRT(dtOneSecond*tilerPreviewTime)
    else
    begin
      if assigned(aTilerLayer) then
      begin
        Log.WriteLn('triggered preview timer for '+elementID);
        aTilerLayer.signalRequestPreview;
      end
      else Log.WriteLn('tiler layer=nill for triggered preview timer for '+elementID, llWarning);
    end;
  except
    on E: Exception
    do Log.WriteLn('Exception in TLayer.handleTilerRefresh ('+self.elementID+'): '+E.Message, llError);
  end;
end;

{ KPI }

constructor TKPI.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string;
  aDefaultLoad: Boolean; const aDisplayGroup: string);
begin
  //
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, aDisplayGroup);
end;

destructor TKPI.Destroy;
begin
  inherited;
  //
end;

function TKPI.getJSON: string;
begin
  Result := inherited getJSON+','+
    '"title": "'+title+'", '+ // todo: same as name; remove?
    '"subtitle": "'+subtitle+'", '+
    '"ranges": ['+jsonArrayOfDoubleToStr(ranges)+'], '+
    '"measures": ['+jsonArrayOfDoubleToStr(measures)+'], '+
    '"markers": ['+jsonArrayOfDoubleToStr(markers)+']';
end;

procedure TKPI.Update;
var
  _json: string;
begin
  // send update to clients
  _json := '{"type":"updatekpi","payload":{'+JSON+'}}';
  forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      aClient.signalString(_json);
    end);
  // send also to clients on scenario for udpate of preview
  fScenario.forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      aClient.signalString(_json);
    end);
end;

{ TChartAxis }

constructor TChartAxis.Create(const aLabel, aColor, aQuantity, aUnit: string; aSet: Boolean = False; aMin: Double = NaN; aMax: Double = NaN);
begin
  inherited Create;
  fLabel  := aLabel;
  fColor := aColor;
  fQuantity := aQuantity;
  fUnit := aUnit;
  fSet := aSet;
  fMin := aMin;
  fMax := aMax;
end;

function TChartAxis.getJSON: string;
begin
  Result := '{"label":"'+fLabel+'","color":"'+fColor+
              '","qnt":"'+fQuantity+'","unit":"'+fUnit+
              '","set":'+IfThen(fSet, 1, 0).ToString+
              ',"min":"'+fMin.ToString+
              '","max":"'+fMax.ToString+'"}';
end;

{ TChartValue }

class function TChartValue.Create(aX: Double; const aY: TArray<Double>): TChartValue;
begin
  Result.x := aX;
  Result.y := aY;
end;

function TChartValue.getJSON: string;
var
  res: string;
  v: Double;
begin
  res := '';
  for v in y
  do jsonAdd(res, DoubleToJSON(v));
  Result := '{"x":'+DoubleToJSON(x)+',"y":['+res+']}';
end;

{ TChartLines }

{
procedure TChartLines.AddValue(aX: Double; const aY: TArray<Double>);
var
  v: TChartValue;
begin
  v := TChartValue.Create(aX, aY);
  TMonitor.Enter(fAllValues);
  try
    fAllValues.Add(v);
    fUpdateTimer.Arm(DateTimeDelta2HRT(dtOneSecond*chartUpdateTime),
      procedure (aTimer: TTimer; aTime: THighResTicks)
        begin
          RecalculateChartValues;
        end);
  finally
    TMonitor.Exit(fAllValues);
  end;
end;
}
procedure TChartLines.AddValue(aX: Double; const aY: TArray<Double>);
var
  v: TChartValue;
//  i: Integer;
  Comparison: TComparison<TChartValue>;
begin
  v := TChartValue.Create(aX, aY);
  TMonitor.Enter(fAllValues);
  try
    {
    if (fAllValues.Count>0) and (aX<=fAllValues.Last.x) then
    begin
      i := fAllValues.Count-1;
      while (i>0) and (aX<fAllValues[i-1].x)
      do i := i-1;
      if fAllValues[i].x=aX
      then fAllValues[i] := v // replace
      else fAllValues.Insert(i, v); // insert at correct position
    end
    else fAllValues.Add(v);
    }
    fAllValues.Add(v);

    Comparison :=
      function(const Left, Right: TChartValue): Integer
      begin
        if Left.x=Right.x
        then Result := 0
        else if Left.x>Right.x
        then Result := 1
        else Result := -1;
      end;

    fAllValues.Sort(TComparer<TChartValue>.Construct(Comparison));
    fUpdateTimer.Arm(DateTimeDelta2HRT(dtOneSecond*chartUpdateTime),
      procedure (aTimer: TTimer; aTime: THighResTicks)
        begin
          RecalculateChartValues;
        end);
  finally
    TMonitor.Exit(fAllValues);
  end;
end;

constructor TChartLines.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aChartType: string;
  aXAxis: TChartAxis; aYAxes: TArray<TChartAxis>; const aXScale: string; aChartMaxUpdateTime: Integer; aShowAvgLine: Boolean; aYMax, aYMin: Double);
var
  axis: TChartAxis;
begin
  fYMin := aYMin;
  fYMax := aYMax;
  fShowAvgLine := aShowAvgLine;
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, aChartType);
  if Assigned(aXAxis)
  then fXAxis := aXAxis
  else fXAxis := TChartAxis.Create('', '', '', '');
  fYAxes := TObjectList<TChartAxis>.Create(True);
  for axis in aYAxes
  do fYAxes.Add(axis);
  fXScale := aXScale;
  fValues := TChartValues.Create;
  fAllValues := TChartValues.Create;
  //fUpdateValues := TChartValues.Create;
  fChartUpdateTime := DefaultChartUpdateTime;
  fUpdateTimer := fScenario.Project.Timers.CreateInactiveTimer;
  fUpdateTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond * aChartMaxUpdateTime);
end;

destructor TChartLines.Destroy;
begin
  if Assigned(fUpdateTimer) then
  begin
    fUpdateTimer.Cancel;
    fUpdateTimer := nil;
  end;
  inherited;
  FreeAndNil(fXAxis);
  FreeAndNil(fYAxes);
  FreeAndNil(fValues);
end;

function TChartLines.getJSON: string;
var
  axis: TChartAxis;
  res: string;
begin
  Result := inherited getJSON;
  jsonAdd(Result, '"x":'+fXAxis.toJSON);
  res := '';
  for axis in fYAxes
  do jsonAdd(res, axis.toJSON);
  Result := Result+',"y":['+res+']';
  Result := Result+',"xScale":"' + xScale + '"';
  Result := Result+',"maxPoints":' + chartMaxPoints.ToString;
  Result := Result+',"showAvgLine":'+Ord(fShowAvgLine).toString;
  if not fYMin.IsNan
  then Result := Result+',"graphAxisMinY":'+fYMin.toString(dotFormat);
  if not fYMax.IsNan
  then Result := Result+',"graphAxisMaxY":'+fYMax.toString(dotFormat);
end;

function TChartLines.getJSONData: string;
var
  v: TChartValue;
begin
  Result := '';
  for v in values
  do jsonAdd(Result, v.toJSON);
end;

//function TChartLines.getChartMaxUpdateTime: Integer;
//begin
//  Result := Round(HRT2DateTime(fUpdateTimer.MaxPostponeDelta)/dtOneSecond);
//end;

procedure TChartLines.RecalculateChartValues;
var
//  step: Double;
//  current: Double;
  i,iy: Integer;
  updateString: string;
  chartValue: TChartValue;
  _elementID: string;
  _updateString: string;
  _scenario: TScenario;
  minValues: TChartValue;
  maxValues: TChartValue;
  delta: Double;
  peaks: TList<Integer>;
begin
  TMonitor.Enter(fAllValues);
  try
    begin
      fValues.Clear;
      if fAllValues.Count <= chartMaxPoints then
      begin
        for i := 0 to fAllValues.Count - 1
        do fValues.Add(fAllValues[i]);
      end
      else
      begin
        minValues := fAllValues[0];
        maxValues := minValues;
        for i := 1 to fAllValues.Count - 1 do
        begin
          for iy := 0 to length(fAllValues[i].y)-1 do
          begin
            if minValues.y[iy]>fAllValues[i].y[iy]
            then minValues.y[iy] := fAllValues[i].y[iy];
            if maxValues.y[iy]<fAllValues[i].y[iy]
            then maxValues.y[iy] := fAllValues[i].y[iy];
          end;
        end;
        delta := maxValues.y[0]-minValues.y[0];
        for iy := 1 to length(minValues.y)-1 do
        begin
          if delta<maxValues.y[iy]-minValues.y[iy]
          then delta := maxValues.y[iy]-minValues.y[iy];
        end;
        peaks := TList<Integer>.Create;
        try
          // use 5 percent of abs delta
          DetectPeaks(fAllValues, 0.05*delta, peaks);
          for i := 0 to peaks.Count-1
          do fValues.Add(fAllValues[peaks[i]]);
        finally
          peaks.Free;
        end;
        // todo: exchange with peak detect?
        {
        step := fAllValues.Count / chartMaxPoints;
        current := 0;
        while current < (fAllValues.Count - 1) do
        begin
          fValues.Add(fAllValues[System.Math.Floor(current)]);
          current := current + step;
        end;
        }
      end;
      updateString := '';
        for chartValue in fValues do
        begin
          if (updateString <> '') then
            updateString := updateString + ',';
          updateString := updateString + chartValue.ToJSON;
        end;
    end;
  finally
    TMonitor.Exit(fAllValues);
  end;
  forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      // todo: can be for more then 1 chart!
      aClient.signalString('{"type":"resetgraphs","payload":["'+elementID+'"]}');
      aClient.signalString('{"type":"updatechart","payload":[{"id":"'+elementID+'","data":['+updateString+']}]}');
    end);
  // store locally for lambda call below
  _elementID := elementID;
  _updateString := updateString;
  _scenario := scenario;
  // chart preview on lower frequency
  fPreviewRequestTimer.Arm(DateTimeDelta2HRT(dtOneSecond*DefaultChartPreviewTime),
    procedure(aTimer: TTimer; aTime: THighResTicks)
    begin
      _scenario.forEachSubscriber<TClient>(
        procedure(aClient: TClient)
        begin
          // todo: can be for more then 1 chart!
          aClient.signalString('{"type":"resetgraphs","payload":["'+_elementID+'"]}');
          aClient.signalString('{"type":"updatechart","payload":[{"id":"'+_elementID+'","data":['+_updateString+']}]}');
        end);
    end);
end;

procedure TChartLines.reset;
begin
  TMonitor.Enter(fAllValues);
  try
    begin
      fValues.Clear;
      fAllValues.Clear;
    end;
  finally
    TMonitor.Exit(fAllValues);
  end;
  forEachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
      // todo: can be for more then 1 chart!
      aClient.signalString('{"type":"resetgraphs","payload":["'+elementID+'"]}');
    end);
end;

//procedure TChartLines.setChartMaxUpdateTime(const aValue: Integer);
//begin
//  if chartMaxUpdateTime <> aValue then
//  begin
//    fUpdateTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond * aValue);
//  end;
//end;

{ TTableChart }

function TTableChart.getJSONData: string;
begin
  Result := '["header 1", "header 2"],["row 1", 1.1],["row 2", 2.2]';
end;

{ TScenario }

function TScenario.AddChart(aChart: TChart): TChart;
begin
  TMonitor.Enter(fCharts);
  try
    fCharts.AddOrSetValue(aChart.ID, aChart);
  finally
    TMonitor.Exit(fCharts);
  end;
  Result := aChart;
end;

function TScenario.AddKPI(aKPI: TKPI): TKPI;
begin
  TMonitor.Enter(fKPIs);
  try
    fKPIs.AddOrSetValue(aKPI.ID, aKPI);
  finally
    TMonitor.Exit(fKPIs);
  end;
  Result := aKPI;
end;

function TScenario.AddLayer(aLayer: TLayerBase): TLayerBase;
begin
  TMonitor.Enter(fLayers);
  try
    fLayers.AddOrSetValue(aLayer.ID, aLayer);
  finally
    TMonitor.Exit(fLayers);
  end;
  Result := aLayer;
end;

procedure TScenario.EnableControl(const aControlName: string);
begin
  SetControl(aControlName, controlEnabled);
end;

procedure TScenario.DisableControl(const aControlName: string);
begin
  SetControl(aControlName, controlDisabled);
end;

procedure TScenario.SetControl(const aControlName, aControlValue: string); //sets control with custom JSON
begin
  TMonitor.Enter(fControls);
  try
    fControls.AddOrSetValue(aControlName, aControlValue);
  finally
    TMonitor.Exit(fControls);
  end;
  foreachSubscriber<TClient>(
    procedure(aClient: TClient)
    begin
     aClient.signalControl(aControlName);
    end);
end;

function TScenario.splitLayersOnCategories(
  const aSelectCategories: TArray<string>; aInCategories,
  aNotInCategories: TList<TLayerBase>): Boolean;
var
  ilp: TPair<string, TLayerBase>;
  cat: string;
  found: Boolean;
begin
  Result := False;
  if length(aSelectCategories)>0  then
  begin
    // select basic layers on categories
    for ilp in fLayers do
    begin
      found := False;
      for cat in aSelectCategories do
      begin
        if ilp.Key.ToUpper=cat.ToUpper then
        begin
          aInCategories.Add(ilp.Value);
          found := True;
          Result := True;
        end;
      end;
      if not found and ilp.Value.basicLayer then
      begin
        aNotInCategories.Add(ilp.Value);
        Result := True;
      end;
    end;
  end
  else
  begin
    // all basic layers
    for ilp in fLayers do
    begin
      if ilp.Value.basicLayer then
      begin
        aNotInCategories.Add(ilp.Value);
        Result := True;
      end;
    end;
  end;
end;

{
function TScenario.GetControl(const aControlName: string) : string;
begin
  TMonitor.Enter(fControls);
  try
    if fControls.ContainsKey(aControlName) then
      Result := fControls[aControlName]
    else
      Result := fProject.GetControl(aControlName);
  finally
    TMonitor.Exit(fControls);
  end;
end;

function TScenario.GetControlEnabled(const aControlName: string) : Boolean;
begin
  Result := GetControl(aControlName) <> controlDisabled;
end;

function TScenario.getControlsJSON: string;
var
  key: string;
begin
  TMonitor.Enter(fControls);
  try
    Result := '';
    for key in fControls.Keys do
    begin
      if Result <> '' then
        Result := Result + ',';
      Result := Result + '"' + key + '":' + fControls[key];
    end;
    TMonitor.Enter(fProject.controls);
    try
      for key in fProject.controls.Keys do
        if not fControls.ContainsKey(key) then
        begin
          if Result <> '' then
            Result := Result + ',';
          Result := Result + '"' + key + '":' + fProject.controls[key];
        end;
    finally
      TMonitor.Exit(fProject.controls);
    end;
  finally
    TMonitor.Exit(fControls);
  end;
end;
}
constructor TScenario.Create(aProject: TProject; const aID, aName, aDescription: string; aAddbasicLayers: Boolean; aMapView: TMapView);
begin
  inherited Create;
  fControls := TDictionary<string, string>.Create;
  fProject := aProject;
  fID := aID;
  fName := aName;
  fDescription := aDescription;
  fMapView := aMapView;
  fLayers := TObjectDictionary<string,TLayerBase>.Create([doOwnsValues]);
  fKPIs := TObjectDictionary<string, TKPI>.Create([doOwnsValues]);
  fCharts := TObjectDictionary<string, TChart>.Create([doOwnsValues]);
  fAddbasicLayers := aAddbasicLayers;
  //fSelectionLayer := TLayer.Create(Self, 'Test', 'selection2', 'Selection', 'Selection', True, '"test"', 'LineString', 'tile', True, 0, False, 0, 'default', '', '', TDiscretePalette.Create('basic palette', [], TGeoColors.Create($F06EAA or $FF000000)));
  //fSelectionLayer.RegisterOnTiler(False, stGeometryI, fName + '-selectionLayer', -NaN, fSelectionLayer.palette.Clone);
  //fSelectionLayer.fTilerLayer.signalSliceAction(tsaClearSlice);
  //fSelectionLayer.fTilerLayer.signalAddSlice(fSelectionLayer.palette.Clone);
  //fLayers.Add('selection', fSelectionLayer);
  ReadBasicData;
end;

destructor TScenario.Destroy;
begin
  inherited;
  FreeAndNil(fLayers);
  FreeAndNil(fKPIs);
  FreeAndNil(fCharts);
  FreeAndNil(fControls);
end;

function TScenario.getControl(const aControlName: string): string;
var
  found: Boolean;
begin
  TMonitor.Enter(fControls);
  try
    found := fControls.TryGetValue(aControlName, Result);
  finally
    TMonitor.Exit(fControls);
  end;
  if not found then
  begin
    if Assigned(fProject)
    then Result := fProject.Control[aControlName]
    else Result := '';
  end;
end;

function TScenario.getElementID: string;
begin
  Result := ID;
  if Assigned(fProject)
  then Result := fProject.fProjectID+sepElementID+Result;
end;

procedure TScenario.HandleClientGraphLabelClick(aClient: TClient; aPayload: TJSONValue);
var
  chart: TChart;
  graphID, labelTitle: string;
  found: Boolean;
begin
  if aPayload.TryGetValue<string>('graphID', graphID) and
    aPayload.TryGetValue<string>('labelTitle', labelTitle) then
  begin
    chart := nil;

    TMonitor.Enter(fCharts);
    try
      found := fCharts.TryGetValue(graphID, chart);
    finally
      TMonitor.Exit(fCharts);
    end;

    if found then
    begin
      chart.HandleGraphLabelClick(aClient, labelTitle);
    end;
  end;
end;

function TScenario.HandleClientSubscribe(aClient: TClient): Boolean;
var
  scenarioControlNames: TArray<string>;
  controlName: string;
begin
  Result := inherited;
  //signal all scenario specific controls
  TMonitor.Enter(fControls);
  try
    scenarioControlNames := fControls.Keys.ToArray;
  finally
    TMonitor.Exit(fControls);
  end;
  for controlName in scenarioControlNames
  do aClient.signalControl(controlname);
end;

function TScenario.HandleClientUnsubscribe(aClient: TClient): Boolean;
var
  scenarioControlNames: TArray<string>;
  controlName: string;
begin
  Result := inherited;
  //set all scenario specific controls back to the project values
  TMonitor.Enter(fControls);
  try
    scenarioControlNames := fControls.Keys.ToArray;
  finally
    TMonitor.Exit(fControls);
  end;
  for controlName in scenarioControlNames
  do aClient.signalControl(controlName);
end;

procedure TScenario.HandleNewMapView(aClient: TClient; aMapView: TMapView);
begin
  forEachSubscriber<TClient>(
    procedure(aScenarioClient: TClient)
    begin
      if (aScenarioClient<>aClient) and aScenarioClient.fFollow then
      begin
        aScenarioClient.signalString('{"type":"session","payload":{"mapView":{"view":{'+
          '"lat":'+aMapView.lat.ToString(dotFormat)+','+
          '"lon":'+aMapView.lon.ToString(dotFormat)+','+
          '"zoom":'+aMapView.zoom.ToString(dotFormat)+'}}}}');
      end;
    end);
end;

procedure TScenario.LayerRefreshed(aTilerLayerID: Integer; const aElementID: string; aTimeStamp: TDateTime; aLayer: TSubscribeObject);
begin
  // called when tiler sends refresh signal for layer
  // default no action
end;

procedure TScenario.ReadBasicData;
begin
  // default no action
end;

procedure TScenario.registerLayers;
var
  layer: TLayerBase;
begin
  for layer in fLayers.Values
  do layer.RegisterLayer;
end;

function TScenario.selectLayersOnCategories(const aSelectCategories: TArray<string>; aLayers: TList<TLayerBase>): Boolean;
var
  ilp: TPair<string, TLayerBase>;
  cat: string;
begin
  Result := False;
  if length(aSelectCategories)>0  then
  begin
    // select basic layers on categories
    for ilp in fLayers do
    begin
      for cat in aSelectCategories do
      begin
        if ilp.Key.ToUpper=cat.ToUpper then
        begin
          aLayers.Add(ilp.Value);
          Result := True;
        end;
      end;
    end;
  end
  else
  begin
    // all basic layers
    for ilp in fLayers do
    begin
      if ilp.Value.basicLayer then
      begin
        aLayers.Add(ilp.Value);
        Result := True;
      end;
    end;
  end;
end;

function TScenario.SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories, aSelectedIDs: TArray<string>): string;
begin
  Result := '';
end;

function TScenario.SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories, aPrefCategories: TArray<string>; aGeometry: TWDGeometry): string;
var
  layers, prefLayers: TList<TLayerBase>;
  categories: string;
  objectsGeoJSON, objectsIDs: string;
  totalObjectCount: Integer;
  extent: TWDExtent;
  l: TLayerBase;
  objectCount: Integer;
begin
  Result := '';
  //fSelectionLayer.fTilerLayer.signalSliceAction(); //clear layer
  layers := TList<TLayerBase>.Create;
  try
    if Length(aSelectCategories) > 0 then
    begin
      if selectLayersOnCategories(aSelectCategories, layers) then
      begin
        categories := '';
        objectsGeoJSON := '';
        objectsIDs := '';
        totalObjectCount := 0;
        extent := TWDExtent.FromGeometry(aGeometry);
        for l in layers do
        begin
          if l is TLayer then
          begin
            objectCount := (l as TLayer).findObjectsInGeometry(extent, aGeometry, objectsGeoJSON, objectsIDs);
            //objectCount := (l as TLayer).signalObjectsInGeometry(extent, aGeometry, fSelectionLayer.fTilerLayer);
            if objectCount>0 then
            begin
              if categories=''
              then categories := '"'+l.id+'"'
              else categories := categories+',"'+l.id+'"';
              totalObjectCount := totalObjectCount+objectCount;
            end;
          end;
        end;
        if totalObjectCount > MaxLayerObjects then
        begin
          objectsGeoJSON := '';
          aClient.SendMessage('Selected too many objects to display', mtWarning);
        end;
        Result :=
          '{"type":"selectedObjects","payload":{"selectCategories":['+categories+'],'+
           '"mode":"'+aMode+'",'+
           '"ids":['+objectsIDs+'],'+
           '"objects":['+objectsGeoJSON+']}}';
        Log.WriteLn('select on geometry:  found '+totalObjectCount.ToString+' objects in '+categories);
      end;
    end
    else
    begin
      prefLayers := TList<TLayerBase>.Create;
      try
        if splitLayersOnCategories(aPrefCategories, prefLayers, layers) then
        begin
          categories := '';
          objectsGeoJSON := '';
          totalObjectCount := 0;
          extent := TWDExtent.FromGeometry(aGeometry);
          for l in prefLayers do
          begin
            if l is TLayer then
            begin
              objectCount := (l as TLayer).findObjectsInGeometry(extent, aGeometry, objectsGeoJSON, objectsIDs);
              //objectCount := (l as TLayer).signalObjectsInGeometry(extent, aGeometry, fSelectionLayer.fTilerLayer);
              if objectCount>0 then
              begin
                if categories=''
                then categories := '"'+l.id+'"'
                else categories := categories+',"'+l.id+'"';
                totalObjectCount := totalObjectCount+objectCount;
              end;
            end;
          end;
          if totalObjectCount = 0 then
          begin
            for l in layers do
            begin
              if l is TLayer then
              begin
                objectCount := (l as TLayer).findObjectsInGeometry(extent, aGeometry, objectsGeoJSON, objectsIDs);
                //objectCount := (l as TLayer).signalObjectsInGeometry(extent, aGeometry, fSelectionLayer.fTilerLayer);
                if objectCount>0 then
                begin
                  if categories=''
                  then categories := '"'+l.id+'"'
                  else categories := categories+',"'+l.id+'"';
                  totalObjectCount := totalObjectCount+objectCount;
                end;
              end;
            end;
          end;
          if totalObjectCount > MaxLayerObjects then
          begin
            objectsGeoJSON := '';
            aClient.SendMessage('Selected too many objects to display', mtWarning);
          end;
          Result :=
            '{"type":"selectedObjects","payload":{"selectCategories":['+categories+'],'+
            '"mode":"'+aMode+'",'+
            '"ids":['+objectsIDs+'],'+
            '"objects":['+objectsGeoJSON+']}}';
          Log.WriteLn('select on geometry:  found '+totalObjectCount.ToString+' objects in '+categories);
        end;
      finally
        prefLayers.Free;
      end;
    end;
//    if selectLayersOnCategories(aSelectCategories, layers) then
//    begin
//      categories := '';
//      objectsGeoJSON := '';
//      totalObjectCount := 0;
//      extent := TWDExtent.FromGeometry(aGeometry);
//      for l in layers do
//      begin
//        if l is TLayer then
//        begin
//          objectCount := (l as TLayer).findObjectsInGeometry(extent, aGeometry, objectsGeoJSON);
//          if objectCount>0 then
//          begin
//            if categories=''
//            then categories := '"'+l.id+'"'
//            else categories := categories+',"'+l.id+'"';
//            totalObjectCount := totalObjectCount+objectCount;
//          end;
//        end;
//      end;
//      Result :=
//        '{"selectedObjects":{"selectCategories":['+categories+'],'+
//         '"mode":"'+aMode+'",'+
//         '"objects":['+objectsGeoJSON+']}}';
//      Log.WriteLn('select on geometry:  found '+totalObjectCount.ToString+' objects in '+categories);
//    end;
    // todo: else warning?
  finally
    layers.Free;
  end;
end;

function TScenario.SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories, aPrefCategories: TArray<string>; aX, aY, aRadius, aZoom: Double): string;
var
  prefLayers, layers: TList<TLayerBase>;
  dist: TDistanceLatLon;
  nearestObject: TLayerObjectWithID;
  nearestObjectLayer: TLayer;
  nearestObjectDistanceInMeters: Double;
  objectSide, curSide: Integer;
  maxDist, maxPrefDist: Double;
  l: TLayerBase;
  o: TLayerObjectWithID;
  categories: string;
  objectsGeoJSON: string;
  totalObjectCount: Integer;
  objectCount: Integer;
begin
  Result := '';
  maxPrefDist := PixelsToMeters(MaxPreferredSelectPixelDist, aY, aZoom);
  maxDist := PixelsToMeters(MaxSelectPixelDist, aY, aZoom);
  layers := TList<TLayerBase>.Create;
  try
    if length(aSelectCategories)>0 then //already a selected object -> force select in same layer(s?)
    begin
      if selectLayersOnCategories(aSelectCategories, layers) then
      begin
        dist := TDistanceLatLon.Create(aY, aY);
        if (aRadius=0) then
        begin
          nearestObject := nil;
          nearestObjectLayer := nil;
          nearestObjectDistanceInMeters := Double.PositiveInfinity;
          for l in layers do
          begin
            if l is TLayer then
            begin
              o := (l as TLayer).findNearestObject(dist, aX, aY, nearestObjectDistanceInMeters, curSide);
              if assigned(o) and (nearestObjectDistanceInMeters<maxDist) then
              begin
                nearestObjectLayer := l as TLayer;
                nearestObject := o;
                objectSide := curSide;
                if nearestObjectDistanceInMeters=0
                then break;
              end;
            end;
          end;
          if Assigned(nearestObject) then
          begin
            // todo:
            Result :=
              '{"type":"selectedObjects","payload":{"selectCategories":["'+nearestObjectLayer.ID+'"],'+
               '"mode":"'+aMode+'",'+
               '"objects":['+nearestObject.JSON2D[objectSide, nearestObjectLayer.geometryType, '']+']}}';
            Log.WriteLn('found nearest object layer: '+nearestObjectLayer.ID+', object: '+string(nearestObject.ID)+', distance: '+nearestObjectDistanceInMeters.toString+', side: ' + objectSide.ToString);
          end
          else
          begin
            // todo:
            Result :=
              '{"type":"selectedObjects","payload":{"selectCategories":[],'+
               '"mode":"'+aMode+'",'+
               '"objects":[]}}';
            Log.WriteLn('found no nearest object within distance: '+nearestObjectDistanceInMeters.toString+' > '+maxDist.ToString);
            aClient.SendMessage('found no nearest object within distance: '+nearestObjectDistanceInMeters.toString+' > '+Ceil(maxDist).ToString, mtWarning);
          end;
        end
        else
        begin
          categories := '';
          objectsGeoJSON := '';
          totalObjectCount := 0;
          for l in layers do
          begin
            if l is TLayer then
            begin
              objectCount := (l as TLayer).findObjectsInCircle(dist, aX, aY, aRadius, objectsGeoJSON);
              if objectCount>0 then
              begin
                if categories=''
                then categories := '"'+l.id+'"'
                else categories := categories+',"'+l.id+'"';
                totalObjectCount := totalObjectCount+objectCount;
              end;
            end;
          end;
          Result :=
            '{"type":"selectedObjects","payload":{"selectCategories":['+categories+'],'+
             '"mode":"'+aMode+'",'+
             '"objects":['+objectsGeoJSON+']}}';
          Log.WriteLn('select on radius:  found '+totalObjectCount.ToString+' objects in '+categories);
        end;
      end;
    end
    else
    begin
      prefLayers := TList<TLayerBase>.Create;
      try
        if splitLayersOnCategories(aPrefCategories, prefLayers, layers) then
        begin
          dist := TDistanceLatLon.Create(aY, aY);
          if (aRadius=0) then
          begin
            nearestObject := nil;
            nearestObjectLayer := nil;
            nearestObjectDistanceInMeters := Double.PositiveInfinity;
            for l in prefLayers do
            begin
              if l is TLayer then
              begin
                o := (l as TLayer).findNearestObject(dist, aX, aY, nearestObjectDistanceInMeters, curSide);
                if assigned(o) and (nearestObjectDistanceInMeters<maxDist) then
                begin
                  nearestObjectLayer := l as TLayer;
                  nearestObject := o;
                  objectSide := curSide;
                  if nearestObjectDistanceInMeters=0
                  then break;
                end;
              end;
            end;
            if nearestObjectDistanceInMeters > maxPrefDist then
            begin
              for l in layers do
              begin
                if l is TLayer then
                begin
                  o := (l as TLayer).findNearestObject(dist, aX, aY, nearestObjectDistanceInMeters, curSide);
                  if assigned(o) and (nearestObjectDistanceInMeters<maxDist) then
                  begin
                    nearestObjectLayer := l as TLayer;
                    nearestObject := o;
                    objectSide := curSide;
                    if nearestObjectDistanceInMeters=0
                    then break;
                  end;
                end;
              end;
            end;
            if Assigned(nearestObject) then
            begin
              // todo:
              Result :=
                '{"type":"selectedObjects","payload":{"selectCategories":["'+nearestObjectLayer.ID+'"],'+
                 '"mode":"'+aMode+'",'+
                 '"objects":['+nearestObject.JSON2D[objectSide, nearestObjectLayer.geometryType, '']+']}}';
              Log.WriteLn('found nearest object layer: '+nearestObjectLayer.ID+', object: '+string(nearestObject.ID)+', distance: '+nearestObjectDistanceInMeters.toString + ', side: ' + objectSide.ToString);
            end
            else
            begin
              // todo:
              Result :=
                '{"type":"selectedObjects","payload":{"selectCategories":[],'+
                 '"mode":"'+aMode+'",'+
                 '"objects":[]}}';
              Log.WriteLn('found no nearest object within distance: '+nearestObjectDistanceInMeters.toString+' > '+maxDist.ToString);
              aClient.SendMessage('found no nearest object within distance: '+nearestObjectDistanceInMeters.toString+' > '+Ceil(maxDist).ToString, mtWarning);
            end;
          end
          else
          begin
            categories := '';
            objectsGeoJSON := '';
            totalObjectCount := 0;
            for l in prefLayers do
            begin
              if l is TLayer then
              begin
                objectCount := (l as TLayer).findObjectsInCircle(dist, aX, aY, aRadius, objectsGeoJSON);
                if objectCount>0 then
                begin
                  if categories=''
                  then categories := '"'+l.id+'"'
                  else categories := categories+',"'+l.id+'"';
                  totalObjectCount := totalObjectCount+objectCount;
                end;
              end;
            end;
            if totalObjectCount = 0 then
            begin
              for l in layers do
              begin
                if l is TLayer then
                begin
                  objectCount := (l as TLayer).findObjectsInCircle(dist, aX, aY, aRadius, objectsGeoJSON);
                  if objectCount>0 then
                  begin
                    if categories=''
                    then categories := '"'+l.id+'"'
                    else categories := categories+',"'+l.id+'"';
                    totalObjectCount := totalObjectCount+objectCount;
                  end;
                end;
              end;
            end;
            Result :=
              '{"type":"selectedObjects","payload":{"selectCategories":['+categories+'],'+
               '"mode":"'+aMode+'",'+
               '"objects":['+objectsGeoJSON+']}}';
            Log.WriteLn('select on radius:  found '+totalObjectCount.ToString+' objects in '+categories);
          end;
        end;
      finally
        prefLayers.Free;
      end;
    end;
    // todo: else warning?
  finally
    layers.Free;
  end;
end;

function TScenario.SelectObjects(aClient: TClient; const aType, aMode: string; const aSelectCategories: TArray<string>; aJSONQuery: TJSONArray): string;
begin
  Result := '';
end;

function TScenario.selectObjectsProperties(aClient: TClient; const aSelectCategories, aSelectedObjects: TArray<string>): string;
begin
  Result := '';
end;

{ TScenarioLink }

procedure TScenarioLink.buildHierarchy;
// put all direct children in hierarchy
var
  i: Integer;
  child: TScenarioLink;
  _parent: TScenarioLink;
begin
  for i := fChildren.Count-1 downto 0 do
  begin
    child := fChildren[i];
    _parent := root.findScenario(child.parentID);
    if Assigned(_parent) and (_parent<>child) and (child.parent<>_parent) then
    begin
      fChildren.Extract(child);
      _parent.children.Insert(0, child);
      child.parent := _parent;
    end;
  end;
end;

constructor TScenarioLink.Create(const aID, aParentID, aReferenceID: string; const aName, aDescription, aStatus: string; aLink: TScenario);
begin
  inherited Create;
  fChildren := TObjectList<TScenarioLink>.Create;
  fLink := aLink;
  fParent := nil;
  //
  fID := aID;
  fParentID := aParentID;
  fReferenceID := aReferenceID;
  fName := aName;
  fDescription := aDescription;
  fStatus := aStatus;
end;

destructor TScenarioLink.Destroy;
begin
  FreeAndNil(fChildren);
  fLink := nil;
  inherited;
end;

function TScenarioLink.findScenario(const aID: string): TScenarioLink;
var
  i: Integer;
begin
  Result := nil;
  i := fChildren.Count-1;
  while (i>=0) and not Assigned(Result) do
  begin
    if fChildren[i].ID<>aID then
    begin
      Result := fChildren[i].findScenario(aID);
      i := i-1;
    end
    else Result := fChildren[i];
  end;
end;

function TScenarioLink.JSON: string;
var
  child: TScenarioLink;
begin
  Result := '';
  for child in fChildren do
  begin
    jsonAdd(Result,
      '{"id":"'+child.ID+'",'+
       '"name":"'+child.name+'",'+
       '"description":"'+child.description+'",'+
       '"status":"'+child.status+'",'+
       '"reference":"'+child.referenceID+'",'+
       '"children":'+child.JSON+'}');
  end;
  Result := '['+Result+']';
end;

function TScenarioLink.name: string;
begin
  if Assigned(Self)
  then Result := fName
  else Result := '';
end;

function TScenarioLink.removeLeaf(const aStatus: string): Boolean;
var
  i: Integer;
begin
  for i := fChildren.Count-1 downto 0 do
  begin
    if fChildren[i].removeLeaf(aStatus)
    then fChildren.Delete(i);
  end;
  Result := (fChildren.Count=0) and (fStatus=aStatus);
end;

function TScenarioLink.root: TScenarioLink;
begin
  Result := Self;
  while Assigned(Result.fParent)
  do Result := Result.fParent;
end;

function compareScenarioLinks(const aScenarioLink1, aScenarioLink2: TScenarioLink): Integer;
begin
  Result := AnsiCompareText(aScenarioLink1.description, aScenarioLink2.description);
end;

procedure TScenarioLink.sortChildren();
begin
  children.Sort(TComparer<TScenarioLink>.Construct(compareScenarioLinks));
end;

{ TMeasure }

constructor TMeasureAction.Create(const aID, aDescription, aObjectTypes, aAction, aActionParameters: string;
    aActionID: Integer);
begin
  inherited Create;
  fID := aID;
  fDescription := aDescription;
  fObjectTypes := aObjectTypes;
  fAction := aAction;
  if aActionParameters=''
  then fActionParameters := 'null'
  else fActionParameters := aActionParameters;
  fActionID := aActionID;
end;

procedure TMeasure.AddAction(const aID, aDescription, aObjectTypes, aAction,
  aActionParameters: string; aActionID: Integer);
begin
  TMonitor.Enter(fActions);
  try
    fActions.AddOrSetValue(aID, TMeasureAction.Create(aID, aDescription, aObjectTypes, aAction, aActionParameters, aActionID));
  finally
    TMonitor.Exit(fActions);
  end;
end;

constructor TMeasure.Create(const aID, aMeasure, aDescription,
  aObjectTypes, aAction, aActionParameters: string; aActionID: Integer);
begin
  fMeasure := aMeasure;
  fActions := TObjectDictionary<string, TMeasureAction>.Create([doOwnsValues]);
  AddAction(aID, aDescription, aObjectTypes, aAction, aActionParameters, aActionID);
end;

destructor TMeasure.Destroy;
begin
  inherited;
  FreeAndNil(fActions);
end;

function TMeasure.FindMeasure(const aActionID: string;
  out aMeasure: TMeasureAction): Boolean;
begin
  Result := fActions.TryGetValue(aActionID, aMeasure);
end;

function TMeasure.getMeasureJSON: string;
var
  action: TMeasureAction;
begin
  Result := '';
  for action in fActions.Values do
  begin
    if Result <> '' then
      Result := Result + ',';
    Result := Result + action.getActionJSON;
  end;
  Result := '{"actions":[' + Result + '], "measure":"' + fMeasure + '"}';
end;

procedure TMeasure.RemoveAction(const aID: string);
begin
  //
end;

{ WindControl }

constructor TWindControl.Create(aProject: TProject; aWindDirection, aWindSpeed: double; aLive: Boolean);
begin
  inherited Create;
  fProject := aProject;
  fWindDirection := aWindDirection;
  fWindSpeed := aWindSpeed;
  fLive := aLive;
end;

procedure TWindControl.setLive(const aValue: Boolean);
var
  jsonLive: string;
begin
  fLive := aValue;
  if fLive
  then jsonLive := '"live":true'
  else jsonLive := '"live":false';
  fProject.forEachClient(
    procedure(aClient: TClient)
    begin
      aClient.signalString(
        '{'+
          '"type":"winddata",'+
          '"payload":{'+jsonLive+'}'+
        '}');
    end);
end;

procedure TWindControl.setWindDirection(const aValue: double);
begin
  fWindDirection := aValue;
  update(fWindDirection, fWindSpeed);
end;

procedure TWindControl.setWindSpeed(const aValue: double);
begin
  fWindSpeed := aValue;
  update(fWindDirection, fWindSpeed);
end;

procedure TWindControl.update(aWindDirection, aWindSpeed: double; aLive: Integer; aSendingClient: TClient);
begin
  fWindDirection := aWindDirection;
  fWindSpeed := aWindSpeed;
  if aLive>=0
  then fLive := aLive<>0;
  fProject.forEachClient(procedure(aClient: TClient)
    begin
//      if aClient<>aSendingClient
//      then
      updateClient(aWindDirection, aWIndSpeed, aLive, aClient);
    end);
end;

procedure TWindControl.updateClient(aWindDirection, aWindSpeed: double; aLive: Integer; aClient: TClient);
var
  jsonLive: string;
begin
  if aLive=0
  then jsonLive := ',"live":false'
  else if aLive>0
  then jsonLive := ',"live":true'
  else jsonLive := '';

  aClient.signalString(
    '{'+
      '"type":"winddata",'+
      '"payload":'+
        '{'+
          '"speed":'+DoubleToJSON(aWindSpeed)+','+
          '"direction":'+DoubleToJSON(aWindDirection)+
          jsonLive+
        '}'+
    '}');
end;

{ TProject }

function TProject.addClient(const aClientID: string): TClient;
begin
  Result := TClient.Create(Self, fProjectCurrentScenario, fProjectRefScenario, aClientID);
  TMonitor.Enter(clients);
  try
    clients.Add(Result);
  finally
    TMonitor.Exit(clients);
  end;
end;

function TProject.addGroup(const aGroup: string; aPresenter: TClient): Boolean;
var
  group: TGroup;
begin
  Result := False; // sentinel
  TMonitor.Enter(fGroups);
  try
    if not fGroups.ContainsKey(aGroup) then
    begin
      group := TGroup.Create(false);
      try
        group.Add(aPresenter);
        Result := True;
      finally
        fGroups.Add(aGroup, group);
      end;
    end;
  finally
    TMonitor.Exit(fGroups);
  end;
end;

function TProject.addGroupMember(const aGroup: string; aMember: TClient): Boolean;
var
  group: TGroup;
begin
  Result := False; // sentinel
  TMonitor.Enter(fGroups);
  try
    if fGroups.TryGetValue(aGroup, group) then
    begin
      if not group.Contains(aMember)
      then group.Add(aMember);
      Result := true;
    end;
  finally
    TMonitor.Exit(fGroups);
  end;
end;

procedure TProject.AddMeasure(const aID, aCategory, aMeasure, aDescription,
  aObjectTypes, aAction, aActionParameters: string; const aActionID: Integer);
var
  measure: TMeasureCategory;
begin
  TMonitor.Enter(fMeasures);
  try
    if fMeasures.TryGetValue(aCategory, measure) then
      measure.AddMeasure(aID, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters, aActionID)
    else
      fMeasures.Add(aCategory, TMeasureCategory.Create(aID, aCategory, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters, aActionID));
  finally
    TMonitor.Exit(fMeasures);
  end;
end;

function TProject.addOrGetClient(const aClientID: string): TClient;
begin
  TMonitor.Enter(clients);
  try
    for Result in clients do
    begin
      if Result.clientID=aClientID
      then exit;
    end;
    // if we come to this point id is not found so create new client
    Result := addClient(aClientID);
  finally
    TMonitor.Exit(clients);
  end;
end;

procedure TProject.addDownloadableFile(const aName: string; const aFileTypes: TArray<string>; aFileGenerator: TFileGenerator);
var
  fi: TDownloadableFileInfo;
begin
  fi.fileGenerator := aFileGenerator;
  fi.fileTypes := aFileTypes;
  fDownloadableFiles.AddOrSetValue(aName, fi);
  forEachClient(
    procedure(aClient: TClient)
    begin
      aClient.SendAddDownloadableFile(aName, aFileTypes);
    end);
end;

constructor TProject.Create(aSessionModel: TSessionModel; aConnection: TConnection;
  const aProjectID, aProjectName, aTilerFQDN, aTilerStatusURL: string; aDBConnection: TCustomConnection;
  aAddBasicLayers: Boolean; aMaxNearestObjectDistanceInMeters: Integer; aMapView: TMapView);
begin
  inherited  Create;
  fMapView := aMapView;
  fGroups := TObjectDictionary<string, TGroup>.Create([doOwnsValues]);
  fDownloadableFiles := TDictionary<string, TDownloadableFileInfo>.Create;
  fClientMessageHandlers := TDictionary<string, TClientMessageHandler>.Create;
  fAddBasicLayers := aAddbasicLayers;
  fControls := TDictionary<string, string>.Create;
  fSessionModel := aSessionModel;
  fWindControl := nil;
  fConnection := aConnection;
  fClients := TGroup.Create(true); // owns
  fScenarios := TObjectDictionary<string, TScenario>.Create([doOwnsValues]);
  fScenarioLinks := TScenarioLink.Create('', '', '', '', '', '', nil);
  fTimers := TTimerPool.Create;
  fDiffLayers := TObjectDictionary<string, TDiffLayer>.Create([doOwnsValues]);
  fProjectID := aProjectID;
  fProjectName := aProjectName;
  fProjectDescription := ''; // default no description set, set by property..
  fMaxNearestObjectDistanceInMeters := aMaxNearestObjectDistanceInMeters;
  fTiler := TTiler.Create(aConnection, aTilerFQDN, aTilerStatusURL);
  fTiler.onTilerStartup := handleTilerStartup;
  fDBConnection := aDBConnection;

  clientMessageHandlers.AddOrSetValue('graphLabelClick', HandleClientGraphLabelClick);

  fMeasures := TObjectDictionary<string, TMeasureCategory>.Create([doOwnsValues]);
  // imb init
  fProjectEvent := fConnection.eventEntry(WS2IMBEventName+'.'+fProjectID, False).subscribe;
  // read basic data
  ReadBasicData();
  // add handler for new sessions
  fProjectEventHandler :=
    procedure(event: TEventEntry; aInt: Integer; const aString: string)
    begin
      try
        if aInt=actionNew then
        begin
          Log.WriteLn(aProjectId+': link to '+aString);
          // add client if not already known
          addOrGetClient(aString.Split(['&'])[0]);
        end
        else if aInt=actionInquire then
        begin
          Log.WriteLn(aProjectId+': possibly a duplicate publisher found', llWarning);
        end;
      except
        on E: Exception
        do Log.WriteLn('Exception in TProject.Create fProjectEvent.OnIntString: '+E.Message, llError);
      end;
    end;
  fProjectEvent.OnIntString.Add(fProjectEventHandler);

  // todo: start timer for tiler status as a heartbeat
  if aTilerStatusURL<>'' then
  begin
    timers.SetTimer(timerTilerStatusAsHeartbeat, hrtNow+DateTimeDelta2HRT(dtOneHour), DateTimeDelta2HRT(dtOneHour));
    log.WriteLn('Set status timer: '+aTilerStatusURL);
  end;

  // link unlinked clients by inquiring existing sessions
  fProjectEvent.signalIntString(actionInquire, '');
end;

destructor TProject.Destroy;
begin
  fTimers.StopAll;
  if Assigned(fProjectEvent) then
  begin
    fProjectEvent.OnIntString.Remove(fProjectEventHandler);
    fProjectEvent.unPublish;
    fProjectEvent.unSubscribe;
  end;
  FreeAndNil(fDiffLayers);
  FreeAndNil(fClients);
  FreeAndNil(fGroups);
  FreeAndNil(fScenarios);
  FreeAndNil(fMeasures);
  FreeAndNil(fControls);
  FreeAndNil(fDownloadableFiles);
  FreeAndNil(fClientMessageHandlers);
  FreeAndNil(fTimers);
  inherited;
  FreeAndNil(fDBConnection);
end;


function TProject.diffElementID(aCurrent, aReference: TScenarioElement): string;
begin
  if aCurrent.scenario.project.projectID=aReference.scenario.project.projectID then
  begin
    if aCurrent.scenario.ID=aReference.scenario.ID
    then Result := aCurrent.scenario.project.ProjectID+sepElementID+aCurrent.scenario.ID+sepElementID+aCurrent.ID+'-'+aReference.ID+'-diff'
    else
    begin
      if aCurrent.ID=aReference.ID
      then Result := aCurrent.scenario.project.ProjectID+sepElementID+aCurrent.scenario.ID+'-'+aReference.scenario.ID+sepElementID+aCurrent.ID+'-diff'
      else Result := aCurrent.scenario.project.ProjectID+sepElementID+aCurrent.scenario.ID+sepElementID+aCurrent.ID+'-'+aReference.scenario.ID+sepElementID+aReference.ID+'-diff';
    end;
  end
  else Result := aCurrent.elementID+'-'+aReference.elementID+'-diff';
end;

procedure TProject.DisableControl(const aControlName: string);
begin
  SetControl(aControlName, controlDisabled);
end;

procedure TProject.EnableControl(const aControlName: string);
begin
  SetControl(aControlName, controlEnabled);
end;

procedure TProject.forEachClient(aForEachClient: TForEachClient);
var
  client: TClient;
begin
  TMonitor.Enter(clients);
  try
    for client in clients
    do aForEachClient(client);
  finally
    TMonitor.Exit(clients);
  end;
end;

function TProject.getClientURL: string;
begin
  Result := GetSetting(WebClientURISwitch, DefaultWebClientURI)+'?session='+projectID;
end;

function TProject.getControl(const aControlName: string): string;
begin
  TMonitor.Enter(fControls);
  try
    if not fControls.TryGetValue(aControlName, Result)
    then Result := '';
  finally
    TMonitor.Exit(fControls);
  end;
end;

{
function TProject.GetControl(const aControlName: string): string;
begin
  TMonitor.Enter(fControls);
  try
    if fControls.ContainsKey(aControlName) then
      Result := fControls[aControlName]
    else
      Result := controlDisabled;
  finally
    TMonitor.Exit(fControls);
  end;
end;

function TProject.GetControlEnabled(const aControlName: string) : Boolean;
begin
  Result := GetControl(aControlName) <> controlDisabled;
end;

function TProject.getControlsJSON: string;
var
  key: string;
begin
  TMonitor.Enter(fControls);
  try
    Result := '';
    for key in fControls.Keys do
    begin
      if Result <> '' then
        Result := Result + ',';
      Result := Result + '"' + key + '":' + fControls[key];
    end;
  finally
    TMonitor.Exit(fControls);
  end;
end;
}
function TProject.getGroupMembersNoLocking(const aGroup: string; aMembers: TGroup): Boolean;
// NO locking
var
  group: TGroup;
begin
  Result := False; // sentinel
  if fGroups.TryGetValue(aGroup, group) then
  begin
    aMembers.AddRange(group.ToArray);
    Result := true;
  end;
end;

function TProject.getMeasuresHistoryJSON: string;
begin
  Result := ''; // default no measures history
end;

function TProject.getMeasuresJSON: string;
var
  category: TMeasureCategory;
begin
  if fMeasures.Count>0 then
  begin
    TMonitor.Enter(fMeasures);
    try
      // todo:
      for category in fMeasures.Values do
      begin
        if Result <> '' then
          Result := Result + ',';
        Result := Result + category.GetCategoryJSON;
      end;
      Result := '[' + Result + ']';
    finally
      TMonitor.Exit(fMeasures);
    end;
  end
  else Result := '[]';
end;

function TProject.getQueryDialogDataJSON: string;
begin
  Result := 'null'; // in case of real data build JSON object {}
end;

function TProject.getWindControl: TWindControl;
begin
  if not Assigned(fWindControl)
  then fWindControl := TWindControl.Create(Self, 0, 0);
  Result := fWindControl;
end;

procedure TProject.HandleClientGraphLabelClick(aProject: TProject;
  aClient: TClient; const aType: string; aPayload: TJSONValue);
begin
  if Assigned(aClient.currentScenario)
  then aClient.currentScenario.HandleClientGraphLabelClick(aClient, aPayload);
end;

procedure TProject.handleClientMessage(aClient: TClient; aScenario: TScenario; aJSONObject: TJSONObject);
begin
  // default no action
end;

procedure TProject.handleFileUpload(aClient: TClient; const aFileInfo: TClientFileUploadInfo);
begin
  // default no action
end;

procedure TProject.handleNewClient(aClient: TClient);
var
  live: Integer;
begin
  if Assigned(fWindControl) then
  begin
    if fWindControl.live
    then live := 1
    else live := 0;
    fWindControl.updateClient(fWindControl.windDirection, fWindControl.windSpeed, live, aClient);
  end;
end;

procedure TProject.handleNotAuthorized(aClient: TClient; aMessage: TJSONObject; const aToken: string);
begin
  // default no action, just log not authorized
  Log.WriteLn('Client not authorized: '+aToken+' ('+aMessage.ToJSON+')', llWarning);
end;

procedure TProject.handleRemoveClient(aClient: TClient);
begin
  // default no action
end;

procedure TProject.handleTilerStartup(aTiler: TTiler; aStartupTime: TDateTime);
var
  scenario: TScenario;
  diffLayer: TDiffLayer;
begin
  TMonitor.Enter(scenarios);
  try
    for scenario in scenarios.Values
    do scenario.registerLayers;
  finally
    TMonitor.Exit(scenarios);
  end;
  // diff layers
  TMonitor.Enter(diffLayers);
  try
    for diffLayer in diffLayers.Values do
    begin
      if Assigned(diffLayer.tilerLayer) then
      begin
        // reset tiler url to avoid invalid url send to client
        diffLayer.tilerLayer.resetURL;
        // re-register
        diffLayer.registerOnTiler;
      end;
    end;
  finally
    TMonitor.Exit(diffLayers);
  end;
end;

function TProject.isPresenterNoLocking(const aGroup: string; aMember: TClient): Boolean;
var
  group: TGroup;
begin
  Result := fGroups.TryGetValue(aGroup, group) and (group.Count>0) and (group[0]=aMember);
end;

procedure TProject.Login(aClient: TClient; aJSONObject: TJSONObject);
var
  scenarioID: string;
  scenario: TScenario;
begin
  if not aJSONObject.TryGetValue<string>('userid', aClient.fUserID)
  then aClient.fUserID := '<no-user-id>';
  if not aJSONObject.TryGetValue<string>('editMode', aClient.fEditMode)
  then aClient.fEditMode := '';
  if aJSONObject.TryGetValue<string>('scenario', scenarioID) and (scenarioID<>'') then
  begin
    TMonitor.Enter(scenarios);
    try
      if scenarios.TryGetValue(scenarioID, scenario) then
      begin
        aClient.removeClient(aClient.currentScenario);
        aClient.currentScenario := scenario;
        aClient.addClient(aClient.currentScenario);
        Log.WriteLn('connected to scenario '+scenarioID+' user '+aClient.fUserID);
        // retry
        aClient.SendSession(Self);
        //SendMeasures(); // todo:?
        aClient.SendDomains('domains');
      end
      else Log.WriteLn('Client login: could not connect to scenario '+scenarioID+' user "'+aClient.fUserID+'"', llError);
    finally
      TMonitor.Exit(scenarios);
    end;
  end;
end;

procedure TProject.handleTypedClientMessage(aClient: TClient; const aMessageType: string; var aJSONObject: TJSONObject);
var
  handler: TClientMessageHandler;
  payload: TJSONValue;
begin
  if fClientMessageHandlers.TryGetValue(aMessageType, handler) then
  begin
    if not aJSONObject.TryGetValue<TJSONValue>('payload', payload)
    then payload := nil;
    handler(Self, aClient, aMessageType, payload);

    // todo: only for seeing if everyhting works ok
    // the portal uses messages without a payload component
    if (aMessageType<>'RequestPortalLogin') and (aMessageType<>'RequestPortalProjectList') and (aMessageType<>'RequestPortalParameters')then
    begin
      if not Assigned(payload)
      then Log.WriteLn('TProject.handleTypedClientMessage: payload is not a valid json value: '+aJSONObject.ToJSON, llWarning);
    end;
  end;
end;

function TProject.isAuthorized(aClient: TClient; const aToken: string): Boolean;
begin
  Result := True; // by default all clients are authorized
end;

procedure TProject.ReadBasicData;
begin
  // default no action
end;

function TProject.ReadScenario(const aID: string): TScenario;
begin
  Result := nil;
end;

function TProject.removeGroupNoLocking(const aGroup: string; aNoSendCloseMember: TClient): Boolean;
var
  group: TGroup;
  member: TClient;
begin
  Result := False; //  sentinel
  if fGroups.TryGetValue(aGroup, group) then
  begin
    // signal clients that group is going 'away'
    for member in group do
    begin
      if member<>aNoSendCloseMember
      then member.signalString('{"type":"groupcontrol","payload":{"command":"groupclose","group":"'+aGroup+'"}}');
    end;
    fGroups.Remove(aGroup);
    Result := True;
  end;
end;

function TProject.removeGroupMember(const aGroup: string; aMember: TClient): Boolean;
var
  group: TGroup;
begin
  Result := False; // sentinel
  TMonitor.Enter(fGroups);
  try
    if fGroups.TryGetValue(aGroup, group) then
    begin
      if group.Contains(aMember) then
      begin
        if group[0]<>aMember then
        begin
          group.Remove(aMember);
          Result := True;
        end;
      end
      else Result := True;
    end;
  finally
    TMonitor.Exit(fGroups);
  end;
end;

function TProject.removeMemberFromAllGroups(aMember: TClient): Boolean;
var
  group: string;
  removeGroups: TList<string>;
  ngp: TPair<string, TGroup>;
begin
  Result := False; //  sentinel
  TMonitor.Enter(fGroups);
  try
    removeGroups := TList<string>.Create;
    try
      for ngp in fGroups do
      begin
        if ngp.Value.Contains(aMember) then
        begin
          // check for presenter
          if ngp.Value[0]=aMember
          then removeGroups.Add(ngp.Key)
          else ngp.Value.Remove(aMember);
          Result := true;
        end;
      end;
      // remove groups where aMember was presenter
      for group in removeGroups
      do removeGroupNoLocking(group, aMember);
    finally
      removeGroups.Free;
    end;
  finally
    TMonitor.Exit(fGroups);
  end;
end;

procedure TProject.removeDownloadableFile(const aName: string);
begin
  forEachClient(
    procedure(aClient: TClient)
    begin
      aClient.SendRemoveDownloadableFile([aName]);
    end);
  fDownloadableFiles.Remove(aName);
end;

{
procedure TProject.SendPreview;
begin
  Log.WriteLn('Sending preview to clients connected to project '+Self.ProjectName);
  forEachClient(
    procedure(aClient: TClient)
    var
      se: TClientSubscribable;
      preview: string;
		begin
      TMonitor.Enter(aClient.subscribedElements);
      try
        for se in aClient.subscribedElements do
         begin
           if se is TLayer then
           begin
             preview := (se as TLayer).PreviewBase64;
             if preview<>'' then
             begin
               aClient.SendPreview(se.elementID, preview);
               Log.WriteLn('Send preview for '+se.elementID);
             end
             else Log.WriteLn('NO preview to send for '+se.elementID);
           end;
         end;
      finally
        TMonitor.Exit(aClient.subscribedElements);
      end;
    end);
end;
}

{
procedure TProject.SendRefresh;
begin
  Log.WriteLn('Sending refresh to clients connected to project '+Self.ProjectName);
  forEachClient(
    procedure(aClient: TClient)
    var
      se: TClientSubscribable;
      tiles: string;
    begin
      TMonitor.Enter(aClient.subscribedElements);
      try
        for se in aClient.subscribedElements do
        begin
          if se is TLayer then
          begin
            tiles := (se as TLayer).uniqueObjectsTilesLink;
            aClient.SendRefresh(se.elementID, '', tiles);
            Log.WriteLn('Send refresh for '+se.elementID+': '+tiles);
          end;
          // todo: else
        end;
      finally
        TMonitor.Exit(aClient.subscribedElements);
      end;
    end);
end;
}

procedure TProject.SendString(const aString: string);
begin
  //Log.WriteLn('Sending string to clients connected to project '+Self.ProjectName);
  forEachClient(
    procedure(aClient: TClient)
    begin
      aClient.signalString(aString);
    end);
end;

procedure TProject.SetControl(const aControlName, aControlValue: string);
var
  signaledClients: TDictionary<TClient, boolean>;
  scenario: TScenario;
begin
  TMonitor.Enter(fControls);
  try
    fControls.AddOrSetValue(aControlName, aControlValue);
  finally
    TMonitor.Exit(fControls);
  end;
  signaledClients := TDictionary<TClient, boolean>.Create;
  try
    forEachClient(
      procedure(aClient: TClient)
      begin
        aClient.signalControl(aControlName);
        signaledClients.AddOrSetValue(aClient, True);
      end);
    for scenario in Scenarios.Values do
    begin
      scenario.forEachSubscriber<TClient>(
        procedure(aClient: TClient)
        begin
          if not signaledClients.ContainsKey(aClient) then
          begin
            aClient.signalControl(aControlName);
            signaledClients.AddOrSetValue(aClient, True);
          end;
        end);
    end;
  finally
    signaledClients.Free;
  end;
end;

procedure TProject.setMapView(const aValue: TMapView);
begin
  fMapView := aValue;
  // todo: signal to connected clients? used to set scenarios so maybe not needed hereend;
end;

//procedure TProject.setMeasuresEnabled(aValue: Boolean);
//var
//  client: TClient;
//begin
//  if fMeasuresEnabled<>aValue then
//  begin
//    fMeasuresEnabled := aValue;
//    // update session info for all connected clients
//    // todo: check locking and use forEachClient
//    for client in clients
//    do client.SendMeasuresEnabled;
//  end;
//end;

//procedure TProject.setMeasuresHistoryEnabled(aValue: Boolean);
//var
//  client: TClient;
//begin
//  if fMeasuresHistoryEnabled<>aValue then
//  begin
//    fMeasuresHistoryEnabled := aValue;
//    // update session info for all connected clients
//    // todo: check locking and use forEachClient
//    for client in clients
//    do client.SendMeasuresHistoryEnabled;
//  end;
//end;

procedure TProject.setProjectDescription(const aValue: string);
begin
  fProjectDescription := aValue;
  // todo: (re-)signal description to connected clients
end;

procedure TProject.setProjectName(const aValue: string);
begin
  fProjectName := aValue;
  // todo: (re-)signal name to connected clients

end;

//procedure TProject.setSelectionEnabled(aValue: Boolean);
//var
//  client: TClient;
//begin
//  if fSelectionEnabled<>aValue then
//  begin
//    fSelectionEnabled := aValue;
//    // update session info for all connected clients
//    // todo: check locking and use forEachClient
//    for client in clients
//    do client.SendSelectionEnabled;
//  end;
//end;
//
//procedure TProject.setTimeSlider(aValue: Integer);
//var
//  client: TClient;
//begin
//  if fTimeSlider<>aValue then
//  begin
//    fTimeSlider := aValue;
//    // update session info for all connected clients
//    // todo: check locking and use forEachClient
//    for client in clients
//    do client.SendTimeSlider;
//  end;
//end;

procedure TProject.timerTilerStatusAsHeartbeat(aTimer: TTimer; aTime: THighResTicks);
begin
  // request status from tiler as a heartbeat (keep IIS isapi module a live)
  try
    fTiler.getTilerStatus;
    Log.WriteLn('keep-a-live tiler status request');
  except
    on E: Exception
    do Log.WriteLn('Exception in keep-a-live tiler status request: '+E.Message, llError);
  end;
end;

{ TModel }

constructor TModel.Create(aConnection: TConnection);
begin
  inherited Create;
  fConnection := aConnection;
end;

{ TSessionModel }

constructor TSessionModel.Create(aConnection: TConnection);
begin
  inherited Create(aConnection);
  fProjects := TObjectList<TProject>.Create;
end;

destructor TSessionModel.Destroy;
begin
  FreeAndNil(fProjects); // todo: pointer exception some where during free of this list
  inherited;
end;

function TSessionModel.FindElement(const aElementID: string): TClientSubscribable;
var
  s: TArray<System.string>;
  project: TProject;
  scenario: TScenario;
  layer: TLayerBase;
  kpi: TKPI;
  chart: TChart;
  diffLayer: TDiffLayer;
begin
  s := aElementID.Split([sepElementID]);
  if Length(s)>=3 then
  begin
    for project in fProjects do
    begin
      if project.ProjectID.CompareTo(s[Length(s)-3])=0 then
      begin
        TMonitor.Enter(project.scenarios); //check scenario elements
        try
          if project.scenarios.TryGetValue(s[Length(s)-2], scenario) then
          begin
            if scenario.KPIs.TryGetValue(s[Length(s)-1], kpi)
            then Exit(kpi)
            else if scenario.Layers.TryGetValue(s[Length(s)-1], layer)
            then Exit(layer)
            else if scenario.Charts.TryGetValue(s[Length(s)-1], chart)
            then Exit(chart);
          end;
        finally
          TMonitor.Exit(project.scenarios);
        end;
        TMonitor.Enter(project.diffLayers);
        try
          if project.diffLayers.TryGetValue(aElementID, diffLayer) then
            Exit(diffLayer)
          else
            Exit(nil);
        finally
          TMonitor.Exit(project.diffLayers);
        end;
      end;
    end;
  end;
  Exit(nil);
end;

{ TMapView }

class function TMapView.Create(aLat, aLon, aZoom: Double): TMapView;
begin
  Result.fLat := aLat;
  Result.fLon := aLon;
  Result.fZoom := aZoom;
end;

class function TMapView.Create(const aConfig: string): TMapView;
var
  config: TArray<string>;
begin
  // lat,lon,zoom (lat, lon: dotFormat floats in wgs84, zoom: tiles zoom level)
  // defaults: TNO building, Uithof, Utrecht, The Netherlands
  Result.fLat := 52.088601;
  Result.fLon := 5.166504;
  Result.fZoom := 11;
  try
    config := aConfig.Split([',', ';']);
    if length(config)>=2 then
    begin
      Result.fLat := double.Parse(config[0], dotFormat);
      Result.fLon := double.Parse(config[1], dotFormat);
      if length(config)>2
      then Result.fZoom := double.Parse(config[2], dotFormat);
    end;
  except
  end;
end;

procedure TMapView.DumpToLog;
begin
  Log.WriteLn('MapView: lat:'+lat.ToString+' lon:'+lon.ToString+' zoom:'+zoom.ToString);
end;

function TMapView.ToExtent(xTileCount, yTileCount: Integer): TWDExtent;
var
  deg: double;
  xDiff, yDiff: Double;
begin
  deg := 360 / Power(2, Zoom);

  xDiff := deg * (xTileCount/2);
  yDiff := deg * (yTileCount/2);

  Result.Create;
  Result.xMin := lon - xDiff;
  Result.xMax := lon + xDiff;
  Result.yMin := lat - yDiff;
  Result.yMax := lat + yDiff;
end;

{ TLayerSwitch }

constructor TLayerSwitch.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean; const aObjectTypes: string; aBasicLayer: Boolean);
begin
  fZoomLayers := TList<TLayerOnZoom>.Create;
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, aObjectTypes, '', ltSwitch, True, Double.NaN, aBasicLayer);

end;

destructor TLayerSwitch.Destroy;
begin

  inherited;
  FreeAndNil(fZoomLayers);
end;

function TLayerSwitch.getJSON: string;
var
  _json: string;
  loz: TLayerOnZoom;
begin
  // layers:[{zoom:0,layer:{layer}}]
  _json := '';
  for loz in fZoomLayers
  do jsonAdd(_json, '{"zoom":'+DoubleToJSON(loz.zoomLevel)+', "layer":{'+loz.layer.JSON+'}}');
  Result := inherited getJSON+',"layers":['+_json+']';
end;

{ TLayerOnZoom }

class function TLayerOnZoom.Create(aZoomLevel: Double; aLayer: TLayer): TLayerOnZoom;
begin
  Result.zoomLevel := aZoomLevel;
  Result.layer := aLayer;
end;

{ TChart }

constructor TChart.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean;
  const aChartType: string; const aDisplayGroup: string);
begin
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, aDisplayGroup);
  fChartType := aChartType;
  fChildCharts := TDictionary<string, TChart>.Create;
  fPreviewRequestTimer := scenario.project.Timers.CreateInactiveTimer();
  fPreviewRequestTimer.MaxPostponeDelta := DateTimeDelta2HRT(dtOneSecond*MaxchartPreviewTime); // todo: make local and editable via ini
end;

destructor TChart.Destroy;
begin
  if Assigned(fPreviewRequestTimer) then
  begin
    fPreviewRequestTimer.Cancel;
    fPreviewRequestTimer := nil;
  end;
  FreeAndNil(fChildCharts);
  inherited;
end;

function TChart.getJSON: string;
begin
  Result := inherited;
  jsonAdd(Result,
    '"type":"'+fChartType+'",'+
    '"data":['+getJSONData+']');
end;

procedure TChart.HandleGraphLabelClick(aClient: TClient; aLabelTitle: string);
begin

end;

procedure TChart.reset;
begin
  //default: do nothing;
end;

procedure TChart.Show;
begin
  scenario.project.forEachClient(
    procedure(aClient: TClient)
    begin
      // todo: can be for more then 1 chart!
      aClient.signalString('{"type":"showchart","payload":["'+elementID+'"]}');
    end);
end;

{ TSpiderChart }

constructor TSpiderChart.Create(aScenario: TScenario; const aDomain, aID, aName, aDescription: string; aDefaultLoad: Boolean);
begin
  inherited Create(aScenario, aDomain, aID, aName, aDescription, aDefaultLoad, 'spider');
  fData := TSpiderData.Create(aName);
  fWidth := 400;
  fHeight := 400;
  fMaxScale := 10; // als niet gedefineerd (NaN) pakt hij de maximale van alle values -> 10 is een goede default?
end;

destructor TSpiderChart.Destroy;
begin
  FreeAndNil(fData);
  inherited;
end;

function TSpiderChart.getJSON: string;
begin
  Result := inherited getJSON;
  jsonAdd(Result,
    //'"level":0'+
    '"width":'+fWidth.toString+',"height":'+fHeight.toString+',"maxValue":'+DoubleToJSON(fMaxScale));
//  if fLabels
//  then Result := Result+',"labels":true'
//  else Result := Result+',"labels":false';
//  if fLegend
//  then Result := Result+',"legend":true'
//  else Result := Result+',"legend":false';
//  if not double.IsNan(fMaxScale)
//  then Result := Result+ ',"maxValue":'+fMaxScale.toString(dotFormat);
end;

function TSpiderChart.getJSONData: string;
begin
  fData.normalize(fMaxScale);
  Result := '{'+fData.getJSON+'}';
end;

procedure TSpiderChart.normalize;
begin
  fData.normalize(fMaxScale);
end;

procedure TSpiderChart.RecalculateAverages;
begin
  fData.RecalculateAverages;
end;

{ TSpiderData }

constructor TSpiderData.Create(const aTitle: string);
begin
  inherited Create;
  fTitle  := aTitle;
  fLabels := TDictionary<string, Integer>.Create;
  fAxes := TDictionary<string, Integer>.Create;
  fValues := TObjectList<TObjectList<TSpiderDataValue>>.Create(True);
end;

function TSpiderData.createValue(const aLabel, aAxis: string): TSpiderDataValue;
var
  l: Integer;
  a: Integer;
  va: TObjectList<TSpiderDataValue>;
begin
  // label, axis
  // check if new label
  if not fLabels.TryGetValue(aLabel, l) then
  begin
    // new label
    l := fLabels.Count;
    fLabels.Add(aLabel, l);
    a := fAxes.Count-1;
    // check all new rows in fValues
    while l>=fValues.Count do
    begin
      va := TObjectList<TSpiderDataValue>.Create;
      // check columns for this row
      while a>=va.Count
      do va.Add(TSpiderDataValue.Create(0, '', nil));
      fValues.Add(va);
    end;
  end;
  // check if new axis
  if not fAxes.TryGetValue(aAxis, a) then
  begin
    // new axis
    a := fAxes.Count;
    fAxes.Add(aAxis, a);
    // check all rows in fValues
    for va in fValues do
    begin
      // check columns for this row
      while a>=va.Count
      do va.Add(TSpiderDataValue.Create(0, '', nil));
    end;
  end;
  Result := fValues[l].Items[a];
end;

destructor TSpiderData.Destroy;
begin
  FreeAndNil(fLabels);
  FreeAndNil(fAxes);
  FreeAndNil(fValues);
  inherited;
end;

function TSpiderData.getJSON: string;

  function jsonAxes: string;
  var
    aip: TPair<string, Integer>;
  begin
    Result := '';
    for aip in fAxes
    do jsonAdd(Result, '"'+aip.Key+'"');
  end;

  function jsonLabels: string;
  var
    lip: TPair<string, Integer>;
  begin
    Result := '';
    for lip in fLabels
    do jsonAdd(Result, '"'+lip.Key+'"');
  end;

  function jsonValues: string;
  var
    row: TObjectList<TSpiderDataValue>;
    col: TSpiderDataValue;
    jsonRow: string;
  begin
    Result := '';
    for row in fValues do
    begin
      jsonRow := '';
      for col in row do
      begin
        // open column
        if col.value.IsNan
        then jsonAdd(jsonRow, '{"value":'+'0')
        else jsonAdd(jsonRow, '{"value":'+DoubleToJSON(col.value));
        if col.link<>''
        then jsonRow := jsonRow+',"link":"'+col.link+'"';
        if Assigned(col.data)
        then jsonRow := jsonRow+',"data":{'+col.data.getJSON+'}';
        // close column
        jsonRow := jsonRow+'}';
      end;
      if jsonRow<>''
      then jsonAdd(Result, '['+jsonRow+']');
    end;
  end;

begin
  Result :=
    '"title":"'+fTitle+'",'+
    '"labels":['+jsonLabels+'],'+
    '"axes":['+jsonAxes+'],'+
    '"matrix":['+jsonValues+']';
end;

function TSpiderData.GetOrAddValue(const aLabel, aAxis: string): TSpiderDataValue;
begin
  Result := getValue(aLabel, aAxis);
  if not Assigned(Result)
  then Result := createValue(aLabel, aAxis);
end;

function TSpiderData.getValue(const aLabel, aAxis: string): TSpiderDataValue;
var
  l: Integer;
  a: Integer;
begin
  if fLabels.TryGetValue(aLabel, l) and fAxes.TryGetValue(aAxis, a)
  then Result := fValues[l][a]
  else Result := nil;
end;

function TSpiderData.getValueData(const aLabel, aAxis: string): TSpiderData;
var
  sd: TSpiderDataValue;
begin
  sd := getValue(aLabel, aAxis);
  if Assigned(sd)
  then Result := sd.Data
  else Result := nil;
end;

function TSpiderData.getValueLink(const aLabel, aAxis: string): string;
var
  sd: TSpiderDataValue;
begin
  sd := getValue(aLabel, aAxis);
  if Assigned(sd)
  then Result := sd.link
  else Result := '';
end;

function TSpiderData.getValueValue(const aLabel, aAxis: string): Double;
var
  sd: TSpiderDataValue;
begin
  sd := getValue(aLabel, aAxis);
  if Assigned(sd)
  then Result := sd.value
  else Result := Double.NaN;
end;

function TSpiderData.maxValue: Double;
var
  row: TObjectList<TSpiderDataValue>;
  col: TSpiderDataValue;
begin
  Result := Double.NaN;
  for row in fValues do
    for col in row do
    begin
      if (not col.value.IsNan) and (Result.IsNan or (Result<col.value))
      then Result := col.value;
    end;
end;

procedure TSpiderData.normalize(aMaxScale: Double);
// recursive
var
  row: TObjectList<TSpiderDataValue>;
  col: TSpiderDataValue;
begin
  normalize(maxValue, aMaxScale);
  for row in fValues do
    for col in row do
    begin
      if Assigned(col.data)
      then col.data.normalize(aMaxScale);
    end;
end;

function TSpiderData.RecalculateAverages: Double;
// recursive
var
  row: TObjectList<TSpiderDataValue>;
  col: TSpiderDataValue;
  c: Integer;
  v: Double;
begin
  Result := Double.NaN;
  c := 0;
  for row in fValues do
    for col in row do
    begin
      if Assigned(col.data)  then
      begin
        v := col.data.RecalculateAverages();
        if not v.IsNan
        then col.value := v;
      end;
      if not col.value.IsNan then
      begin
        if Result.IsNan
        then Result := col.value
        else Result := Result+col.value;
        c := c+1;
      end;
    end;
  if not Result.IsNan
  then Result := Result/c;
end;

procedure TSpiderData.setValueData(const aLabel, aAxis: string; const Value: TSpiderData);
var
  sd: TSpiderDataValue;
begin
  sd := getValue(aLabel, aAxis);
  if not Assigned(sd)
  then sd := createValue(aLabel, aAxis);
  // clean up previous value (ignrores nil)
  sd.data.Free;
  sd.data := Value; // new value now owned
end;

procedure TSpiderData.setValueLink(const aLabel, aAxis, Value: string);
var
  sd: TSpiderDataValue;
begin
  sd := getValue(aLabel, aAxis);
  if not Assigned(sd)
  then sd := createValue(aLabel, aAxis);
  sd.link := Value;
end;

procedure TSpiderData.setValueValue(const aLabel, aAxis: string; const Value: Double);
var
  sd: TSpiderDataValue;
begin
  sd := getValue(aLabel, aAxis);
  if not Assigned(sd)
  then sd := createValue(aLabel, aAxis);
  sd.value := Value;
end;

procedure TSpiderData.normalize(aMaxValue, aMaxScale: Double);
// single
var
  row: TObjectList<TSpiderDataValue>;
  col: TSpiderDataValue;
begin
  if not (aMaxValue.IsNan or (aMaxValue=0)) then
  begin
    for row in fValues do
      for col in row do
      begin
        if not col.value.IsNan
        then col.value := aMaxScale*col.value/aMaxValue;
      end;
  end;
end;

{ TSpiderDataValue }

constructor TSpiderDataValue.Create(aValue: Double; const aLink: string; aData: TSpiderData);
begin
  inherited Create;
  fValue := aValue;
  fLink := aLink;
  fData := aData;
end;

destructor TSpiderDataValue.Destroy;
begin

  inherited;
end;

{ TMeasureCategory }

procedure TMeasureCategory.AddMeasure(const aID, aMeasure,
  aDescription, aObjectTypes, aAction, aActionParameters: string;
  aActionID: Integer);
var
  measure: TMeasure;
begin
  TMonitor.Enter(fMeasures);
  try
    if fMeasures.TryGetValue(aMeasure, measure) then
      measure.AddAction(aID, aDescription, aObjectTypes, aAction, aActionParameters, aActionID)
    else
      fMeasures.Add(aMeasure, TMeasure.Create(aID, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters, aActionID));
  finally
    TMonitor.Exit(fMeasures);
  end;
end;

constructor TMeasureCategory.Create(const aID, aCategory, aMeasure,
  aDescription, aObjectTypes, aAction, aActionParameters: string;
  aActionID: Integer);
begin
  fCategory := aCategory;
  fMeasures := TObjectDictionary<string, TMeasure>.Create([doOwnsValues]);
  AddMeasure(aID, aMeasure, aDescription, aObjectTypes, aAction, aActionParameters, aActionID);
end;

destructor TMeasureCategory.Destroy;
begin
  inherited;
  FreeAndNil(fMeasures);
end;

function TMeasureCategory.FindMeasure(const aActionID: string;
  out aMeasure: TMeasureAction): Boolean;
var
  measure: TMeasure;
begin
  Result := False;
  for measure in fMeasures.Values do
  begin
    if measure.FindMeasure(aActionID, aMeasure) then
    begin
      Result := True;
      exit;
    end;
  end;
end;

function TMeasureCategory.GetCategoryJSON: string;
var
  measure: TMeasure;
begin
  Result := '';
  for measure in fMeasures.Values do
  begin
    if Result <> '' then
      Result := Result + ',';
    Result := Result + measure.getMeasureJSON;
  end;
  Result := '{"measures":[' + Result + '], "category":"' + fCategory + '"}';
end;

procedure TMeasureCategory.RemoveMeasure(const aMeasure, aID);
begin
  //todo: is this needed? -> implement
end;

function TMeasureAction.getActionJSON: string;
begin
  Result := '{"id":"' + fID + '",' +
    '"action":"' + fAction + '",' +
    '"parameters":'+fActionParameters+','+
    '"description":"' + fDescription + '",' +
    '"objecttypes":"' + fObjectTypes + '"}';
end;

{ TMeasureHistory }

constructor TMeasureHistory.Create(const aMeasureID, aCategory, aMeasure, aAction,
  aObjectIDs, aSelectCategories, aScenarioID: string; const aUTCTime: TDateTime);
begin
  fMeasureID := aMeasureID;
  fCategory := aCategory;
  fMeasure := aMeasure;
  fAction := aAction;
  fObjectIDs := aObjectIDs;
  fSelectCategories := aSelectCategories;
  fScenarioID := aScenarioID;
  fUTCTime := aUTCTime;
end;

constructor TMeasureHistory.Create(aMeasure: TMeasureAction; const objectIDs,
  aSelectCategories: string);
begin
  //todo
end;

{ TSubscribeObject }

constructor TSubscribeObject.Create;
begin
  fClosing := False;
  fSubscribers := TObjectDictionary<TSubscribeObject, TSubAction>.Create([]);
  fSubscriptions := TObjectDictionary<TSubscribeObject, TSubscribeObject>.Create([]);
  fSubscribersLock.Create;
  fSubscriptionsLock.Create;
end;

destructor TSubscribeObject.Destroy;
var
  sub: TSubscribeObject;
begin
  EnterCriticalSection(subscribeObjectCS);
  try
    SubscribersLock.BeginWrite;
    try
      for sub in fSubscribers.Keys do
        sub.UnsubscribeFrom(Self, True);
      FreeAndNil(fSubscribers);
    finally
      SubscribersLock.EndWrite;
    end;
    SubscriptionsLock.BeginWrite;
    try
      for sub in fSubscriptions.Keys do
        sub.HandleUnsubscribe(Self);
      FreeAndNil(fSubscriptions);
    finally
      SubscriptionsLock.EndWrite;
    end;
    inherited;
  finally
    LeaveCriticalSection(subscribeObjectCS);
  end;
end;

procedure TSubscribeObject.forEachSubscriber<T>(aForEach: TForEach<T>);
var
  subscriber: TSubscribeObject;
begin
  SubscribersLock.BeginRead;
  try
    for subscriber in Subscribers.Keys do
      if subscriber is T then
        aForEach(subscriber as T);
  finally
    SubscribersLock.EndRead;
  end;
end;

function TSubscribeObject.HandleSubscribe(aSubscriber: TSubscribeObject;
  aSubAction: TSubAction): Boolean;
begin
  Result := False;
  if Assigned(fSubscribers) then
  begin
    SubscribersLock.BeginWrite;
    try
      if Assigned(fSubscribers) then
      begin
        if not fSubscribers.ContainsKey(aSubscriber) then
          Result := True;
        fSubscribers.AddOrSetValue(aSubscriber, aSubAction);
      end;
    finally
      SubscribersLock.EndWrite;
    end;
  end;
end;

function TSubscribeObject.HandleUnsubscribe(aSubscriber: TSubscribeObject): Boolean;
begin
  Result := False;
  SubscribersLock.BeginWrite;
  try
    if Assigned(fSubscribers) and fSubscribers.ContainsKey(aSubscriber) then
    begin
      fSubscribers.Remove(aSubscriber);
      Result := True;
    end;
  finally
    SubscribersLock.EndWrite;
  end;
end;

procedure TSubscribeObject.SendAnonymousEvent(const aAction,
  aObjectID: Integer);
begin
  SendEvent(nil, aAction, aObjectID);
end;

procedure TSubscribeObject.SendEvent(aSender: TSubscribeObject; const aAction,
  aObjectID: Integer);
var
  subscriber: TPair<TSubscribeObject, TSubAction>;
begin
  SubscribersLock.BeginRead;
  try
    for subscriber in fSubscribers do
      if subscriber.Key <> aSender then
        subscriber.Value(aSender, aAction, aObjectID);
  finally
    SubscribersLock.EndRead;
  end;
end;

function TSubscribeObject.SubscribeTo(aSubscribable: TSubscribeObject;
  aSubAction: TSubAction): Boolean;
begin
  EnterCriticalSection(subscribeObjectCS);
  try
    Result := False;
    if (aSubscribable.HandleSubscribe(Self, aSubAction)) then
      begin
      SubscriptionsLock.BeginWrite;
      try
        if Assigned(fSubscriptions) then
        begin
          if not fSubscriptions.ContainsKey(aSubscribable) then
            Result := True;
          fSubscriptions.AddOrSetValue(aSubscribable, aSubscribable);
        end;
      finally
        SubscriptionsLock.EndWrite;
      end;
    end;
  finally
    LeaveCriticalSection(subscribeObjectCS);
  end;
end;

function TSubscribeObject.UnsubscribeFrom(aSubscribable: TSubscribeObject; aDestroying: Boolean = False): Boolean;
begin
  Result := False;
  EnterCriticalSection(subscribeObjectCS);
  try
    if not aDestroying then //only HandleUnsubscribe if aSubscribable is not in his Destroy function
      aSubscribable.HandleUnSubscribe(Self);
    SubscriptionsLock.BeginWrite;
    try
      if fSubscriptions.ContainsKey(aSubscribable) then
      begin
        fSubscriptions.Remove(aSubscribable);
        Result := True;
      end;
    finally
      SubscriptionsLock.EndWrite;
    end;
  finally
    LeaveCriticalSection(subscribeObjectCS);
  end;
end;

{ TLayerObjectWithID }

constructor TLayerObjectWithID.Create(aLayer: TLayer; const aID: TWDID);
begin
  inherited Create(aLayer);
  fID := aID;
end;

function TLayerObjectWithID.Encode: TByteBuffer;
begin
  Result := TByteBuffer.bb_tag_rawbytestring(icehObjectID, ID);
end;

function TLayerObjectWithID.EncodeRemove: TByteBuffer;
begin
  Result := TByteBuffer.bb_tag_rawbytestring(icehNoObjectID, ID);
end;

initialization
  InitializeCriticalSection(subscribeObjectCS);
finalization
  DeleteCriticalSection(subscribeObjectCS);
end.
