unit wunderground;

interface

uses
  myCoInit, // first
  Logger,
  Variants,
  System.JSON,
  System.SysUtils,
  Winapi.ActiveX,
  System.SysConst,
  System.Win.ComObj;

const
  API_KEY_RESPONSE = 'e5dbb01ce9f0fbdc'; // wunderground key for response, hans.cornelissen@tno.nl

  WUNDERGROUND_URL_BASE = 'http://api.wunderground.com/api/'+API_KEY_RESPONSE+'/conditions/q/'; // add lat,lon.json

  dotFormat: TFormatSettings = (DecimalSeparator:'.');

type
  TMeteoInfo = record
  class function Create(aLat, aLon: Double): TMeteoInfo; static;
  public
    acquisitionTime: TDateTime; // local
    acquisitionLat: Double; // input
    acquisitionLon: Double; // input
    acquisitionElevation: Double; // from request

    observationLat: Double;
    observationLon: Double;

    weather: string;
    temp_c: Double;
    relative_humidity: string;
    wind_dir: string;
    wind_degrees: Double;
    wind_kph: Double;
    wind_gust_kph: Double;
    pressure_mb: Double;
    dewpoint_c: Double;
  public
    function MeteoAt(aLat, aLon: Double): Boolean;
  end;



implementation

{ TMeteoInfo }

class function TMeteoInfo.Create(aLat, aLon: Double): TMeteoInfo;
begin
  // acquisition
  Result.acquisitionTime := Double.NaN;
  Result.acquisitionLat := Double.NaN;
  Result.acquisitionLon := Double.NaN;
  Result.acquisitionElevation := Double.NaN;
  Result.observationLat := Double.NaN;
  Result.observationLon := Double.NaN;
  // meteo
  Result.weather := '';
  Result.temp_c := Double.NaN;
  Result.relative_humidity := '';
  Result.wind_dir := '';
  Result.wind_degrees := Double.NaN;
  Result.wind_kph := Double.NaN;
  Result.wind_gust_kph := Double.NaN;
  Result.pressure_mb := Double.NaN;
  Result.dewpoint_c := Double.NaN;
  if not (aLat.IsNan or aLon.IsNan)
  then Result.MeteoAt(aLat, aLon);
end;

function TMeteoInfo.MeteoAt(aLat, aLon: Double): Boolean;
var
  http: variant;
  jsonObject: TJSONObject;
  display_location: TJSONObject;
  response: string;
  current_observation: TJSONObject;
  observation_location: TJSONObject;
begin
  // sentinel
  Result := False;
  CheckCoInit;
  // do request
  http := VarNull;
  try
    http := createoleobject('WinHttp.WinHttpRequest.5.1');
  except
  end;
  if not VarIsNull(http) then
  begin
    try
      try
        http.open('GET', WUNDERGROUND_URL_BASE+aLat.toString(dotFormat)+','+aLon.toString(dotFormat)+'.json', false);
        http.send;
        // process response
        response := http.responsetext;
        jsonObject := TJSONObject.ParseJSONValue(response) as TJSONObject;
        try
          if jsonObject.TryGetValue<TJSONObject>('current_observation', current_observation) then
          begin
            // acquisition parameters
            acquisitionTime := Now;
            acquisitionLat := aLat;
            acquisitionLon := aLon;
            // meteo parameters
            if current_observation.TryGetValue<TJSONObject>('display_location', display_location) then
            begin
              acquisitionElevation := Double.Parse(display_location.GetValue<string>('elevation'), dotFormat);
            end;
            if current_observation.TryGetValue<TJSONObject>('observation_location', observation_location) then
            begin
              observationLat :=  Double.Parse(observation_location.GetValue<string>('latitude'), dotFormat);
              observationLon :=  Double.Parse(observation_location.GetValue<string>('longitude'), dotFormat);
            end;
            // rest meteo
            weather := current_observation.GetValue<string>('weather');
            temp_c := current_observation.GetValue<Double>('temp_c');
            relative_humidity := current_observation.GetValue<string>('relative_humidity');
            wind_dir := current_observation.GetValue<string>('wind_dir');
            wind_degrees := current_observation.GetValue<Double>('wind_degrees');
            wind_kph := current_observation.GetValue<Double>('wind_kph');
            wind_gust_kph := current_observation.GetValue<Double>('wind_gust_kph');
            pressure_mb := current_observation.GetValue<Double>('pressure_mb');
            dewpoint_c := current_observation.GetValue<Double>('dewpoint_c');
            Result := True;
          end;
        finally
          jsonObject.Free;
        end;
      except
        on E: Exception do
        begin
          Log.WriteLn('Could not load meteo: '+E.Message, llError);
          Result := False;
        end;
      end;
    finally
      http := VarNULL;
    end;
  end;
end;

initialization

finalization
  // auto
end.

