unit OSMGeometries;

interface

uses
  Classes,
  MyCoInit,
  Logger,
  WorldDataCode,
  System.JSON,
  System.NetEncoding,
  System.Win.ComObj,
  Generics.Collections,
  System.SysConst, System.SysUtils;

const
//  OVERPASS_BASE_URL = 'http://api.openstreetmap.fr/oapi/interpreter';
//  OVERPASS_BASE_URL = 'http://overpass.osm.rambler.ru/cgi/interpreter';
   OVERPASS_BASE_URL = 'http://overpass-api.de/api/interpreter';
  //?data=

  dotFormat: TFormatSettings = (DecimalSeparator:'.');

type
  TOSMID = Int64;

  TWDOSMGeometry = class(TWDGeometry)
    private
      fID: TOSMID;
    public
      function Copy: TWDGeometryBase; override;
      property ID: TOSMID read fID write fID;
  end;

  // y lat, x lon
  TOSMQuery = class
      constructor Create(aExtent: TWDExtent; const aQuery: string); overload;
      constructor Create(aExtent: TWDExtent; const aSelections: array of string); overload;
      constructor Create(aExtent: TWDExtent; const aMajorType: String; const aSubTypes: array of string); overload;
      destructor Destroy; override;
  private
    protected
      fDataAvailable: boolean;
      fExtent: TWDExtent;
      fSelections: array of string;
      fQuery: string;

      fOSMNodes: TDictionary<TOSMID, TWDPoint2D>;
      fOSMTags: TObjectDictionary<TOSMID, TStrings>;
      fOSMWays: TDictionary<TOSMID, TArray<TOSMID>>;

      function  BuildQuery: string;
      procedure FillGeometries(aList: TObjectList<TWDGeometry>; aDictionary: TObjectDictionary<TOSMID, TWDGeometry>); overload;
    public
      procedure FillGeometries(aList: TObjectList<TWDGeometry>); overload;
      procedure FillGeometries(aDictionary: TObjectDictionary<TOSMID, TWDGeometry>); overload;
      procedure FillTagsForWays(aTags: array of string; AList: TObjectDictionary<TOSMID, TStrings>);

      procedure Execute;
      property  DataAvailable: boolean read fDataAvailable write fDataAvailable;
  end;

  TWDExtentHelper = record helper for TWDExtent
    function asOverpassExtent: string;
  end;

function ExecuteOSMQuery(
  aExtent: TWDExtent;
  const aMajorType: string; const aSubTypes: array of string;
  aGeometries: TObjectList<TWDGeometry>): Integer; overload;

function ExecuteOSMQuery(
  aExtent: TWDExtent;
  const aSelections: array of string; // way["key"~"value1|value2|value3"] or rel["key"~"value1|value2|value3"]
  aGeometries: TObjectList<TWDGeometry>): Integer; overload;


implementation

function HTTPGet(const anURL: String; out aResponse: String): Integer;
var
  http: variant;
begin
  // check/init COM for thread once
  CheckCoInit();
  // do request
  http := createoleobject('WinHttp.WinHttpRequest.5.1');
  try
    http.open('GET', anURL, false);
    http.send;
    // process response
    aResponse := http.responsetext;
    Result := http.status;
  finally
    http := VarNULL;
  end;
end;

function ExecuteOSMQuery(aExtent: TWDExtent; const aMajorType: string; const aSubTypes: array of string; aGeometries: TObjectList<TWDGeometry>): Integer; overload;
var
  osmQ: TOSMQuery;
begin
  osmQ := TOSMQuery.Create(aExtent, aMajorType, aSubTypes);
  try
    osmQ.Execute;
    osmQ.FillGeometries(aGeometries);
    Result := aGeometries.Count;
  finally
    osmQ.Free;
  end;
end;

function ExecuteOSMQuery(aExtent: TWDExtent; const aSelections: array of string; aGeometries: TObjectList<TWDGeometry>): Integer; overload;
var
  osmQ: TOSMQuery;
begin
  osmQ := TOSMQuery.Create(aExtent, aSelections);
  try
    osmQ.Execute;
    osmQ.FillGeometries(aGeometries);
    Result := aGeometries.Count;
    Log.WriteLn('Finished OSM Query ' + osmQ.fQuery + ')', llOK);
  finally
    osmQ.Free;
  end;
end;

{ TOSMQuery }


constructor TOSMQuery.Create(aExtent: TWDExtent; const aQuery: string);
begin
  fQuery := aQuery;
  fDataAvailable := False;
  fExtent := aExtent;

  fOSMNodes := TDictionary<TOSMID, TWDPoint2D>.Create;
  fOSMTags := TObjectDictionary<TOSMID, TStrings>.Create([doOwnsValues]);
  fOSMWays := TDictionary<TOSMID, TArray<TOSMID>>.Create;
end;

constructor TOSMQuery.Create(aExtent: TWDExtent;
  const aSelections: array of string);
var
  I: Integer;
begin
  setlength(fSelections, length(aSelections));
  for I := Low(aSelections) to High(aSelections) do
    fSelections[I] := aSelections[I];
  fExtent := aExtent;
  Create(aExtent, BuildQuery);
end;

constructor TOSMQuery.Create(aExtent: TWDExtent; const aMajorType: String;
  const aSubTypes: array of string);
var
  subTypesStr: string;
  st: string;
  q: String;
begin
  subTypesStr := '';
  for st in aSubTypes do
  begin
    if subTypesStr<>''
    then subTypesStr := subTypesStr+'|';
    subTypesStr := subTypesStr+st;
  end;

  q := 'way["'+aMajorType+'"~"'+subTypesStr+'"]';
  Create(aExtent, [q]);
end;

function TOSMQuery.BuildQuery: string;
var
  s: string;
  es: string;
begin
  result := '';
  es := fExtent.asOverpassExtent;
  for s in fSelections do
  begin
    result := Result+s+es+';'
  end;
  result := '[out:json];('+result+');out;>;out;';
end;

destructor TOSMQuery.Destroy;
begin
  FreeAndNil(fOSMNodes);
  FreeAndNil(fOSMTags);
  FreeAndNil(fOSMWays);
  inherited;
end;

procedure TOSMQuery.Execute;
var
  response: string;
  rCode: Integer;

  jsonObject, tagsJSON: TJSONObject;
  elements, nodesJSON: TJSONArray;
  pair: TJSONPair;
  element: TJSONObject;
  i, j: Integer;
  id: TOSMID;
  intNodes: TArray<TOSMID>;
  nodeType: String;
  latlon: TWDPoint2D;
  tags: TStrings;
begin
  rCode :=  HTTPGet(OVERPASS_BASE_URL + '?data='+TNetEncoding.URL.Encode(fQuery), response);
  if rCode=200 then
  begin
    jsonObject := TJSONObject.ParseJSONValue(response) as TJSONObject;
    try
      if jsonObject.TryGetValue('elements', elements) then
      begin
        for i := 0 to elements.Count-1 do
        begin
          element := elements.Items[i] as TJSONObject;
          nodeType := element.GetValue<string>('type');
          id := element.GetValue<TOSMID>('id');
          if nodeType = 'way' then
          begin
            if element.TryGetValue('nodes', nodesJSON) then
            begin
              setLength(intNodes, nodesJSON.Count);
              for j := 0 to nodesJSON.count-1
              do intNodes[j] := nodesJSON.Items[j].GetValue<TOSMID>;
              fOSMWays.AddOrSetValue(id, intNodes);
            end;

            if element.TryGetValue('tags', tagsJSON) then
            begin
              tags := TStringList.Create;
              try
                for pair in tagsJSON
                do tags.Values[pair.JsonString.Value] := pair.JsonValue.Value;
              finally
                fOSMTags.AddOrSetValue(id, tags);
              end;
            end;

          end
          else if nodeType = 'node' then
          begin
            latlon.y := Double.Parse(element.GetValue<string>('lat'), dotFormat);
            latlon.x := Double.Parse(element.GetValue<string>('lon'), dotFormat);
            fOSMNodes.AddOrSetValue(id, latlon);
          end;
        end;
      end;
    finally
      jsonObject.Free;
    end;
    fDataAvailable := True;
  end;
end;

procedure TOSMQuery.FillGeometries(aList: TObjectList<TWDGeometry>);
begin
  FillGeometries(aList, nil);
end;

procedure TOSMQuery.FillGeometries(
  aDictionary: TObjectDictionary<TOSMID, TWDGeometry>);
begin
  FillGeometries(nil, aDictionary);
end;

procedure TOSMQuery.FillGeometries(aList: TObjectList<TWDGeometry>;
  aDictionary: TObjectDictionary<TOSMID, TWDGeometry>);
var
  way: TPair<TOSMID, TArray<TOSMID>>;
  geometry: TWDOSMGeometry;
  i: Integer;
  latlon: TWDPoint2D;
  doDict, doList: boolean;
begin
  doDict := Assigned(aDictionary);
  doList := Assigned(aList);

  for way in fOSMWays do
  begin
    geometry := TWDOSMGeometry.Create;
    try
      for i := Low(way.Value) to High(way.Value) do
      begin
        if fOSMNodes.TryGetValue(way.Value[i], latlon) then
          geometry.AddPoint(latlon.x, latlon.y, Double.NaN)
        else
          Log.WriteLn('Missing node: ' + IntToStr(way.Value[i]) + ' in OSM Way ' + IntToStr(way.Key), llWarning);
      end;
      geometry.fID := way.Key;
    finally
      if doDict then
        aDictionary.Add(way.Key, geometry);
      if doList then
        if doDict then
          aList.Add(geometry.Copy as TWDOSMGeometry)
        else
          aList.Add(geometry);
    end;
  end;
end;

procedure TOSMQuery.FillTagsForWays(aTags: array of string; AList: TObjectDictionary<TOSMID, TStrings>);
var
  osmItem: TPair<TOSMID, TStrings>;
  tagStrings: TStrings;
  tag, v: string;
begin
  for osmItem in aList do
  begin
    if fOSMTags.TryGetValue(osmItem.Key, tagStrings) then
      for tag in aTags do
      begin
        v := tagStrings.Values[tag];
        if not v.IsEmpty then
          osmItem.Value.Values[tag] := v;
      end;
  end;
end;

{ TWDOSMGeometry }

function TWDOSMGeometry.Copy: TWDGeometryBase;
var
  part, part2: TWDGeometryPart;
  point: TWDGeometryPoint;
begin
  // inherited copy?

  Result := TWDOSMGeometry.Create; // Correctly create descendants
  for part in Parts do
  begin
    part2 := TWDGeometryPart.Create;

    for point in part.Points do
      part2.AddPoint(point.x, point.y, point.z);

    (Result as TWDOSMGeometry).Parts.Add(part2);
    (Result as TWDOSMGeometry).fID := fID;
  end;
end;

{ TWDExtentHelper }

function TWDExtentHelper.asOverpassExtent: string;
begin
   Result := '('+
      yMin.toString(dotFormat)+','+xMin.ToString(dotFormat)+','+
      yMax.ToString(dotFormat)+','+xMax.ToString(dotFormat)+
    ')';
end;

end.
